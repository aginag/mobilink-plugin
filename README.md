# README #

IOS
Instructions for the plugin after installation:

1. In the bridging header file of the project (example Other Sources->Bridging-Header.h), include the line below:
 #import "UltraLiteDB.h"
 #import "ZipArchive.h"

2. For the ZipArchive to work, additional framework file needs to be added. Under the target, in section ‘Linked Frameworks and Libraries’, add a new entry and search and choose ‘libz.tbd’ 

3. In the file ‘UltraLiteDB.mm’, the import statement needs to be adjusted as per project name. For example, if the project name is ‘My Project’, then the statement will look like this:
 #import “My_Project-Swift.h”

In case the project name is ‘MyProj’, then the statement will look like this:
 #import “MyProj-Swift.h”

### What is this repository for? ###

* This is a repository for mobilink plugin from msc mobile GmbH for SAM

### How do I get set up? ###

* cordova plugin add https://bitbucket.org/msc-mobile/mobilink-plugin.git

### Who do I talk to? ###

* alexander.ilg@msc-mobile.com, ganesh.ms@msc-mobile.com
* Other community or team contact
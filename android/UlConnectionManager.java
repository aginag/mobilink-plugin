package com.mscmobile.mscmobilinkplugin;

import com.sap.ultralitejni17.ConfigFileAndroid;
import com.sap.ultralitejni17.Connection;
import com.sap.ultralitejni17.DatabaseManager;

import java.util.ArrayList;
import java.util.concurrent.Semaphore;

/**
 * Created by Klaus on 24.10.2016.
 */

class UlConnectionInfo {
    private Connection _connection = null;
    private volatile boolean _inUse = false;

    public UlConnectionInfo(Connection conn) {
        _connection = conn;
    }

    public Connection getConnection() {
        return _connection;
    }

    public boolean getInUse() {
        return _inUse;
    }

    public void setInUse(boolean inUse) {
        _inUse = inUse;
    }
}

class UlConnectionManager {

    private final Object _lock = new Object();
    private final Object _closeLock = new Object();
    private ConfigFileAndroid _config = null;
    private volatile UlConnectionInfo[] _conInfos = null;
    private Semaphore _conSemaphore = null;

    public UlConnectionManager(ConfigFileAndroid config) throws Exception {
        _config = config;
        _conInfos = getConnections(config);
        _conSemaphore = new Semaphore(_conInfos.length, true); //true: FIFO queue
    }

    private UlConnectionInfo[] getConnections(ConfigFileAndroid config) throws Exception {
        ArrayList<UlConnectionInfo> connections = new ArrayList<UlConnectionInfo>();
        try {
            //aquire as many connections as possible
            DatabaseManager.release();

            while (true) {
                //for(int i = 0; i<3;i++){
                //config.setEN
                //config.setDatabaseName("files/files/pbertrand/sam.udb");
                Connection conn = DatabaseManager.connect(config);
                UlConnectionInfo conInfo = new UlConnectionInfo(conn);
                connections.add(conInfo);
            }
        } catch (Exception ex) {
            String msg = ex.getMessage();
        }
        if (connections.isEmpty()) {
            throw new Exception("Could not aquire any DB connection for: " + config.getCreationString());
        }
        return connections.toArray(new UlConnectionInfo[connections.size()]);
    }

    public Connection getConnection() throws Exception {
        _conSemaphore.acquire();
        synchronized (_lock) {
            for (int i = 0; i < _conInfos.length; i++) {
                UlConnectionInfo conInfo = _conInfos[i];
                if (!conInfo.getInUse()) {
                    conInfo.setInUse(true);
                    return conInfo.getConnection();
                }
            }
            throw new Exception("Could not get a Connection: all connections are in use");
        }
    }

    public Connection getSingleConnection() throws Exception {
        DatabaseManager.release();
        Connection conn = DatabaseManager.connect(_config);
        return conn;
    }

    public void releaseConnection(Connection conn) throws Exception {
        if (conn == null) {
            return;
        }

        synchronized (_lock) {
            for (int i = 0; i < _conInfos.length; i++) {
                UlConnectionInfo conInfo = _conInfos[i];
                if (conInfo.getConnection() == conn) {
                    conInfo.setInUse(false);
                    _conSemaphore.release();
                    return;
                }
            }
            throw new Exception("Could not release Connection as provided connection is unknown");
        }
    }

    /**
     * @param hard if true closes the connections immediately, else waits for them to become available
     * @throws InterruptedException if soft closing the connections failed
     */
    public void close(boolean hard) throws InterruptedException {
        synchronized (_closeLock) {
//            _closeLock.wait();
            if (_conInfos != null) {
                try {
                    if (hard) {
                        //hard close the connections; if they are in use by other threads it will throw exceptions
                        for (UlConnectionInfo conInfo : _conInfos) {
                            Connection con = conInfo.getConnection();
                            try {
                                con.release();
                            } catch (Exception ex) {
                                //TODO maybe do some console out
                                String msg = ex.getMessage();
                            }
                        }
                    } else {
                        //soft close the connections, wait until they are all available, then close them
                        try {
                            _conSemaphore.acquire(_conInfos.length);
                            for (UlConnectionInfo conInfo : _conInfos) {
                                try {
                                    conInfo.getConnection().release();
                                } catch (Exception ex) {
                                    //TODO maybe do some console out
                                    String msg = ex.getMessage();
                                }
                            }
                        } finally {
                            _conSemaphore.release(_conInfos.length);
                        }
                    }
                } finally {
                    _conInfos = null;
                    _conSemaphore = null;
                }
            }
        }
    }
}
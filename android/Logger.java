package com.mscmobile.mscmobilinkplugin;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Klaus on 27.09.2016.
 */
enum LogLevel {
VERBOSE,
INFO,
ERROR,
DO_NOT_LOG;

static int _padding = determinePadding();

static int determinePadding() {
int padding = 0;
for (LogLevel logLevel : LogLevel.values()) {
String logLevelAsString = logLevel.toString();
if (logLevelAsString.length() > padding) {
padding = logLevelAsString.length();
}
}
return padding;
}

public static LogLevel parse(String logLevel) throws Exception {
switch (logLevel) {
case "0":
return LogLevel.ERROR;
case "1":
return LogLevel.INFO;
case "2":
return LogLevel.VERBOSE;
default:
throw new Exception("unsopported LogLevel: " + logLevel);
}
}

/**
 * padded toString() implementation;
 * the returned strings all have the same length;
 * the length is determined based on the enum value with the longest name
 *
 * @return
 */
public String toPaddedString() {
return String.format("%1$-" + _padding + "s", this.toString());
}
}

class LogFileEntry {
    private String _time = null;
    private char _logLevel;
    private String _message = null;
    
    public LogFileEntry(String time, char logLevel, String message) {
        _time = time;
        _logLevel = logLevel;
        _message = message;
    }
    
    public String getTime() {
        return _time;
    }
    
    public char getLogLevel() {
        return _logLevel;
    }
    
    public String getMessage() {
        return _message;
    }
    
    public void setMessage(String message) {
        _message = message;
    }
}

class Logger {
    private static String _newLine = System.getProperty("line.separator");
    private static String _timeFormat = "yyyy-MM-dd HH:mm:ss";
    private File _logFile = null;
    private LogLevel _logLevel = LogLevel.VERBOSE;
    private FileWriter _writer = null;
    
    protected Logger(File folder, String logFileName, LogLevel logLevel) throws Exception {
        if (!folder.exists() || !folder.isDirectory()) {
            folder.mkdir();
        }
        
        _logFile = new File(folder, logFileName);
        _writer = new FileWriter(_logFile, true); //true : append to file
        _logLevel = logLevel;
        _writer.write("##################################################" + _newLine);
        _writer.write(String.format("%s: Initializing log with UTC time; log level: %s" +
                                    _newLine, currentDateTime(), logLevel));
        _writer.flush();
    }
    
    private static String currentDateTime() {
        Date currentTime = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat(_timeFormat);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        String result = sdf.format(currentTime);
        return result;
    }
    
    protected void close() throws IOException {
        if (_writer != null) {
            _writer.flush();
            _writer.close();
            _writer = null;
        }
    }
    
    protected String getLogFilePath() {
        String result = null;
        if (_logFile != null) {
            result = _logFile.getAbsolutePath();
        }
        return result;
    }
    
    protected synchronized void log(String message, LogLevel logLevel) {
        try {
            if (IsLogLevelOk(logLevel)) {
                _writer.append(dateAndLogLevel(logLevel) + message + _newLine);
                _writer.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * empties the log file
     */
    protected synchronized void clearLog() {
        try {
            PrintWriter writer = new PrintWriter(_logFile);
            writer.print("");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
    }
    
    protected synchronized JSONArray getLogContent() throws Exception {
        JSONArray result = new JSONArray();
        
        //_writer.flush();
        FileInputStream is = new FileInputStream(_logFile);
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        
        String line = reader.readLine();
        while (line != null) {
            result.put(line);
            line = reader.readLine();
        }
        reader.close();
        is.close();
        
        return result;
    }
    
    protected synchronized ArrayList<LogFileEntry> cutText() throws Exception {
        ArrayList<String> LogLines = new ArrayList<>();
        FileInputStream is = null;
        BufferedReader reader = null;
        PrintWriter writer = null;
        
        PerformanceTimer pt = new PerformanceTimer();
        pt.start();
        pt.interim("cutText: reading lines from log file");
        try {
            //read log file lines
            is = new FileInputStream(_logFile);
            reader = new BufferedReader(new InputStreamReader(is));
            
            String line = reader.readLine();
            while (line != null) {
                LogLines.add(line);
                line = reader.readLine();
            }
            is.close();
            is = null;
            pt.interim("cutText: finished reading lines from log file");
            
            //empty log file
            writer = new PrintWriter(_logFile);
            writer.print("");
            writer.close();
            writer = null;
            pt.interim("cutText: emptied log file");
            
            //convert to LogFileEntry structure
            ArrayList<LogFileEntry> logEntries = logLinesToLogFileEntry(LogLines);
            pt.interim("cutText: converted log lines");
            pt.stop();
            return logEntries;
        } finally {
            if (reader != null) {
                reader.close();
            }
            if (is != null) {
                is.close();
            }
            if (writer != null) {
                writer.close();
            }
        }
    }
    
    protected LogLevel getLogLevel() {
        return _logLevel;
    }
    
    protected void setLogLevel(LogLevel logLevel) {
        log("changing log level to: " + logLevel, LogLevel.VERBOSE);
        _logLevel = logLevel;
    }
    
    @Override
    protected void finalize() throws Throwable {
        if (_writer != null) {
            _writer.flush();
        }
        super.finalize();
        if (_writer != null) {
            _writer.close();
        }
    }
    
    private String dateAndLogLevel(LogLevel logLevel) {
        String paddedLogLevel = logLevel.toPaddedString();
        String result = String.format("%s: %s: ", currentDateTime(), paddedLogLevel);
        return result;
        //return String.format("%1$s: %2$s: ", currentDateTime(), logLevel.ToPaddedString());
    }
    
    private boolean IsLogLevelOk(LogLevel logLevel) {
        if (logLevel.ordinal() >= _logLevel.ordinal()) {
            return true;
        }
        return false;
    }
    
    private ArrayList<LogFileEntry> logLinesToLogFileEntry(AbstractCollection<String> lines) {
        String logFileInitChar = "#";
        ArrayList<LogFileEntry> result = new ArrayList<>(lines.size());
        int timeLength = _timeFormat.length();
        int logLevelEndPos = timeLength + 2 + LogLevel._padding; //+2 because of ": " after timeLength
        int messageStartPos = logLevelEndPos + 2;//+2 because of ": " after log level
        LogFileEntry logFileEntry = null;
        String logMessage = "";
        
        for (String line : lines) {
            if (line != null && !line.isEmpty() && !line.startsWith(logFileInitChar)) {
                String time = "";
                String logLevel = null;
                
                //log header with time
                if (line.length() > timeLength && line.charAt(13) == ':' && line.charAt(16) == ':') {
                    time = line.substring(0, timeLength);
                }
                
                //log level
                if (line.length() > logLevelEndPos && line.charAt(timeLength) == ':' && line.charAt(logLevelEndPos) == ':') {
                    int startPos = timeLength + 1;
                    logLevel = line.substring(startPos, logLevelEndPos).trim();
                }
                
                //a log file entry can be multiline
                if (logLevel != null) {
                    //add previous entry
                    if (logFileEntry != null) {
                        logFileEntry.setMessage(logMessage);
                        result.add(logFileEntry);
                        logFileEntry = null;
                        logMessage = "";
                    }
                    logMessage += line.substring(messageStartPos).trim();
                    logFileEntry = new LogFileEntry(time, logLevel.charAt(0), "");
                } else if (time == null || time.isEmpty()) {
                    logMessage += " " + line;
                }
            }
        }
        if (logFileEntry != null) {
            logFileEntry.setMessage(logMessage);
            result.add(logFileEntry);
        }
        return result;
    }
}

package com.mscmobile.mscmobilinkplugin;

import android.support.annotation.Nullable;
import android.util.Log;

import com.sap.ultralitejni17.FileTransfer;
import com.sap.ultralitejni17.FileTransferProgressData;
import com.sap.ultralitejni17.FileTransferProgressListener;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.PluginResult;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InvalidObjectException;
import java.security.InvalidKeyException;
import java.util.ArrayList;

/**
 * Created by Klaus on 26.10.2016.
 */
enum ULTransferType {
    Upload,
    Download,
}

class UlFileTransfer {
    private FileTransfer _fileTransfer = null;
    private ULTransferType _type;
    private String _transferId = null;
    private CallbackContext _cbContext = null;
    private volatile boolean _cancelRequested = false;

    public UlFileTransfer(FileTransfer fileTransfer, ULTransferType type, String transferId,
                          CallbackContext cbContext) {
        _fileTransfer = fileTransfer;
        _type = type;
        _transferId = transferId;
        _cbContext = cbContext;
    }

    public ULTransferType getType() {
        return _type;
    }

    public String getTransferId() {
        return _transferId;
    }

    public FileTransfer getFileTransfer() {
        return _fileTransfer;
    }

    public CallbackContext getCallbackContext() {
        return _cbContext;
    }

    public boolean getCancelRequested() {
        return _cancelRequested;
    }

    public void setCancelRequested(boolean cancel) {
        _cancelRequested = cancel;
    }
}

class UlFileTransferManager {
    private final static Object _fileTransfersLock = new Object();
    private final static Object _criticalSection = new Object();
    private volatile static ArrayList<UlFileTransfer> _fileTransfers = new ArrayList<>();
    private static UlFileTransferManager _instance = new UlFileTransferManager();

    private UlFileTransferManager() {
    }

    static UlFileTransferManager getInstance() {
        return _instance;
    }

    public void transferFile(UlFileTransfer ft) throws Exception {
        FileTransferProgressListener listener = null;

        try {
            //synchronized: no multiple file transfers can be started for the same transferId
            synchronized (_criticalSection) {
                //check if there is already a file transfer with the same id
                String id = ft.getTransferId();
                //if(id != null && id.isEmpty()){
                if (id == null || id.isEmpty()) {
                    throw new InvalidKeyException("transferId must not be null or empty");
                }
                UlFileTransfer existingFileTransfer = getFileTransfer(id);
                if (existingFileTransfer != null) {
                    String msg = String.format("transferFile error: a file transfer for id: %s is already in progress", id);
                    throw new InvalidObjectException(msg);
                }

                final CallbackContext cbContext = ft.getCallbackContext();
                final String transferId = ft.getTransferId();

                listener = new FileTransferProgressListener() {
                    @Override
                    public boolean fileTransferProgressed(FileTransferProgressData pData) {
                        UlFileTransfer fTransfer = getFileTransfer(transferId);
                        if (fTransfer == null) {
                            String msg = "fileTransferProgressListener error: cannot find file transfer object for id: " + transferId;
                            throw new IllegalStateException(msg);
                        }
                        //in case a callback context was provided we provide progress info
                        if (cbContext != null) {
                            try {
                                JSONObject progress = createFileTransferProgress(transferId, "status not available",
                                        pData.getFileSize(), pData.getBytesTransferred(), "");
                                //PluginResult pr = new PluginResult(PluginResult.Status.OK, progress.toString());
                                PluginResult pr = new PluginResult(PluginResult.Status.OK, progress);
                                pr.setKeepCallback(true);
                                cbContext.sendPluginResult(pr);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        /* for testing only
                        if(fTransfer.getCancelRequested()){
                            Log.d("ftm", "progress listener received flag to cancel download for: " + fTransfer.getTransferId());
                        }
                        */
                        }
                        //false: continue file transfer; true: abort file transfer
                        return fTransfer.getCancelRequested();
                    }
                };
                addFileTransfer(ft);
            }

            boolean success = false;
            //Download
            if (ft.getType() == ULTransferType.Download) {
                if (listener != null) {
                    success = ft.getFileTransfer().downloadFile(listener);//blocking; returns when transfer is finished
                } else {
                    success = ft.getFileTransfer().downloadFile(); //blocking
                }
            }
            //Upload
            else {
                if (listener != null) {
                    success = ft.getFileTransfer().uploadFile(listener);//blocking; returns when transfer is finished
                } else {
                    success = ft.getFileTransfer().uploadFile(); //blocking
                }
            }

            if (!success) {
                throw new Exception("could not transfer file :" + ft.getTransferId());
            }
        } catch (Exception ex) {
            String msg = String.format("transferFile: could not transfer file with id %s; error: %s",
                    ft.getTransferId(), ex.getMessage());
            throw new Exception(msg);
        } finally {
            removeFileTransfer(ft);
        }
    }

    /**
     * thread safe
     *
     * @param transferId
     * @throws Exception
     */
    public void cancelFileTransfer(String transferId) throws Exception {
        UlFileTransfer fileTransfer = getFileTransfer(transferId);
        if (fileTransfer != null) {
            fileTransfer.setCancelRequested(true);
            Log.d("ftm", "cancelling file transfer for: " + transferId);
        } else {
            throw new Exception("cancelFileTransfer error: cannot find file transfer with id: " +
                    transferId);
        }
    }

    @Nullable
    private UlFileTransfer getFileTransfer(String transferId) {
        synchronized (_fileTransfersLock) {
            for (int i = 0; i < _fileTransfers.size(); i++) {
                UlFileTransfer fileTransfer = _fileTransfers.get(i);
                if (transferId.equals(fileTransfer.getTransferId())) {
                    return fileTransfer;
                }
            }
        }
        return null;
    }

    private void addFileTransfer(UlFileTransfer fileTransfer) {
        synchronized (_fileTransfersLock) {
            _fileTransfers.add(fileTransfer);
        }
    }

    private void removeFileTransfer(UlFileTransfer ft) {
        synchronized (_fileTransfersLock) {
            _fileTransfers.remove(ft);
        }
    }

    private JSONObject createFileTransferProgress(String fileId, String status, long fileSize,
                                                  long bytesTransferred, String error) throws JSONException {
        JSONObject result = new JSONObject();
        result.put("progressType", "FileTransfer");
        result.put("fileId", fileId);
        result.put("status", status);
        result.put("fileSize", fileSize);
        result.put("bytesTransferred", bytesTransferred);
        result.put("error", error);

        return result;
    }

}
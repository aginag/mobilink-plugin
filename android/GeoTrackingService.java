package com.mscmobile.mscmobilinkplugin;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;


public class GeoTrackingService {
    private static final String TAG = "GEO_TRACKING_SERVICE";
    private static final String TRACKING_SYNC_MODEL = "LIVE_TRACKING";
    private static final String LOCATION_BROADCAST_PERMISSION = "com.mscmobile.mscmobilinkplugin.GeoTrackingService.RECEIVE_BROADCAST";
    private static final Integer DEFAULT_SYNC_INTERVAL = 3 * 60000;
    
    private Context context;
    private CordovaInterface cordova;
    private CallbackContext cbContext;
    
    private Timer timer;
    private MlUser currentUser;
    private Integer customInterval;
    private GeoLocation currentLocation;
    
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
    Log.v(TAG, "Received");
    Bundle extras = intent.getExtras();
    
    if (extras == null) {
        return;
    }
    
    double latitude = extras.getDouble(LocationBackgroundService.EXTRA_LATITUDE);
    double longitude = extras.getDouble(LocationBackgroundService.EXTRA_LONGITUDE);
    
    if (currentUser != null) {
        Log.v(TAG, "Sync new location");
        currentUser.logger.log("New Location received: " + latitude + ", " + longitude, LogLevel.INFO);

        if (currentUser.ulWrapper._syncInProgress) {
            currentUser.logger.log("Sync currently in progress, not writing it to the database. Caching the location.", LogLevel.INFO);
            currentLocation = new GeoLocation(latitude, longitude);
            return;
        }
        
        try {
            // 1. Write new location to udb table
            insertLocation(latitude, longitude);
            currentLocation = new GeoLocation(latitude, longitude);
        } catch (Exception e) {
            Log.e(TAG, "Error when trying handle new location: " + e.getMessage());
            currentUser.logger.log("Error when trying handle new location: " + e.getMessage(), LogLevel.ERROR);
        }
    }
}
};

public GeoTrackingService(Context context, CordovaInterface cordova) {
this.context = context;
this.cordova = cordova;
this.currentUser = null;

this.context.registerReceiver(this.broadcastReceiver, new IntentFilter(LocationBackgroundService.LOCATION_ACTION_BROADCAST), LOCATION_BROADCAST_PERMISSION, null);
}

public void start(MlUser user, CallbackContext cbContext, Integer interval) {
this.currentUser = user;
this.cbContext = cbContext;
this.currentUser.logger.log("Starting location tracking", LogLevel.INFO);
this.customInterval = interval != null ? interval * 60000 : DEFAULT_SYNC_INTERVAL;
this.timer = new Timer();

currentUser.logger.log("Location Tracking - Using custom sync interval of " + this.customInterval + "ms", LogLevel.INFO);

Intent locationService = new Intent(this.context, LocationBackgroundService.class);
locationService.putExtra(LocationBackgroundService.EXTRA_INTERVAL, 15000);

if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
this.context.startForegroundService(locationService);
} else {
this.context.startService(locationService);
}


this.timer.scheduleAtFixedRate(new TimerTask() {
@Override
public void run() {
Log.v(TAG, "Sync new location");
try {
pingMiddleware();
handlePreDbSync();
syncDbAsync();
} catch (IOException e) {
Log.e(TAG, "Location Tracking - Error when trying to ping MW: " + e.getMessage());
currentUser.logger.log("Location Tracking - Error when trying to ping MW: " + e.getMessage(), LogLevel.ERROR);
} catch (Exception e) {
Log.e(TAG, "Location Tracking - Error when trying to sync against LT sync model: " + e.getMessage());
currentUser.logger.log("Location Tracking - Error when trying to sync against LT sync model: " + e.getMessage(), LogLevel.ERROR);
}
}
}, 1000, this.customInterval);

if (this.cbContext != null) {
this.cbContext.sendPluginResult(GeoTrackingServicePluginResults.serviceStarted());
}
}

public void stop() {
this.currentUser.logger.log("Stopping location tracking", LogLevel.INFO);
this.currentUser = null;
this.cbContext = null;

Intent stopIntent = new Intent(this.context, LocationBackgroundService.class);
stopIntent.setAction(LocationBackgroundService.STOPFOREGROUND_ACTION);
this.context.startService(stopIntent);

this.timer.cancel();
this.timer.purge();
}

private void handlePreDbSync() throws Exception {
JSONArray result = this.currentUser.ulWrapper.executeQueryAsJsonArray("SELECT * FROM SAM_LT_COORD WHERE ACTION_FLAG != 'O'", false);

if (result.length() > 0 || currentLocation == null) {
return;
}

insertLocation(currentLocation.latitude, currentLocation.longitude);
}

private void insertLocation(double latitude, double longitude) throws Exception {
String latitudeString = Double.toString(latitude).length() > 18 ? Double.toString(latitude).substring(0, 18) : Double.toString(latitude);
String longitudeString = Double.toString(longitude).length() > 18 ? Double.toString(longitude).substring(0, 18) : Double.toString(longitude);

DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
String deleteOldLocationStmt = "DELETE FROM SAM_LT_COORD";
String newLocationStmt = String.format("INSERT INTO SAM_LT_COORD (LATITUDE, LONGITUDE, USER_NAME, CREATED_ON, ACTION_FLAG)" +
"VALUES('%s', '%s', '%s', '%s', '%s')",
latitudeString, longitudeString, currentUser.getName(), dateFormat.format(new Date()), "N");

Log.v(TAG, "Insert new location: " + newLocationStmt);
this.currentUser.logger.log("Insert new location: " + newLocationStmt, LogLevel.INFO);
this.currentUser.ulWrapper.executeUpdate(deleteOldLocationStmt);
this.currentUser.ulWrapper.executeUpdate(newLocationStmt);
}

private void checkForNewObjects() throws Exception {
JSONArray result = this.currentUser.ulWrapper.executeQueryAsJsonArray("SELECT * FROM SAM_USER_OBJECT_TRACKING", false);

if (result.length() == 0) {
return;
}

if (this.cbContext != null) {

try {
Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
Ringtone r = RingtoneManager.getRingtone(this.context, notification);
r.play();
} catch (Exception e) {
e.printStackTrace();
}

this.cbContext.sendPluginResult(GeoTrackingServicePluginResults.newObjects(result));
}
}

private void pingMiddleware() throws IOException {
Socket socket = new Socket();
socket.connect(new InetSocketAddress(currentUser.getConnectionInfo().hostName, currentUser.getConnectionInfo().port), 5000);
}

private void syncDbAsync() throws Exception {
try {
PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
final PowerManager.WakeLock wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK, "My wakelook");
wakeLock.acquire();

cordova.getThreadPool().execute(new Runnable() {
public void run() {
try {
String syncMode = "";
currentUser.ulWrapper.syncDb(currentUser.getName(),
currentUser.getPw(), TRACKING_SYNC_MODEL,
null, syncMode, false);
} catch (Exception ex) {
Log.e(TAG, "Error when trying to sync db: " + ex.getMessage());
currentUser.logger.log("Error when trying to sync db: " + ex.getMessage(), LogLevel.ERROR);
} finally {
if (wakeLock != null) {
wakeLock.release();
}

try {
checkForNewObjects();
} catch (Exception e) {
currentUser.logger.log("Error when trying to check for new objects: " + e.getMessage(), LogLevel.ERROR);
}

}
}
});
} catch (Exception e) {
Log.e(TAG, "Error when trying to sync db: " + e.getMessage());
currentUser.logger.log("Error when trying to sync db: " + e.getMessage(), LogLevel.ERROR);
}
}
}


class GeoTrackingServicePluginResults {
    public static PluginResult serviceStarted() {
        JSONObject pInfo = new JSONObject();
        
        try {
            pInfo.put("EVENT", 1);
            pInfo.put("STATUS", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        
        PluginResult pr = new PluginResult(PluginResult.Status.OK, pInfo);
        pr.setKeepCallback(true);
        
        return pr;
    }
    
    public static PluginResult locationChanged(double latitude, double longitude) {
        JSONObject pInfo = new JSONObject();
        
        try {
            pInfo.put("EVENT", 2);
            pInfo.put("STATUS", true);
            pInfo.put("LATITUDE", latitude);
            pInfo.put("LONGITUDE", longitude);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        
        PluginResult pr = new PluginResult(PluginResult.Status.OK, pInfo);
        pr.setKeepCallback(true);
        
        return pr;
    }
    
    public static PluginResult newObjects(JSONArray objects) {
        JSONObject pInfo = new JSONObject();
        
        try {
            pInfo.put("EVENT", 3);
            pInfo.put("STATUS", true);
            pInfo.put("OBJECTS", objects);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        
        PluginResult pr = new PluginResult(PluginResult.Status.OK, pInfo);
        pr.setKeepCallback(true);
        
        return pr;
    }
}

class GeoLocation {
    double latitude;
    double longitude;
    
    GeoLocation(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }
}


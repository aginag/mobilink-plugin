package com.mscmobile.mscmobilinkplugin;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;


import android.content.Context;
import android.os.PowerManager;

public class Zipper {

	@SuppressWarnings("resource")

	/**
	 * @param filePath			relative path, e.g. data/data/com.gea.salesplus.odb/files/files/ExpertCareAdFiles/expert_care_ad.zip
	 * @param destFolderPath
	 * @return
	 */
	public static boolean unzip(String filePath, String destFolderPath){
		InputStream is;
		ZipInputStream zis;
		ZipEntry ze;
		try {
			// construct which reads from a file
			is = new FileInputStream(filePath);
			// construct to read (decompress) the data from zip files
			zis = new ZipInputStream(new BufferedInputStream(is));
			try {
				ze = zis.getNextEntry();
				// while file has entry
				while(ze != null){
					// specialized OutputStream for writing content to an internal byte array
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					byte[] buffer = new byte[1024];
					int count;
					// get filename of entry
					String filename = ze.getName();
					FileOutputStream fout = new FileOutputStream(destFolderPath + "/" + filename);
					// count = read compressed file into buffer; while count != -1
					while((count = zis.read(buffer)) != -1){
						// write the bytes from count into the buffer array; 0 = initial position in buffer to retrieve bytes
						baos.write(buffer, 0, count);
						// returns the contents of ByteArrayOutputStream and store it into byte array
						byte[] bytes = baos.toByteArray();
						// write the array to FileOutputStream
						fout.write(bytes);
						// reset the stream to override the bytes
						baos.reset();
					}
					// close FileOutputStream
					fout.close();
					// close the current zip entry and prepares to read next entry
					zis.closeEntry();
					ze = zis.getNextEntry();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		return true;
		
	}

	/**
	 * unzips all contents from zipFilePath into destFolderPath;
	 * overwrites existing files;
	 * supports folders in the zip file
	 * @param zipFilePath
	 * @param destFolderPath
	 * @return true if the zip file could be successfully extracted; false otherwise
	 */
	public static boolean unzipFile(String zipFilePath, String destFolderPath){
		ZipInputStream zis = null;
		File rootFolder = null;
		try{
			zis = new ZipInputStream(new FileInputStream(zipFilePath));
			ZipEntry ze = null;

			rootFolder = new File(destFolderPath);
			//make sure rootFolder exists;
			if(!rootFolder.exists()){
				if(rootFolder.mkdirs()){
					//throw new IOException("Cannot create unzip folder : " + destFolderPath);
					return false;
				}
			}

			while ((ze = zis.getNextEntry()) != null) {
				//ignore directories, will be handled by makeSurePathExists
				if(!ze.isDirectory()){
					File fe = new File(rootFolder, ze.getName());
					makeSurePathExists(fe);

					// overwrite file if exists
					if (fe.exists()) {
						fe.delete();
					}
					fe.createNewFile();

					unzipFileStream(zis, fe);
					zis.closeEntry();
				}
			}
			return true;
		}
		catch(Exception ex){
			//throw new Exception("Could not unzip: " + zipFilePath + " to: " + destFolderPath + ": " + ex.getMessage());
			ex.printStackTrace();
			return false;
		}
		finally {
			if(zis != null){
				try {
					zis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void zip(File source, File destination) throws Exception {
		ZipOutputStream zos = null;

		try{
			if(!source.exists()){
				throw new Exception("zip: specified source not found: " + source.getAbsolutePath());
			}
			zos = new ZipOutputStream(new BufferedOutputStream(new
					FileOutputStream(destination.getAbsolutePath())));
			if(source.isFile()){
				addFileToArchive(zos, source, "");
			}
			else{
				addFolderToArchive(zos, source, "");
			}
			zos.close();
		}
		catch(Exception ex){
			String msg = "zip failed: " + ex.getMessage();
			throw new Exception(msg, ex);
		}
		finally {
			if(zos != null){
				zos.close();
			}
		}
	}
    
    public static void zip(List<File> sources, File destination) throws Exception {
        ZipOutputStream zos = null;
        
        try{
            zos = new ZipOutputStream(new BufferedOutputStream(new
                                                               FileOutputStream(destination.getAbsolutePath())));
            
            for (File file : sources) {
                if(!file.exists()){
                    throw new Exception("zip: specified source not found: " + file.getAbsolutePath());
                }
                
                if(file.isFile()){
                    addFileToArchive(zos, file, "");
                }
                else{
                    addFolderToArchive(zos, file, "");
                }
            }
            
            zos.close();
        }
        catch(Exception ex){
            String msg = "zip failed: " + ex.getMessage();
            throw new Exception(msg, ex);
        }
        finally {
            if(zos != null){
                zos.close();
            }
        }
    }

	/**
	 *
	 * @param zos
	 * @param file
	 * @param pathInArchive the relative path (subfolder(s)) in the archive where to put the file
	 * @throws Exception
     */
	private static void addFileToArchive(ZipOutputStream zos, File file, String pathInArchive) throws Exception {
		int bufferSize = 102400; //100 kbyte
		BufferedInputStream bis = null;

		try {
			bis = new BufferedInputStream(new FileInputStream(file.getAbsolutePath()));
			File zipEntryPath = new File(pathInArchive, file.getName());
			ZipEntry zEntry = new ZipEntry(zipEntryPath.getPath());
			zos.putNextEntry(zEntry);

			byte[] buffer = new byte[bufferSize];
			int count;
			while ((count = bis.read(buffer, 0, bufferSize)) != -1) {
				zos.write(buffer, 0, count);
			}
		}
		catch (Exception ex){
			String msg = String.format("could not add file: %s to archive, error: %s",
					file.getAbsolutePath(), ex.getMessage());
			throw new Exception(msg, ex);

		}
		finally {
			if(bis != null){
				bis.close();
			}
		}
	}

	private static void addFolderToArchive(ZipOutputStream zos, File folder, String pathInArchive) throws Exception {
		try{
			String zipEntryPath = new File(pathInArchive, folder.getName()).getPath();
			File[] files = folder.listFiles();
			for(File folderEntry: files){
				if(folderEntry.isFile()){
					addFileToArchive(zos, folderEntry, zipEntryPath);
				}
				//folderEntry is a directory
				else{
					addFolderToArchive(zos, folderEntry, zipEntryPath);
				}
			}
		}
		catch(Exception ex){
			String msg = String.format("could not add folder: %s to archive, error: ",
					folder.getAbsolutePath(), ex.getMessage());
			throw new Exception(msg, ex);
		}
	}
	
	private static void makeSurePathExists(File fe) throws IOException {
		if(!fe.exists()){
			//fe is a directory
			if(fe.getName().endsWith("/")){
				if(!fe.mkdirs()){
					throw new IOException("could not create folder structure: " + fe.getName());
				}

			}
			//fe is a file
			else{
				//create parent folder
				File parentFolder = fe.getParentFile();
				if(!parentFolder.exists()){
					if(!parentFolder.mkdirs()){
						throw new IOException("could not create folder structure: " + fe.getName());
					}
				}
			}
		}
	}

	/**
	 * @param zis
	 * @param destination must be an existing file
	 * @throws Exception
	 */
	private static void unzipFileStream(ZipInputStream zis, File destination) throws Exception {
		FileOutputStream fos = null;

		try{
			byte[] buffer = new byte[10240]; //10 kilo byte
			int count;

			fos = new FileOutputStream(destination);
			while((count = zis.read(buffer)) != -1){
				fos.write(buffer, 0, count);
			}
		}
		catch(Exception ex){
			throw new Exception("could not unzip to: " + destination.getPath() + ": " + ex.getMessage());
		}
		finally{
			try {
				if(fos != null) {
					fos.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}

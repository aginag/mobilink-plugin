package com.mscmobile.mscmobilinkplugin;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class LocationBackgroundService extends Service {
    private static final String TAG = "MyLocationService";
    private static final int LOCATION_INTERVAL = 15000;
    private static final float LOCATION_DISTANCE = 5;
    private static final String LOCATION_BROADCAST_PERMISSION = "com.mscmobile.mscmobilinkplugin.GeoTrackingService.RECEIVE_BROADCAST";

    public static final int MY_PERMISSION_ACCESS_COURSE_LOCATION = 373;
    public static final String LOCATION_ACTION_BROADCAST = LocationBackgroundService.class.getName();
    public static final String STOPFOREGROUND_ACTION = LocationBackgroundService.class.getName() + "stopForeground";
    public static final String EXTRA_LATITUDE = "MSC_EXTRA_LATITUDE";
    public static final String EXTRA_LONGITUDE = "MSC_EXTRA_LONGITUDE";
    public static final String EXTRA_INTERVAL = "MSC_EXTRA_INTERVAL";

    private static final int TIME_THRESHOLD = 1000 * 60 * 1;
    private static final int ACCURACY_THRESHOLD = 5;

    private LocationManager mLocationManager = null;
    private int customLocationInterval = LOCATION_INTERVAL;
    private Location currentBestLocation = null;

    private final LocationListener gpsLocationListener = new LocationListener() {
    @Override
    public void onLocationChanged(Location location) {
    Log.v(TAG, "GPS Location received: " + location);
    sendLocationToListeners(location);
}

@Override
public void onStatusChanged(String s, int i, Bundle bundle) {
Log.v(TAG, "onStatusChanged");
}

@Override
public void onProviderEnabled(String s) {
Log.v(TAG, "onGPSProviderEnabled");
}

@Override
public void onProviderDisabled(String s) {
Log.v(TAG, "onGPSProviderDisabled");
}
};

private final LocationListener networkLocationListener = new LocationListener() {
@Override
public void onLocationChanged(Location location) {
Log.v(TAG, "Network Location received: " + location);
sendLocationToListeners(location);
}

@Override
public void onStatusChanged(String s, int i, Bundle bundle) {
Log.v(TAG, "onStatusChanged");
}

@Override
public void onProviderEnabled(String s) {
Log.v(TAG, "onNetworkProviderEnabled");
}

@Override
public void onProviderDisabled(String s) {
Log.v(TAG, "onNetworkProviderDisabled");
}
};

@Override
public IBinder onBind(Intent arg0) {
return null;
}

@Override
public int onStartCommand(Intent intent, int flags, int startId) {
Log.v(TAG, "onStartCommand");

if (intent.getAction() != null && intent.getAction().equals(STOPFOREGROUND_ACTION)) {
Log.v(TAG, "Received Stop Foreground Intent ");
stopForeground(true);
stopSelf();
} else {
Bundle extras = intent.getExtras();

if (extras != null) {
this.customLocationInterval = extras.getInt(EXTRA_INTERVAL);
Log.v(TAG, "Using custom location interval: " + this.customLocationInterval);
this.removeLocationUpdates();
this.requestLocationUpdates();
}
}


return START_STICKY;
}

@Override
public void onCreate() {
Log.v(TAG, "onCreate");
super.onCreate();
int NOTIFICATION_ID = (int) (System.currentTimeMillis() % 10000);

if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
startMyOwnForeground(NOTIFICATION_ID);
} else {
startForeground(NOTIFICATION_ID, new Notification.Builder(this).build());
}

sendLocationToListeners(null);
initializeLocationManager();
this.requestLocationUpdates();
}

@Override
public void onDestroy() {
Log.v(TAG, "onDestroy");
super.onDestroy();
this.removeLocationUpdates();
}

@RequiresApi(api = Build.VERSION_CODES.O)
private void startMyOwnForeground(int notificationId) {
String NOTIFICATION_CHANNEL_ID = "com.mscmobile.mscmobilinkplugin.LocationBackgroundService";
String channelName = "MSC Mobilink Plugin Location Background Service";
NotificationChannel chan = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_NONE);
chan.setLightColor(Color.BLUE);
chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
assert manager != null;
manager.createNotificationChannel(chan);

NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
Notification notification = notificationBuilder.setOngoing(true)
.setContentTitle("App is running in background")
.setPriority(NotificationManager.IMPORTANCE_MIN)
.setCategory(Notification.CATEGORY_SERVICE)
.build();
startForeground(notificationId, notification);
}

private void initializeLocationManager() {
Log.v(TAG, "initializeLocationManager - LOCATION_INTERVAL: " + this.customLocationInterval + " LOCATION_DISTANCE: " + LOCATION_DISTANCE);
if (mLocationManager == null) {
mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
}
}

private void removeLocationUpdates() {
if (mLocationManager != null) {
Log.v(TAG, "Removing location updates");
mLocationManager.removeUpdates(this.gpsLocationListener);
mLocationManager.removeUpdates(this.networkLocationListener);
}
}

private void requestLocationUpdates() {
if (mLocationManager == null) {
return;
}

Log.v(TAG, "Requesting location updates");
try {
mLocationManager.requestLocationUpdates(
LocationManager.GPS_PROVIDER,
this.customLocationInterval,
LOCATION_DISTANCE,
gpsLocationListener
);

mLocationManager.requestLocationUpdates(
LocationManager.NETWORK_PROVIDER,
this.customLocationInterval,
LOCATION_DISTANCE,
networkLocationListener
);
} catch (java.lang.SecurityException ex) {
Log.e(TAG, "fail to request location update, ignore", ex);
} catch (IllegalArgumentException ex) {
Log.e(TAG, "network provider does not exist, " + ex.getMessage());
}
}

private void sendLocationToListeners(Location location) {
if (location == null) {
return;
}

if (!this.isBetterLocation(location, this.currentBestLocation)) {
return;
}

this.currentBestLocation = location;

Log.v(TAG, "Sending info");

Intent intent = new Intent();
intent.setAction(LOCATION_ACTION_BROADCAST);
intent.putExtra(EXTRA_LATITUDE, location.getLatitude());
intent.putExtra(EXTRA_LONGITUDE, location.getLongitude());

sendBroadcast(intent, LOCATION_BROADCAST_PERMISSION);
}

/** Determines whether one Location reading is better than the current Location fix
 * @param location  The new Location that you want to evaluate
 * @param currentBestLocation  The current Location fix, to which you want to compare the new one
 */
protected boolean isBetterLocation(Location location, Location currentBestLocation) {
if (currentBestLocation == null) {
// A new location is always better than no location
Log.v(TAG, "Pass - New Location ");
return true;
}

// Check whether the new location fix is newer or older
long timeDelta = location.getTime() - currentBestLocation.getTime();
boolean isSignificantlyNewer = timeDelta > TIME_THRESHOLD;
boolean isSignificantlyOlder = timeDelta < -TIME_THRESHOLD;
boolean isNewer = timeDelta > 0;

// If it's been more than two minutes since the current location, use the new location
// because the user has likely moved
if (isSignificantlyNewer) {
Log.v(TAG, "Pass - isSignificantlyNewer ");
return true;
// If the new location is more than two minutes older, it must be worse
} else if (isSignificantlyOlder) {
Log.v(TAG, "Fail - isSignificantlyOlder ");
return false;
}

// Check whether the new location fix is more or less accurate
int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
boolean isLessAccurate = accuracyDelta > 0;
boolean isMoreAccurate = accuracyDelta < 0;
boolean isSignificantlyLessAccurate = accuracyDelta > ACCURACY_THRESHOLD;

// Check if the old and new location are from the same provider
boolean isFromSameProvider = isSameProvider(location.getProvider(),
currentBestLocation.getProvider());

Log.v(TAG, "accuracyDelta: " + accuracyDelta);
Log.v(TAG, "isLessAccurate: " + isLessAccurate);
Log.v(TAG, "isMoreAccurate: " + isMoreAccurate);
Log.v(TAG, "isSignificantlyLessAccurate: " + isSignificantlyLessAccurate);
Log.v(TAG, "isFromSameProvider: " + isFromSameProvider);

// Determine location quality using a combination of timeliness and accuracy
if (isMoreAccurate) {
Log.v(TAG, "Pass - isMoreAccurate ");
return true;
} else if (isNewer && !isLessAccurate) {
Log.v(TAG, "Pass - isNewer && !isLessAccurate ");
return true;
} else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
Log.v(TAG, "Pass - isNewer && !isSignificantlyLessAccurate && isFromSameProvider ");
return true;
}

Log.v(TAG, "Fail ");
return false;
}

/** Checks whether two providers are the same */
private boolean isSameProvider(String provider1, String provider2) {
if (provider1 == null) {
return provider2 == null;
}
return provider1.equals(provider2);
}
}


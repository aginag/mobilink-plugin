package com.mscmobile.mscmobilinkplugin;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Handler;
import android.print.MscPdfPrint;
import android.print.PrintAttributes;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;

@TargetApi(21)
public class MscHTML2PDF {
    private static final String LOG_TAG = "Html2Pdf";
    private final CordovaInterface cordova;
    private final CordovaWebView webView;
    private CallbackContext callbackContext;
    
    private File applicationFilesFolder;
    private String fileName;
    // set to true to see the webview (useful for debugging)
    private final boolean showWebViewForDebugging = false;
    
    public MscHTML2PDF(CordovaInterface cordova, CordovaWebView webView, CallbackContext callbackContext, File applicationFilesFolder) {
        this.cordova = cordova;
        this.webView = webView;
        this.applicationFilesFolder = applicationFilesFolder;
        this.callbackContext = callbackContext;
    }
    
    public PluginResult createPDF(JSONArray args) {
        
        try {
            if (args.getString(1) != null && args.getString(1) != "null")
                this.fileName = args.getString(1);
            
            final MscHTML2PDF self = this;
            final String content = args.optString(0, "<html></html>");
            this.callbackContext = callbackContext;
            
            cordova.getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    self.loadContentIntoWebView(content);
                }
            });
            
        }catch(JSONException e){
            
        }
        
        // send "no-result" result to delay result handling
        PluginResult pluginResult = new  PluginResult(PluginResult.Status.NO_RESULT);
        pluginResult.setKeepCallback(true);
        return pluginResult;
        
    }
    
    // --------------------------------------------------------------------------
    // LOCAL METHODS
    // --------------------------------------------------------------------------
    
    
    /**
     * Loads the html content into a WebView, saves it as a single multi page pdf file and
     * calls startPdfApp() once it´s done.
     */
    private void loadContentIntoWebView (String content)
    {
        Activity ctx = cordova.getActivity();
        final WebView page = new WebView(ctx);
        final MscHTML2PDF self = this;
        
        if( showWebViewForDebugging )
        {
            page.setVisibility(View.VISIBLE);
        } else {
            page.setVisibility(View.INVISIBLE);
        }
        page.getSettings().setJavaScriptEnabled(false);
        page.setDrawingCacheEnabled(true);
        // Don´t auto-scale the content to the webview's width.
        page.getSettings().setLoadWithOverviewMode(false);
        page.getSettings().setUseWideViewPort(false);
        page.setInitialScale(100);
        // Disable android text auto fit behaviour
        page.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        
        page.setWebViewClient(this.getWebViewClient());
        
        // Set base URI to the assets/www folder
        String baseURL = webView.getUrl();
        baseURL = baseURL.substring(0, baseURL.lastIndexOf('/') + 1);
        
        /** We make it this small on purpose (is resized after page load has finished).
         *  Making it small in the beginning has some effects on the html <body> (body
         *  width will always remain 100 if not set explicitly).
         */
        if( !showWebViewForDebugging )
        {
            ctx.addContentView(page, new ViewGroup.LayoutParams(100, 100));
        }
        else
        {
            ctx.addContentView(page, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        }
        page.loadDataWithBaseURL(baseURL, content, "text/html", "utf-8", null);
    }
    
    /**
     * Returns the web client.
     */
    private WebViewClient getWebViewClient() {
        final MscHTML2PDF self = this;
        
        return  new WebViewClient() {
            @Override
            public void onPageFinished(final WebView page, String url) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run()
                    {
                        
                        // generate the pdf
                        self.createWebPrintJob(page);
                        
                        PluginResult result;
                        // send success result to cordova
                        result = new PluginResult(PluginResult.Status.OK);
                        result.setKeepCallback(false);
                        self.callbackContext.sendPluginResult(result);
                    }
                }, 500);
            }
        };
    }
    
    /**
     * Generates pdf and saves it within the applications files folder
     */
    private void createWebPrintJob(WebView webView) {
        String jobName = "MSC PRINT PDF JOB";
        PrintAttributes attributes = new PrintAttributes.Builder()
        .setMediaSize(PrintAttributes.MediaSize.ISO_A4)
        .setResolution(new PrintAttributes.Resolution("pdf", "pdf", 600, 600))
        .setMinMargins(PrintAttributes.Margins.NO_MARGINS).build();
        MscPdfPrint pdfPrint = new MscPdfPrint(attributes);
        pdfPrint.print(webView.createPrintDocumentAdapter(jobName), this.applicationFilesFolder, this.fileName);
    }
}

package com.mscmobile.mscmobilinkplugin;

import android.util.Log;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;

/**
 * Created by Klaus on 25.10.2016.
 */

class PerformanceTimer {
    private static String _defaultLogTag = "PerformanceTimer";
    private Date start = null;
    private Date interim = null;
    private Date end = null;
    private String logTag = "";

    public PerformanceTimer() {
        logTag = _defaultLogTag;
    }

    public PerformanceTimer(String logTag) {
        this.logTag = logTag;
    }

    public void start() {
        start = new Date();
        interim = new Date();
    }

    /**
     * returns the interim (from the previous interim) in milliseconds
     *
     * @return
     */
    public long interim() {
        Date newInterim = new Date();
        long duration = diff(interim, newInterim);
        interim = newInterim;
        return duration;
    }

    public long interim(String message) {
        long duration = interim();
        Log.d(logTag, message + " " + duration);
        return duration;
    }

    public void stop() {
        end = new Date();
        Log.d(logTag, "stopped; elapsed time in ms: " + getDuration());
    }

    /**
     * returns the duration from start to stop in milliseconds
     *
     * @return
     */
    public long getDuration() {
        return diff(start, end);
    }

    private long diff(Date start, Date end) {
        long diffInMs = end.getTime() - start.getTime();
        return diffInMs;
    }
}

class Util {
    /**
     * returns the exception's stacktrace as a human readable string
     *
     * @param ex
     * @return
     */
    static String getExceptionStackTrace(Exception ex) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        String result = sw.toString();
        //remove newline char from end of string
        //String result1 = result.substring(0,result.lastIndexOf('\n'));
        return result;
    }
}

final class Tests {
    private static CordovaInterface _cordova = null;
    private static UlWrapper _ulWrapper = null;
    private static Logger _logger = null;

    private Tests() {
    }

    static void initialize(CordovaInterface cordova, UlWrapper ulWrapper, Logger logger) {
        _cordova = cordova;
        _ulWrapper = ulWrapper;
        _logger = logger;
    }

    static void testMultithreadedDbAccess(JSONArray args,
                                          final CallbackContext callbackContext) {
        final int loops = 200;
        final String query = "Select * from ACTIVITY_F";
        final Thread[] threads = new Thread[loops];
        PluginResult pr = new PluginResult(PluginResult.Status.OK, "starting #threads: " + loops);
        pr.setKeepCallback(true);
        callbackContext.sendPluginResult(pr);
        final PerformanceTimer pt = new PerformanceTimer();
        pt.start();
        try {
            _cordova.getThreadPool().execute(new Runnable() {
                public void run() {
                    try {
                        for (int i = 0; i < loops; i++) {
                            final int counter = i;
                            Thread thread = new Thread(new Runnable() {
                                public void run() {
                                    String msg = null;
                                    try {
                                        msg = "running query: " + counter;
                                        //_logger.log(msg, LogLevel.DEBUG);
                                        PluginResult tpr = new PluginResult(PluginResult.Status.OK, msg);
                                        tpr.setKeepCallback(true);
                                        callbackContext.sendPluginResult(tpr);

                                        String result = _ulWrapper.executeQuery(query);

                                        msg = "execute query finished: " + counter;
                                        tpr = new PluginResult(PluginResult.Status.OK, msg);
                                        tpr.setKeepCallback(true);
                                        callbackContext.sendPluginResult(tpr);

                                        //_logger.log(msg, LogLevel.DEBUG);
                                    } catch (Exception ex) {
                                        msg = "thread: " + counter + ": " + ex.toString();
                                        _logger.log(msg, LogLevel.VERBOSE);
                                        PluginResult tpr = new PluginResult(PluginResult.Status.OK, msg);
                                        tpr.setKeepCallback(true);
                                        callbackContext.sendPluginResult(tpr);
                                    }
                                }
                            });
                            threads[i] = thread;
                            thread.start();
                        }

                        for (Thread thread : threads) {
                            thread.join();
                        }
                        pt.stop();
                        callbackContext.success("all threads finished, time elapsed(ms): " + pt.getDuration());

                    } catch (Exception e) {
                        callbackContext.error("error_query");
                    }
                }
            });
        } catch (Exception ex) {
            _logger.log(ex.toString(), LogLevel.VERBOSE);
            callbackContext.error("testMultithreadedDbAccess error: " + ex.toString());
        }
    }
    /*
    static void testConnectionOverhead(){
        int loops = 14;
        Connection[] conns = new Connection[14];
        PerformanceTimer pt = new PerformanceTimer();
        pt.start();
        try{
            for(int i = 0; i < loops; i++){
                Connection conn = _ulWrapper.();
                pt.interim("aquired connection: ");
                conn.release();
                pt.interim("connection released: ");

                //conns[i] = conn;
            }
        }
        catch(Exception ex){
            _logger.log(ex.toString(), LogLevel.DEBUG);
        }
    }
    */

    static void createLogFileEntries() {
        for (int i = 0; i < 10000; i++) {
            String msg = "dummy log file entry: " + i;
            _logger.log(msg, LogLevel.VERBOSE);
        }
    }
}

package com.mscmobile.mscmobilinkplugin;

/**
 * Created by Klaus on 31.01.2017.
 */

/**
 * mimicks behaviour of C# ManualResetEvent class
 * see http://stackoverflow.com/questions/3625967/java-equivalent-of-nets-manualresetevent-and-waithandle
 * answer from Robin Davies;
 */
public class ManualResetEvent {
    private final Object monitor = new Object();
    private volatile boolean open = false;

    public ManualResetEvent(boolean open) {
        this.open = open;
    }

    /**
     * if the monitor is set/open, the calling thread continues operation;
     * if the monitor is closed/reset, the calling thread enters a wait state,
     * until the monitor has been set again
     * @throws InterruptedException
     */
    public void waitOne() throws InterruptedException {
        synchronized (monitor) {
            while (open==false) {
                monitor.wait();
            }
        }
    }

    /**
     * opens the monitor, all waiting threads will be notified and continue execution
     */
    public void set() {//open start
        synchronized (monitor) {
            open = true;
            monitor.notifyAll();
        }
    }

    /**
     * closes the monitor, threads calling waitOne will enter wait state
     */
    public void reset() {//close stop
        synchronized(monitor) {
            open = false;
        }
    }
}
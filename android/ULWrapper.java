package com.mscmobile.mscmobilinkplugin;

import java.io.File;
import java.nio.charset.Charset;
import java.util.*;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.sap.ultralitejni17.ConfigFileAndroid;
import com.sap.ultralitejni17.Connection;
import com.sap.ultralitejni17.DatabaseManager;
import com.sap.ultralitejni17.FileTransfer;
import com.sap.ultralitejni17.PreparedStatement;
import com.sap.ultralitejni17.ResultSet;
import com.sap.ultralitejni17.ResultSetMetadata;
import com.sap.ultralitejni17.StreamHTTPParms;
import com.sap.ultralitejni17.SyncObserver;
import com.sap.ultralitejni17.SyncParms;
import com.sap.ultralitejni17.SyncResult;
import com.sap.ultralitejni17.ULjException;

import android.content.Context;
import android.util.Log;

//region helper classes
enum UlUpdateType {
    UPDATE,
    INSERT,
    DELETE
}

class UpdateInsertResult {
    public String NewMainObjectUuid = null;
    public String NewMainObjectTable = null;
}
//endregion

/**
 * for documentation on the Ultralite library see:
 * <SQL Anywhere 17 installation directory>\UltraLite\UltraLiteJ\Android\Html
 * <p>
 * sql anywhere 17 error messages by sql error code:
 * http://dcx.sap.com/index.html#sqla170/en/html/81133cb16ce210148ebed01b8c288d75.html
 * <p>
 * MobiLink server error messages sorted by error code:
 * http://dcx.sap.com/index.html#sqla170/en/html/80f5fb086ce21014ba1bec2569d4de5c.html
 * <p>
 * MobiLink communication error messages sorted by error code:
 * http://dcx.sap.com/index.html#sqla170/en/html/80f5d2ed6ce210149fe6964b2cbd8a16.html
 */
class UlWrapper {

    private final Object _syncLock = new Object();
    private Object _initLock = new Object();
    private Context _context = null;
    private Logger _logger = null;
    private Logger _transactionLogger = null;
    private MlConnectionInfo _conInfo = null;
    private int _protocol = 0;
    private ConfigFileAndroid _config = null;
    private UlConnectionManager _conManager = null;
    private UlFileTransferManager _ftManager = UlFileTransferManager.getInstance();
    public volatile boolean _syncInProgress = false;
    private volatile boolean _syncCancelRequest = false;
    private String _newObjectUuid = null;

    public UlWrapper(Context context, Logger logger, Logger transactionLogger,
                     MlConnectionInfo conInfo) throws Exception {
        _context = context;
        _logger = logger;
        _transactionLogger = transactionLogger;
        _conInfo = conInfo;

        switch (conInfo.protocol.toUpperCase()) {
            case "HTTP":
                _protocol = SyncParms.HTTP_STREAM;
                break;
            case "HTTPS":
                _protocol = SyncParms.HTTPS_STREAM;
                break;
            case "TCPIP":
                _protocol = SyncParms.TCPIP_STREAM;
                break;
            default:
                String msg = String.format("UlWrapper: invalid protocol: " + _protocol);
                _logger.log(msg, LogLevel.ERROR);
                throw new Exception(msg);
        }
    }

    public String getNewObjectUuid() {
        return _newObjectUuid;
    }

    //region initialize
    void close(boolean hardCloseConnections) throws InterruptedException {
        synchronized (_initLock) {
            if (_conManager != null) {
                try {
                    _conManager.close(hardCloseConnections);
                } finally {
                    _conManager = null;
                }
            }
        }
    }

    void setMlConnectionInfo(MlConnectionInfo conInfo) {
        _conInfo = conInfo;
    }

    void createDbConnections(String udbKey) throws Exception {
        _config = DatabaseManager.createConfigurationFileAndroid(_conInfo.dbFilePath, _context);
        if (udbKey != null) {
            _config.setEncryptionKey(udbKey);
        }
        synchronized (_initLock) {
            if (_conManager != null) {
                throw new IllegalAccessError("Cannot create new DB connections. Close existing ones first.");
            }
            _conManager = new UlConnectionManager(_config);
        }
    }

    void closeDbConnections(boolean hardClose) throws InterruptedException {
        synchronized (_initLock) {
            if (_conManager != null) {
                _conManager.close(hardClose);
                _conManager = null;
            }
        }
    }
    //endregion

    //region DB sync

    /**
     * @param user
     * @param pwd
     * @param publication
     * @param cbContext   optional: if null, no progress is information is returned
     * @throws Exception
     */
    public Integer syncDb(String user, String pwd, String publication, final CallbackContext cbContext,final String syncMode, final boolean pingMode)
            throws Exception {
        Connection conn = null;
        Integer authenticationCode = 0;
        Boolean isAnotherSyncRunning = false;

        MscSyncObserver syncObserver = new MscSyncObserver() {

            @Override
            public boolean syncProgress(int i, SyncResult sResult) {
                //in case a callback context was provided we provide progress info
                if (cbContext != null) {
                    int state = i;
                    JSONObject pInfo = new JSONObject();
                    try {
                        pInfo.put("State", convertSyncStateToText(i));
                        pInfo.put("TableCount", sResult.getTotalTableCount());
                        pInfo.put("TableIndex", sResult.getSyncedTableCount());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (i == States.RECEIVING_TABLE && !downloadStarted) {
                        downloadStarted = true;
                    }

                    if (i == States.COMMITTING_DOWNLOAD && !downloadFinished) {
                        downloadFinished = true;
                    }

                    //PluginResult pr = new PluginResult(PluginResult.Status.OK, pInfo.toString());
                    PluginResult pr = new PluginResult(PluginResult.Status.OK, pInfo);
                    pr.setKeepCallback(true);
                    cbContext.sendPluginResult(pr);
                }

                _logger.log(String.format("Current sync state: %s ", i),
                        LogLevel.VERBOSE);
                //return false: continue sync; true: abort sync
                return _syncCancelRequest;
            }
        };

        try {
            synchronized (_syncLock) {

                if (_syncInProgress) {
                    isAnotherSyncRunning = true;
                    throw new IllegalAccessError("Cannot start new DB sync as another sync is still in progress");
                }
                _syncCancelRequest = false;
                _syncInProgress = true;
            }

            _logger.log(String.format("Synchronizing DB for user: %s ", user), LogLevel.INFO);

            String version = this.getVersionFromUdb(publication);

            conn = getDbConnection();
            SyncParms syncParams = conn.createSyncParms(_protocol, user, version);
            syncParams.getStreamParms().setHost(_conInfo.hostName);
            syncParams.getStreamParms().setPort(_conInfo.port);

            if (_conInfo.urlSuffix != null && _conInfo.urlSuffix.length() > 0) {
                syncParams.getStreamParms().setExtraParameters(String.format("url_suffix=%s", _conInfo.urlSuffix));
            }

            syncParams.setPassword(pwd);
            syncParams.setPingOnly(pingMode);
            syncParams.setPublications(publication);
            //syncParams.setAuthenticationParms("syncMode="+syncMode);
            syncParams.setAuthenticationParms(syncMode+",");

            //            String customHeader = String.format("url_suffix=%s;custom_header=%s %s;", _urlSuffix,
            //                    _customHeaderName, token);
            //            syncParams.getStreamParms().setExtraParameters(customHeader);
            _logger.log("sync params:  " + syncParams.getStreamParms().getHost() + ", " + syncParams.getStreamParms().getPort() + ", " + syncParams.getPublications(), LogLevel.VERBOSE);

            if(!pingMode) {
                conn.setSyncObserver(syncObserver);
            }
            conn.synchronize(syncParams); //blocking

            _logger.log(String.format("Successfully synchronized DB for user: %s ", user),
                    LogLevel.INFO);
        } catch (ULjException ex) {
            if (syncObserver.downloadStarted && !syncObserver.downloadFinished) {
                // Sync got interrupted during download stage.
                _logger.log("Sync got interrupted during download stage. Rolling back partial download.",
                        LogLevel.INFO);
                conn.rollbackPartialDownload();
            }

            conn.rollback();//just to be on the safe side as the connection is reused
            String message = "syncDb failed: " + ex.getMessage();
            handleError(ex, "syncDb failed: ");
        } catch (IllegalAccessError illegalAccessError) {
            String message = "syncDb failed: " + illegalAccessError.getMessage();
            Exception ex = new Exception(message);
            handleError(ex, "syncDb failed: ");
        } finally {
            if (conn != null) {
                if (conn.getSyncResult().getAuthValue() >= 2000) {
                    authenticationCode = conn.getSyncResult().getAuthValue();
                } else {
                    authenticationCode = conn.getSyncResult().getStreamErrorCode() != 0 ? conn.getSyncResult().getStreamErrorCode() : conn.getSyncResult().getAuthValue();
                }

                conn.setSyncObserver(null);
                releaseDbConnection(conn);
            }

            if (isAnotherSyncRunning) {
                authenticationCode = 5002;
            }

            synchronized (_syncLock) {
                _syncInProgress = false;
            }

            _syncCancelRequest = false;
        }

        return authenticationCode;
    }

    public void cancelSync() throws Exception {
        try {
            synchronized (_syncLock) {
                if (_syncInProgress) {
                    _syncCancelRequest = true;
                }
            }
        } catch (Exception ex) {
            String msg = handleError(ex, "cancelSync error: ");
            throw new Exception(msg);
        }
    }
    //endregion

    //region file transfer
    public long transferFile(ULTransferType type, String user, String pwd,
                             String version, final String fileId, String fileName, File folder,
                             final CallbackContext callbackContext) throws Exception {
        UlFileTransfer fileTransferInfo = null;
        long authenticationCode = 0;

        try {
            FileTransfer fileTransfer = DatabaseManager.createFileTransferAndroid(_context,
                    fileName, _protocol, user, version);
            fileTransfer.getStreamParms().setHost(_conInfo.hostName);
            fileTransfer.getStreamParms().setPort(_conInfo.port);

            if (_conInfo.urlSuffix != null && _conInfo.urlSuffix.length() > 0) {
                fileTransfer.getStreamParms().setExtraParameters(String.format("url_suffix=%s", _conInfo.urlSuffix));
            }

            fileTransfer.setLocalPath(folder.getPath());
            fileTransfer.setLocalFileName(fileName);
            fileTransfer.setPassword(pwd);
            fileTransfer.getStreamParms().setZlibCompression(true);
            fileTransfer.setResumePartialTransfer(true);

            fileTransferInfo = new UlFileTransfer(fileTransfer, type, fileId, callbackContext);

            String message = null;
            //Download
            if (type == ULTransferType.Download) {
                message = String.format("Downloading (server: %s version: %s) file: %s  to: %s",
                        _conInfo.hostName, version, fileName, folder.getAbsolutePath());
                _logger.log(message, LogLevel.INFO);
            }
            //Upload
            else {
                message = String.format("Uploading (server: %s version: %s) file: %s  from: %s",
                        _conInfo.hostName, version, fileName, folder.getAbsolutePath());
                _logger.log(message, LogLevel.INFO);
            }

            _ftManager.transferFile(fileTransferInfo); //blocking

            message = "Successfully transferred file: " + fileName;
            _logger.log(message, LogLevel.INFO);
        } catch (ULjException ex) {
            String message = "syncDb failed: " + ex.getMessage();
        } catch (Exception ex) {
            handleError(ex, "");
        } finally {
            if (fileTransferInfo != null) {
                if (fileTransferInfo.getFileTransfer().getAuthValue() >= 2000) {
                    authenticationCode = fileTransferInfo.getFileTransfer().getAuthValue();
                } else if (fileTransferInfo.getFileTransfer().getFileAuthCode() >= 2000) {
                    authenticationCode = fileTransferInfo.getFileTransfer().getFileAuthCode();
                } else {
                    authenticationCode = fileTransferInfo.getFileTransfer().getStreamErrorCode() != 0 ? fileTransferInfo.getFileTransfer().getStreamErrorCode() : fileTransferInfo.getFileTransfer().getAuthValue();
                }
            }
        }

        return authenticationCode;
    }

    public void cancelFileTransfer(String fileId) throws Exception {
        _ftManager.cancelFileTransfer(fileId);
    }
    //endregion

    //region execute query
    public String executeQuery(String stmt) throws Exception {
        JSONArray queryResult = executeQueryAsJsonArray(stmt, true);
        return queryResult.toString();
    }

    public JSONArray executeQueryAsJsonArray(String stmt, boolean log) throws Exception {
        JSONArray result = null;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            PerformanceTimer pt = new PerformanceTimer();
            pt.start();

            String msg = "executeQueryAsJsonArray: " + stmt;
            if (log) {
                _logger.log(msg, LogLevel.VERBOSE);
                pt.interim("logged sql statement: ");
            }

            conn = getDbConnection();
            pt.interim("aquired connection: ");

            ps = conn.prepareStatement(stmt);
            pt.interim("prepared Statement: ");

            rs = ps.executeQuery();
            pt.interim("executed Query: ");

            result = convertResultSetToJsonInternal(rs);
            pt.interim("converted Result Set: ");
        } catch (Exception ex) {
            handleError(ex, "executeQueryAsJsonArray error:");
            throw ex;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            releaseDbConnection(conn);
        }
        return result;
    }

    /**
     * @param stmts expected structure: [{'tableName:', statement:''}, {}]
     * @return
     * @throws Exception
     */
    public JSONObject executeQueriesSequentially(JSONArray stmts) throws Exception {
        ResultSet rs = null;
        PreparedStatement ps = null;
        Connection conn = null;
        JSONObject result = new JSONObject();
        PerformanceTimer pt = new PerformanceTimer();
        final String tableName = "tableName";
        final String statement = "statement";

        try {
            String msg = String.format("executeQueriesSequentially #queries: %d", stmts.length());
            _logger.log(msg, LogLevel.VERBOSE);

            pt.start();
            pt.interim("start executeQueriesSequentially, number of queries: " + stmts.length());

            conn = getDbConnection();
            //pt.interim("aquired connection: ");
            for (int i = 0; i < stmts.length(); i++) {
                JSONObject jsonObject = stmts.getJSONObject(i);
                String tbName = jsonObject.getString(tableName);
                String stmt = jsonObject.getString(statement);

                msg = String.format("executing query: %d: %s", i + 1, stmt);
                _logger.log(msg, LogLevel.VERBOSE);
                //pt.interim("logged sql statement: ");

                ps = conn.prepareStatement(stmt);
                pt.interim("prepareStatement query: ");
                if (ps == null) {
                    throw new Exception("Failed preparing statement: " + stmts.getString(i));
                }
                rs = ps.executeQuery();
                //pt.interim("executing query finished; elapsed: ");

                result.put(tbName, convertResultSetToJsonInternal(rs));
                //result.put(convertResultSetToJsonInternal(rs));
                //pt.interim("converting resultset finished; elapsed: ");

                rs.close();
                rs = null;
                ps.close();
                ps = null;
            }
            pt.interim("end executeQueriesSequentially");
            pt.stop();
        } catch (Exception e) {
            handleError(e, "executeQueriesSequentially error:");
            throw e;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            releaseDbConnection(conn);

        }
        return result;
    }
    //endregion

    //region update queries
    public void executeUpdate(String sql) throws Exception {
        PreparedStatement ps = null;
        Connection conn = null;

        try {
            conn = getDbConnection();
            String msg = "executeUpdate: " + sql;
            _logger.log(msg, LogLevel.VERBOSE);
            _transactionLogger.log(msg, LogLevel.VERBOSE);

            ps = conn.prepareStatement(sql);
            if (ps.execute()) {
                conn.commit();
            } else {
                conn.rollback();
                throw new Exception("executeUpdate failed for: " + sql);
            }
        } catch (Exception e) {
            if (conn != null) {
                conn.rollback();
            }
            handleError(e, "executeUpdate error", true);
            throw e;
        } finally {
            if (ps != null) {
                ps.close();
            }
            releaseDbConnection(conn);
        }
    }

    public void executeUpdatesSequentially(JSONArray stmts) throws Exception {
        Connection conn = null;
        PreparedStatement ps = null;
        String msg = null;

        _conManager.close(true);

        try {
            msg = String.format("executeUpdatesSequentially #queries: %d", stmts.length());
            _logger.log(msg, LogLevel.INFO);
            _transactionLogger.log(msg, LogLevel.INFO);

            conn = this.getSingleDbConnection();
            for (int i = 0; i < stmts.length(); i++) {
                String stmt = stmts.getString(i);

                msg = String.format("executing update: %d: %s", i + 1, stmt);
                _logger.log(msg, LogLevel.INFO);

                ps = conn.prepareStatement(stmt);

                if (!ps.execute()) {
                    conn.rollback();
                    throw new Exception("failed executing statement: " + stmts.getString(i));
                }

                ps.close();
                ps = null;
            }

            conn.commit();
        } catch (Exception e) {
            if (conn != null) {
                conn.rollback();
            }
            handleError(e, "executeUpdatesSequentially error", true);
            throw e;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.release();
            }

            this.closeDbConnections(true);
            this.createDbConnections(null);
        }
    }

    public void executeStatementsAndCommit(JSONArray stmts) throws Exception {
        Connection conn = null;
        String mainObjectUuid = null;
        String mainObjectTable = null;
        _newObjectUuid = "";
        String msg;

        try {
            msg = "executeStatementsAndCommit: " + stmts;
            _logger.log(msg, LogLevel.VERBOSE);
            _transactionLogger.log(msg, LogLevel.VERBOSE);

            conn = getDbConnection();
            for (int i = 0; i < stmts.length(); i++) {
                JSONObject query = stmts.getJSONObject(i);
                String type = query.getString("type");
                UlUpdateType updateType = UlUpdateType.valueOf(type.toUpperCase());
                boolean isMainTable = query.getBoolean("mainTable");
                String tableName = query.getString("tableName");
                String whereConditions = query.getString("whereConditions");
                JSONArray values = query.getJSONArray("values");
                JSONArray replacementValues = query.getJSONArray("replacementValues");
                JSONArray mainObjectValues = query.getJSONArray("mainObjectValues");
                UpdateInsertResult result = new UpdateInsertResult();

                executeUpdateInsert(conn, updateType, isMainTable, tableName, whereConditions,
                        values, replacementValues, mainObjectValues, mainObjectUuid, mainObjectTable,
                        result);
                if (result.NewMainObjectTable != null) {
                    mainObjectTable = result.NewMainObjectTable;
                }
                if (result.NewMainObjectUuid != null) {
                    mainObjectUuid = result.NewMainObjectUuid;
                }
            }
            conn.commit();
        } catch (Exception ex) {
            msg = "executeStatementsAndCommit failed";
            msg = handleError(ex, msg, true);
            if (conn != null) {
                conn.rollback();
            }
            throw new Exception(msg, ex);
        } finally {
            releaseDbConnection(conn);
        }
    }

    private void executeUpdateInsert(Connection conn, UlUpdateType updateType, boolean isMainTable,
                                     String tableName, String whereConditions, JSONArray values,
                                     JSONArray replacementValues, JSONArray mainObjectValues,
                                     String mainObjectUuid, String mainObjectTable,
                                     UpdateInsertResult result) throws Exception {
        PreparedStatement ps = null;
        String sql = null;
        String uuid = null;
        String msg = null;

        try {
            switch (updateType) {
                case DELETE:
                    sql = String.format("DELETE FROM %s WHERE %s", tableName, whereConditions);
                    break;

                case UPDATE:
                    sql = String.format("UPDATE %s SET ", tableName);
                    for (int i = 0; i < values.length(); i++) {
                        JSONArray value = values.getJSONArray(i);
                        sql += value.getString(0) + "=?, ";
                    }
                    //remove last ","
                    sql = sql.substring(0, sql.length() - 2);
                    sql += " WHERE " + whereConditions;
                    break;

                case INSERT:
                    //get new UUID
                    String query = "SELECT NEWID() as new_uuid";
                    JSONObject queryResult = executeQueryAsJsonArray(query, false).getJSONObject(0);
                    uuid = queryResult.getString("new_uuid");
                    _newObjectUuid = uuid;
                    sql = String.format("INSERT INTO %s (mobile_id, ", tableName);

                    String valuesString = "";
                    for (int i = 0; i < replacementValues.length(); i++) {
                        JSONArray value = replacementValues.getJSONArray(i);
                        sql += value.getString(0) + ", ";
                        valuesString += "?, ";
                    }
                    for (int i = 0; i < values.length(); i++) {
                        JSONArray value = values.getJSONArray(i);
                        sql += value.getString(0) + ", ";
                        valuesString += "?, ";
                    }

                    //remove last ','
                    sql = sql.substring(0, sql.length() - 2);
                    valuesString = valuesString.substring(0, valuesString.length() - 2);
                    sql += ") Values('" + uuid + "', ";
                    sql += valuesString + ")";
                    if (isMainTable) {
                        mainObjectUuid = uuid;
                        mainObjectTable = tableName;
                    }
                    break;
            }

            //set replacementValues and values in prepared statement
            ps = conn.prepareStatement(sql);
            int i, j;
            for (i = 0; i < replacementValues.length(); i++) {
                JSONArray rv = replacementValues.getJSONArray(i);
                String value = rv.getString(1);
                if (value.equals("mobile_id")) {
                    value = uuid;
                }
                //index starts at 1
                ps.set(i + 1, value);
            }
            i = replacementValues.length();
            for (j = 0; j < values.length(); j++) {
                JSONArray v = values.getJSONArray(j);
                String value = v.getString(1);
                int insertPosition = i + j + 1;
                try {
                    //special DB types like TIMESTAMP might throw an exception if not set properly.
                    //in those cases we set them to "null"
                    ps.set(insertPosition, value);
                } catch (Exception ex) {
                    ps.setNull(insertPosition);
                    msg = String.format("could not insert value in prepared statement: query: %s; position %s; error: %s",
                            sql, insertPosition, ex.getMessage());
                    _logger.log(msg, LogLevel.ERROR);
                    _transactionLogger.log(msg, LogLevel.ERROR);
                    Log.e("mscmobilinkplugin", msg, ex);
                }
            }

            try {
                if (!ps.execute()) {
                    throw new Exception("executing statement failed");
                }
            } catch (ULjException ex) {
                if (conn != null) {
                    if (ex.getErrorCode() == -193) {
                        executeUpdateForFailedInsert(conn, tableName, values);
                    }
                }
            }
        } catch (Exception ex) {
            msg = "executeUpdateInsert failed: " + ex.getMessage();
            _logger.log(msg, LogLevel.ERROR);
            throw new Exception(msg, ex);
        } finally {
            if (ps != null) {
                ps.close();
            }
            result.NewMainObjectTable = mainObjectTable;
            result.NewMainObjectUuid = mainObjectUuid;
            _newObjectUuid = uuid;
        }
    }

    private void executeUpdateForFailedInsert(Connection conn, String tableName, JSONArray values)
            throws Exception {
        PreparedStatement ps = null;
        String whereClause = "";
        String keyColumnsSql = String.format(
                "SELECT SY.column_name AS column_name,ST.object_id AS TABLE_ID, SY.object_id AS COL_ID,SI.index_name AS index_name " +
                        "FROM syscolumn AS SY INNER JOIN systable AS ST ON SY.table_id = ST.object_id " +
                        "INNER JOIN sysixcol AS SX ON ST.object_id = SX.table_id AND SX.column_id = SY.object_id " +
                        "INNER JOIN sysindex AS SI ON SI.table_id = ST.object_id AND SX.index_id = SI.object_id " +
                        "WHERE SI.type = 'key' AND SI.index_name LIKE '%s' AND ST.table_name = '%s'",
                "%_sapkey", tableName);
        try {
            JSONArray keyColumnsResult = executeQueryAsJsonArray(keyColumnsSql, false);
            for (int i = 0; i < keyColumnsResult.length(); i++) {
                JSONObject keyColumnValue = keyColumnsResult.getJSONObject(i);
                String columnName = keyColumnValue.getString("column_name");
                whereClause += columnName;
                for (int j = 0; j < values.length(); j++) {
                    JSONArray array = values.getJSONArray(j);
                    String value = array.getString(0);
                    if (value.equals(columnName)) {
                        whereClause += String.format("='%s' AND ", array.getString(1));
                        break;
                    }
                }
            }
            //delete last ' AND '
            whereClause = whereClause.substring(0, whereClause.length() - 5);

            String sql = String.format("UPDATE %s SET ", tableName);
            for (int i = 0; i < values.length(); i++) {
                JSONArray value = values.getJSONArray(i);
                sql += value.getString(0) + "=?, ";

            }
            //remove last ','
            sql = sql.substring(0, sql.length() - 2);
            sql += " WHERE " + whereClause;

            //prepare statement and add values
            ps = conn.prepareStatement(sql);
            String value = "''";
            for (int i = 0; i < values.length(); i++) {
                JSONArray array = values.getJSONArray(i);
                value = array.getString(1);
                if (array.getString(0).equals("ACTION_FLAG")) {
                    value = "C";
                }
                int insertPosition = i + 1;
                try {
                    //special DB types like TIMESTAMP might throw an exception if not set properly.
                    //in those cases we set them to "null"
                    ps.set(insertPosition, value);
                } catch (Exception ex) {
                    String msg = String.format("could not insert value in prepared statement: " +
                                    "position %s; setting value to null; error: %s",
                            insertPosition, ex.getMessage());
                    _logger.log(msg, LogLevel.ERROR);
                    Log.e("mscmobilinkplugin", msg, ex);
                    ps.setNull(insertPosition);
                }
            }
            if (!ps.execute()) {
                throw new Exception("executing statement failed ");
            }
        } catch (Exception ex) {
            String defaultMessage = "executeUpdateForFailedInsert failed:";
            String msg = handleError(ex, defaultMessage);
            throw new Exception(msg, ex);
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }
    //endregion

    //region helper functions
    private Connection getDbConnection() throws Exception {
        return _conManager.getConnection();
    }

    private Connection getSingleDbConnection() throws Exception {
        return _conManager.getSingleConnection();
    }

    private void releaseDbConnection(Connection conn) throws Exception {
        _conManager.releaseConnection(conn);
    }

    private String convertResultSetToJson(ResultSet result) throws Exception {
        return convertResultSetToJsonInternal(result).toString();
    }

    /**
     * returns an array of {@link JSONObject}. each JSONObject represents a row in the result table.
     * the structure of the JSONobject is {'columnName':'columnValue'; }
     *
     * @param result
     * @return
     * @throws Exception
     */
    private JSONArray convertResultSetToJsonInternal(ResultSet result) throws Exception {

        ResultSetMetadata rsm = result.getResultSetMetadata();
        int ccount = rsm.getColumnCount();
        JSONArray records = new JSONArray();
        while (result.next()) {
            int j = 1;
            JSONObject row = new JSONObject();
            while (ccount >= j) {

                String columnName = rsm.getAliasName(j);
                if (columnName == null || columnName.length() == 0) {
                    columnName = rsm.getTableColumnName(j);
                }

                String content;
                int type = rsm.getDomainType(j);
                if (type == 12 || type == 10) {
                    byte[] contentAsByteArray = result.getBytes(j);
                    content = new String(contentAsByteArray, Charset.defaultCharset());
                } else {
                    content = result.getString(j);
                }
                if (content == null) {
                    content = "";
                }
                if (columnName == null) {

                    columnName = "";
                }
                row.put(columnName, content);

                j++;
            }
            records.put(row);
        }
        return records;
    }

    private String handleError(Exception ex, String defaultLogMessage) {
        return handleError(ex, defaultLogMessage, false);
    }

    /**
     * logs the full error incl. stack trace
     * returns a user friendly error message:
     * case ULjException: defaultLogMessage + errorCode
     * case Exception: defaultLogMessage + ex.getMessage
     *
     * @param ex
     * @param defaultLogMessage
     * @return
     */
    private String handleError(Exception ex, String defaultLogMessage, boolean logTransactionLog) {
        int errorCode;
        String logMessage = defaultLogMessage + " ";
        String returnMessage = logMessage;

        if (ex != null) {
            //handle Ultralite exception
            if (ex instanceof ULjException) {
                ULjException ulEx = ((ULjException) ex);
                errorCode = ulEx.getErrorCode();
                logMessage += ulEx.getMessage();
                returnMessage += "ultralite error code: " + errorCode;
            } else {
                returnMessage += ex.getMessage();
                logMessage = returnMessage;
            }
            _logger.log(logMessage, LogLevel.ERROR);
            _logger.log(Util.getExceptionStackTrace(ex), LogLevel.VERBOSE);
            if (logTransactionLog) {
                _transactionLogger.log(logMessage, LogLevel.ERROR);
                _transactionLogger.log(Util.getExceptionStackTrace(ex), LogLevel.VERBOSE);
            }
        }
        return returnMessage;
    }

    //region execute query
    public String getPublicationFromUdb() throws Exception {
        JSONArray result = this.executeQueryAsJsonArray("SELECT publication_name FROM syspublication where publication_id = 2", false);

        if (result.length() == 0) {
            throw new Exception("No Publication found on this udb");
        }
        JSONObject publication = (JSONObject) result.get(0);

        return publication.getString("publication_name");
    }

    //region execute query
    public String getVersionFromUdb(String publicationName) throws Exception {
        String stmt = String.format("SELECT SYNC_PROFILE_OPTION_VALUE('%s', 'ScriptVersion')", publicationName);
        JSONArray result = this.executeQueryAsJsonArray(stmt, false);

        if (result.length() == 0) {
            throw new Exception("No ScriptVersion found for this publication");
        }
        JSONObject publication = (JSONObject) result.get(0);

        return publication.getString("");
    }

    private String convertSyncStateToText(Integer state) {
        switch (state) {
            case 0:
                return "STATE_STARTING";
            case 1:
                return "STATE_CONNECTING";
            case 2:
                return "STATE_RESUMING_DOWNLOAD";
            case 3:
                return "STATE_SENDING_HEADER";
            case 4:
                return "STATE_SENDING_CHECK_SYNC_REQUEST";
            case 5:
                return "STATE_WAITING_FOR_CHECK_SYNC_RESPONSE";
            case 6:
                return "STATE_PROCESSING_CHECK_SYNC_RESPONSE";
            case 7:
                return "STATE_SENDING_TABLE";
            case 8:
                return "STATE_SENDING_DATA";
            case 9:
                return "STATE_FINISHING_UPLOAD";
            case 10:
                return "STATE_WAITING_FOR_UPLOAD_ACK";
            case 11:
                return "STATE_PROCESSING_UPLOAD_ACK";
            case 12:
                return "STATE_WAITING_FOR_DOWNLOAD";
            case 13:
                return "STATE_RECEIVING_TABLE";
            case 14:
                return "STATE_RECEIVING_DATA";
            case 15:
                return "STATE_CHECKING_RI";
            case 16:
                return "STATE_COMMITTING_DOWNLOAD";
            case 17:
                return "STATE_REMOVING_ROWS";
            case 18:
                return "STATE_CHECKPOINTING";
            case 19:
                return "STATE_ROLLING_BACK_DOWNLOAD";
            case 20:
                return "STATE_SENDING_DOWNLOAD_ACK";
            case 21:
                return "STATE_DISCONNECTING";
            case 22:
                return "STATE_DONE";
            case 23:
                return "STATE_ERROR";
            default:
                return state.toString();

        }
    }
    //endregion
}

class MscSyncObserver implements SyncObserver {
    boolean downloadStarted = false;
    boolean downloadFinished = false;

    @Override
    public boolean syncProgress(int i, SyncResult syncResult) {
        return false;
    }
}





package com.mscmobile.mscmobilinkplugin;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.cordova.BuildConfig;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.apache.cordova.PluginResult.Status;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.Manifest;
import android.app.Activity;
import android.app.FragmentManagerNonConfig;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.PowerManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.system.ErrnoException;


import com.sap.ultralitejni17.ULjException;


class MlUser {
    private static final String _logFileName = "Log.txt";
    private static final String _transactionLogFileName = "TransactionLog.txt";
    public Logger logger = null;
    public Logger transactionLogger = null;
    public UlWrapper ulWrapper = null;
    private String _name = null;
    private String _pw = null;
    private File _folder = null;
    private File _dbFile = null;
    private MlConnectionInfo _conInfo = null;
    private Context _context = null;


    public MlUser(String name, String pw, File folder, MlConnectionInfo conInfo,
                  Context context) throws ULjException {
        _context = context;
        _name = name;
        _pw = pw;
        _folder = folder;
        setMlConnectionInfo(conInfo);
    }

    public void release() throws Exception {

        if (logger != null) {
            logger.close();
        }
        if (transactionLogger != null) {
            transactionLogger.close();
        }
        if (ulWrapper != null) {
            ulWrapper.close(true);
        }
    }

    /**
     * calls @release then initializes the loggers and @ulWrapper
     *
     * @throws Exception
     */
    public void initialize() throws Exception {
        release();
        logger = new Logger(_folder, _logFileName, LogLevel.VERBOSE);
        transactionLogger = new Logger(_folder, _transactionLogFileName, LogLevel.VERBOSE);
        ulWrapper = new UlWrapper(_context, logger, transactionLogger, _conInfo);
        /*if (initializeCompletely) {
         ulWrapper.createDbConnections();
         }*/
    }

    public void setMlConnectionInfo(MlConnectionInfo conInfo) {
        _conInfo = conInfo;
        if (conInfo != null) {
            _dbFile = new File(_folder, conInfo.dbFileName);
            _conInfo.dbFilePath = _dbFile.getPath();
        }
    }

    public MlConnectionInfo getConnectionInfo() {
        return _conInfo;
    }

    public String getName() {
        return _name;
    }

    public String getPw() {
        return _pw;
    }

    public void setPw(String newPw) {
        this._pw = newPw;
    }

    //public UlWrapper getUlWrapper() {return _ulWrapper;}
    public File getFolder() {
        return _folder;
    }

    public File getAttachmentsFolder() {
        return new File(_folder, "attachments");
    }

    public File getDbFile() {
        return _dbFile;
    }
}

class MlConnectionInfo {
    public String hostName = null;
    public int port = 3439;
    public String publication = null;
    public String version = null;
    public String subscription = null;
    public String protocol = null;
    public String dbFileName = null;
    public String dbFilePath = null;
    public String urlSuffix = null;
}

public class MscMobiLinkPlugIn extends CordovaPlugin {

    //private static final String _usersFolderName = "Users";
    //private static File _usersFolder = null;
    private static File _filesFolder = null;
    private static MlUser _curUser = null;
    private static List<MlUser> _allUsers = null;
    private static MlConnectionInfo _conInfo = null;
    private Activity _activity = null;
    private Context _context = null;
    private GeoTrackingService trackingService = null;

    public MscMobiLinkPlugIn() throws Exception {
    }

    private static String cleanPath(String path) {
        String result = path;
        String fileSuffix = "file:///";

        if (path.startsWith(fileSuffix)) {
            result = path.substring(fileSuffix.length());
        }
        return result;
    }

    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
        _activity = cordova.getActivity();
        _context = _activity.getApplicationContext();
        trackingService = new GeoTrackingService(_context, cordova);
        //create users directory
        //_usersFolder = new File(_context.getFilesDir() + "/files/" + _usersFolderName);
        _filesFolder = new File(_context.getFilesDir() + "/files");

        try {
            _allUsers = getAllUsers();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        trackingService.stop();
    }

    public boolean execute(String action, JSONArray args,
                           final CallbackContext cbContext) throws JSONException {

        PluginResult pluginResult = null;

        switch (action) {
            case "defineMLInfo":
                pluginResult = defineMlInfo(args);
                break;
            case "checkNewUser":
                pluginResult = checkNewUser(args);
                break;
            case "login": //async
                loginAsync(args, cbContext);
                break;

            case "executeQuery":
                pluginResult = executeQuery(_curUser, args);
                break;
            case "executeQueries":
                pluginResult = executeQueries(_curUser, args);
                break;
            case "executeQueriesBackground": //async
                executeQueriesAsync(_curUser, args, cbContext);
                break;
            case "executeUpdate":
                pluginResult = executeUpdateInsert(_curUser, args);
                break;
            case "executeUpdatesSequentially":
                pluginResult = executeUpdatesSequentially(_curUser, args);
                break;
            case "executeInsert":
                pluginResult = executeUpdateInsert(_curUser, args);
                break;
            case "executeStatementsAndCommit":
                pluginResult = executeStatementsAndCommit(_curUser, args);
                break;
            case "checkConnectivity": //async
                checkConnectivity(_curUser, cbContext,args);
                break;
            case "sync": //async
                syncDbAsync(_curUser, cbContext,args);
                break;
            case "cancelSync":
                pluginResult = cancelSync(_curUser);
                break;
            case "resizeImage":
                pluginResult = resizeImage(args);
                break;

            case "uploadFile": //async
                transferFileAsync(ULTransferType.Upload, _curUser, args, cbContext);
                break;
            case "downloadFile": //async
                transferFileAsync(ULTransferType.Download, _curUser, args, cbContext);
                break;
            case "cancelFileTransfer":
                pluginResult = cancelFileTransfer(_curUser, args);
                break;

            case "uploadDBFile": //async
                uploadDbFileAsync(_curUser, args, cbContext);
                break;

            case "uploadLogFiles": //async
                uploadLogFilesAsync(_curUser, args, cbContext);
                break;
            case "setNewPassword":
                pluginResult = resetPassword(_curUser, args);
                break;
            case "resetDB": //this should be async
                pluginResult = resetDb(_curUser);
                break;
            case "setUserPwd":
                pluginResult = setUserPwd(args);
                break;
            case "getKeyValue":
                pluginResult = getKeyValue(args);
                break;
            case "removeKey":
                pluginResult = removeKey(args);
                break;
            case "setKeyValue":
                pluginResult = setKeyValue(args);
                break;
            case "createPDF":
                pluginResult = new MscHTML2PDF(cordova,webView,cbContext, _filesFolder).createPDF(args);
                break;
            case "startLocationTracking":
                startLocationTracking(args, cbContext);
                break;
            case "stopLocationTracking":
                pluginResult = stopLocationTracking();
                break;
            case "clearUserData":
                pluginResult = clearUserData(args);
                break;
            default:
                pluginResult = new PluginResult(Status.INVALID_ACTION, "undefined plugin method: + action");
        }

        if (pluginResult != null) {
            cbContext.sendPluginResult(pluginResult);
        }
        return true;
    }

    private void checkConnectivity(final MlUser user, final CallbackContext callbackContext,final JSONArray args) {
        PluginResult syncResult = new PluginResult(Status.OK);
        callbackContext.sendPluginResult(syncResult);
    }

    private boolean fileHasSupportedType (String fileName) {
        return fileName.endsWith(".jpeg") || fileName.endsWith(".jpg") || fileName.endsWith(".png");
    }

    /**
     * Compresses the given file based on the given CompressionQuality.
     *
     * @param args JSONArray containing two arguments: 1. file URL string, 2. Compression Quality
     * @return PluginResult
     */
    private PluginResult resizeImage(JSONArray args) {
        try {
            int fileTargetSize = 1024;
            String fileUrlString = args.getString(0);
            _curUser.logger.log("Compression Request received for file: " + fileUrlString, LogLevel.INFO);

            if (args.length() >= 3) {
                fileTargetSize = args.getInt(2);
            }

            if (fileHasSupportedType(fileUrlString)) {
                double compressionQualityInput = args.getDouble(1);
                Integer compressionQuality = (int) Math.round(compressionQualityInput * 100);

                // cordova app provides an url string of syntax: "file:///.../.../.../..."
                String harmonizedFileUrlString = fileUrlString.startsWith("file:///") ? fileUrlString.replace("file://", "") : fileUrlString;

                File imageFile = new File(harmonizedFileUrlString);
                Bitmap imageBitmap = BitmapFactory.decodeFile(harmonizedFileUrlString);
                if (!imageFile.exists() || imageBitmap == null) {
                    _curUser.logger.log("File not found or missing permission to access file: " + harmonizedFileUrlString, LogLevel.ERROR);
                    return new PluginResult(Status.ERROR, "File not found or missing permission to access file");
                }

                try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {
                    _curUser.logger.log("Compressing file: " + harmonizedFileUrlString, LogLevel.INFO);

                    float widthRatio  = (float) fileTargetSize / imageBitmap.getWidth();
                    float heightRatio = (float) fileTargetSize / imageBitmap.getHeight();

                    int width = imageBitmap.getWidth();
                    int height = imageBitmap.getHeight();

                    // Figure out what our orientation is, and use that to form the rectangle
                    if(widthRatio > heightRatio) {
                        width = Math.round(imageBitmap.getWidth() * heightRatio);
                        height = Math.round(imageBitmap.getHeight() * heightRatio);
                    } else {
                        width = Math.round(imageBitmap.getWidth() * widthRatio);
                        height = Math.round(imageBitmap.getHeight() * widthRatio);
                    }


                    Bitmap newBitmap = Bitmap.createScaledBitmap(imageBitmap, width,
                            height, true);

                    newBitmap.compress(Bitmap.CompressFormat.JPEG, compressionQuality, byteArrayOutputStream);

                    try (FileOutputStream fileOutputStream = new FileOutputStream(imageFile)) {
                        _curUser.logger.log("Writing compressed file: " + harmonizedFileUrlString, LogLevel.INFO);
                        fileOutputStream.write(byteArrayOutputStream.toByteArray());

                        _curUser.logger.log("Compression done for file: " + harmonizedFileUrlString, LogLevel.INFO);
                        return new PluginResult(Status.OK);
                    }
                }
            }

            return new PluginResult(Status.ERROR);

        } catch (JSONException e) {
            _curUser.logger.log("Arguments could not be extracted from JSON: " + e.getMessage(), LogLevel.ERROR);
            return new PluginResult(Status.ERROR);
        } catch (FileNotFoundException e) {
            _curUser.logger.log("File not found or missing permission:" + e.getMessage(), LogLevel.ERROR);
            return new PluginResult(Status.ERROR);
        } catch (IOException e) {
            _curUser.logger.log("Error writing file:" + e.getMessage(), LogLevel.ERROR);
            return new PluginResult(Status.ERROR);
        }
    }

    private PluginResult setKeyValue(JSONArray args) {
        SharedPreferences sharedPrefs = _activity.getPreferences(Context.MODE_PRIVATE);
        try {
            SharedPreferences.Editor editor = sharedPrefs.edit();
            editor.putString(args.getString(0), args.getString(1));
            editor.commit();
        } catch (JSONException e) {
            return new PluginResult(Status.ERROR);
        }
        return new PluginResult(Status.OK);
    }

    private PluginResult removeKey(JSONArray args) {
        SharedPreferences sharedPrefs = _activity.getPreferences(Context.MODE_PRIVATE);
        try {
            SharedPreferences.Editor editor = sharedPrefs.edit();
            editor.remove(args.getString(0));
            editor.commit();
        } catch (JSONException e) {
            return new PluginResult(Status.ERROR);
        }
        return new PluginResult(Status.OK);
    }

    private PluginResult getKeyValue(JSONArray args) {
        SharedPreferences sharedPrefs = _activity.getPreferences(Context.MODE_PRIVATE);
        String defaultValue = null;
        String key = null;
        try {
            key = args.getString(0);
        } catch (JSONException e) {
            return new PluginResult(Status.ERROR);
        }
        return new PluginResult(Status.OK, sharedPrefs.getString(key, defaultValue));
    }

    private PluginResult clearUserData(JSONArray args) {
        try {
            // 1. Get all directories under .../files/files
            File[] directories = _filesFolder.listFiles(new FileFilter() {
                @Override
                public boolean accept(File file) {
                    return file.isDirectory();
                }
            });

            // 2. Delete all of them recursively
            for (File file : directories) {
                this.deleteFileRecursively(file);
            }
        } catch (Exception e) {
            return new PluginResult(Status.ERROR);
        }

        return new PluginResult(Status.OK);
    }

    private void deleteFileRecursively(File file) {
        if (file.isDirectory())
            for (File child : file.listFiles())
                deleteFileRecursively(child);

        file.delete();
    }

    private PluginResult setUserPwd(JSONArray args) {
        SharedPreferences sharedPrefs = _activity.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        //editor.putString(user.getName() + "_name", name);
        try {
            if (sharedPrefs.getString(args.getString(0) + "_pw", null) == null) {
                editor.putString(args.getString(0) + "_pw", args.getString(1));
            }

            if (sharedPrefs.getString("SAMPwd" + args.getString(0), null) == null) {
                editor.putString("SAMPwd" + args.getString(0), args.getString(1));
            }

            editor.commit();
            _curUser = this.getUser(args.getString(0));
            if (_curUser == null) {
                _curUser = this.createUser(args.getString(0), args.getString(1));
                this._allUsers.add(_curUser);
            } else {
                _curUser.setPw(args.getString(1));
            }
            _curUser.initialize();
        } catch (Exception e) {
            e.printStackTrace();
            return new PluginResult(Status.ERROR);
        }
        return new PluginResult(Status.OK);
    }

    //region plugin methods
    //region initialization & login
    private PluginResult defineMlInfo(JSONArray args) {
        PluginResult pluginResult = new PluginResult(Status.OK);
        //TODO: would like to log this, but at this point no user object or logger is available
        try {
            _conInfo = new MlConnectionInfo();
            _conInfo.hostName = args.getString(0);
            _conInfo.port = args.getInt(1);
            _conInfo.subscription = args.getString(2);
            _conInfo.version = args.getString(3);
            _conInfo.publication = args.getString(4);
            _conInfo.dbFileName = args.getString(5) + ".udb";
            _conInfo.protocol = args.getString(6);
            _conInfo.urlSuffix = args.getString(7);

            for (MlUser user : _allUsers) {
                MlConnectionInfo conInfo = new MlConnectionInfo();
                conInfo.hostName = _conInfo.hostName;
                conInfo.port = _conInfo.port;
                conInfo.subscription = _conInfo.subscription;
                conInfo.version = _conInfo.version;
                conInfo.publication = _conInfo.publication;
                conInfo.dbFileName = _conInfo.dbFileName;
                conInfo.protocol = _conInfo.protocol;
                conInfo.urlSuffix = _conInfo.urlSuffix;
                user.setMlConnectionInfo(conInfo);
            }
            //TODO: what to do with a current logged in user?
        } catch (Exception e) {
            pluginResult = new PluginResult(Status.ERROR, "defineMlInfo error: " + e.getMessage());
        }
        return pluginResult;
    }

    private PluginResult checkNewUser(JSONArray args) {
        PluginResult pluginResult = new PluginResult(Status.OK, true);
        try {
            String userName = args.getString(0);
            MlUser user = getUser(userName);
            if (user != null) {
                if (user.getDbFile().exists()) {
                    pluginResult = new PluginResult(Status.OK, false);
                }
            }
        } catch (Exception e) {
            pluginResult = new PluginResult(Status.ERROR, "checkNewUser error: " + e.getMessage());
        }

        return pluginResult;
    }
    //endregion

    /**
     * sets the _curUser object
     *
     * @param args
     * @param callbackContext
     */
    private void loginAsync(JSONArray args, final CallbackContext callbackContext) {
        //TODO: implement wakelock?
        try {
            SharedPreferences sharedPrefs = _activity.getPreferences(Context.MODE_PRIVATE);
            final String userName = args.getString(0);
            final String pw = args.getString(1);
            final String key = "SAMPwd" + args.getString(0);
            final String savedPwd = sharedPrefs.getString(key, null);
            String encryptionKey = sharedPrefs.getString("SAMEncKey" + userName, null);
            //case user changes -> release resources of previous user
            if (_curUser != null && !_curUser.getName().equals(userName)) {
                _curUser.release();
            }

            //change current user object;
            _curUser = getUser(userName);
            _curUser.ulWrapper.createDbConnections(encryptionKey);
            setLogLevelFromDb(_curUser);

            if (_curUser.getPw().equals(savedPwd)) {
                callbackContext.sendPluginResult(new PluginResult(Status.OK));
            } else {
                // Check new password against mw
                _curUser.logger.log("Login - Entered password does not match with password in keychain. Checking entered password against MW...", LogLevel.INFO);

                int authenticationCode = checkPasswordIsValid(_curUser, pw);
                if (authenticationCode == 1000) {
                    // set the new password in the user credentials and continue login
                    _curUser.logger.log("Login - Password check against MW successful, using the entered password from now on", LogLevel.INFO);

                    JSONArray newArgs = new JSONArray();
                    newArgs.put(pw);

                    _curUser.setPw(pw);
                    setNewPassword(_curUser, newArgs);
                    callbackContext.sendPluginResult(new PluginResult(Status.OK));
                } else {
                    _curUser.logger.log("Login - Password check against MW failed", LogLevel.INFO);
                    _curUser.release();
                    callbackContext.sendPluginResult(new PluginResult(Status.ERROR, authenticationCode));
                }
            }


        } catch (Exception e) {
            callbackContext.sendPluginResult(new PluginResult(Status.ERROR, "login error: " + e.getMessage()));
        }
    }

    private int checkPasswordIsValid(final MlUser user, final String newPassword) {
        int authenticationCode = 0;
        Boolean hasError = false;

        try {
            MlConnectionInfo conInfo = user.getConnectionInfo();

            String syncMode = "";
            String syncModel = user.ulWrapper.getPublicationFromUdb();

            Socket socket = new Socket();
            socket.connect(new InetSocketAddress(user.getConnectionInfo().hostName, user.getConnectionInfo().port), 5000);

            authenticationCode = user.ulWrapper.syncDb(user.getName(),
                    newPassword, conInfo.publication,
                    null,syncMode,true);
        } catch (Exception ex) {
            hasError = true;
        } finally {
            user.logger.log("Login - Password check returned authentication code " + authenticationCode, LogLevel.INFO);
        }

        return authenticationCode;
    }

    private void startLocationTracking(JSONArray args, CallbackContext cbContext) {
        Integer interval = null;

        if (args != null && args.length() > 0) {
            try {
                interval = Integer.parseInt((String) args.get(0));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        startLocationTrackingWithPermissions(cbContext, interval);
    }

    private PluginResult stopLocationTracking() {
        trackingService.stop();
        return new PluginResult(PluginResult.Status.OK);
    }

    private void startLocationTrackingWithPermissions(CallbackContext cbContext, Integer interval) {
        if ( ContextCompat.checkSelfPermission( _context, android.Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED ) {
            cordova.requestPermission(this, LocationBackgroundService.MY_PERMISSION_ACCESS_COURSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION);
            return;
        }

        trackingService.start(_curUser, cbContext, interval);
    }

    @Override
    public void onRequestPermissionResult(int requestCode, String[] permissions, int[] grantResults) throws JSONException {
        super.onRequestPermissionResult(requestCode, permissions, grantResults);

        if (requestCode == LocationBackgroundService.MY_PERMISSION_ACCESS_COURSE_LOCATION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startLocationTrackingWithPermissions(null, null);
            } else if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED) {

            }
        }
    }

    private void loginAsyncOld(JSONArray args, final CallbackContext callbackContext) {
        //TODO: implement wakelock?
        try {
            final String userName = args.getString(0);
            final String pw = args.getString(1);

            //case user changes -> release resources of previous user
            if (_curUser != null && !_curUser.getName().equals(userName)) {
                _curUser.release();
            }

            //change current user object;
            _curUser = getUser(userName);
            if (_curUser == null) {
                _curUser = createUser(userName, pw);
                _allUsers.add(_curUser);
            }

            if (_curUser.getDbFile().exists()) {
                //case: existing user
                //user already synched, verify credentials
                if (!doUserCredentialsMatch(userName, pw)) {
                    callbackContext.sendPluginResult(new PluginResult(Status.ERROR, "Wrong credentials for user: " + userName));
                } else {
                    _curUser.initialize();
                    if (!BuildConfig.DEBUG) {
                        setLogLevelFromDb(_curUser);
                    }
                    callbackContext.sendPluginResult(new PluginResult(Status.OK));
                }
            } else {
                //case: new user
                //download DB file and store credentials
                final MlUser user = _curUser;
                cordova.getThreadPool().execute(new Runnable() {
                    public void run() {
                        PluginResult downloadResult = new PluginResult(Status.OK);
                        try {
                            user.initialize();
                            //download DB
                            MlConnectionInfo conInfo = user.getConnectionInfo();
                            user.ulWrapper.transferFile(ULTransferType.Download, user.getName(),
                                    pw, conInfo.version, conInfo.dbFileName,
                                    conInfo.dbFileName, user.getFolder(), callbackContext);

                        } catch (Exception ex) {
                            downloadResult = new PluginResult(Status.ERROR, "Error downloading DB: "
                                    + ex.getMessage());
                            callbackContext.sendPluginResult(downloadResult);
                            return;
                        }
                        try {
                            //update credentials & create DB connections
                            saveUserCredentials(userName, pw);
                            //user.ulWrapper.createDbConnections();
                            if (!BuildConfig.DEBUG) {
                                setLogLevelFromDb(_curUser);
                            }
                        } catch (Exception ex) {
                            downloadResult = new PluginResult(Status.ERROR, "Error initializing DB: "
                                    + ex.getMessage());
                        }
                        callbackContext.sendPluginResult(downloadResult);
                    }
                });
            }
        } catch (Exception e) {
            callbackContext.sendPluginResult(new PluginResult(Status.ERROR, "login error: " + e.getMessage()));
        }
    }

    //region queries
    private PluginResult executeQuery(MlUser user, JSONArray args) {
        PluginResult pluginResult;
        try {
            String sql = args.getString(0);
            JSONArray result = user.ulWrapper.executeQueryAsJsonArray(sql, true);
            pluginResult = new PluginResult(Status.OK, result);
        } catch (Exception e) {
            String error = e.toString();
            String[] errorMsg = error.split(":");
            pluginResult = new PluginResult(Status.ERROR, errorMsg[errorMsg.length - 1]);
        }
        return pluginResult;
    }

    private PluginResult executeQueries(MlUser user, JSONArray args) {
        PluginResult pluginResult;
        try {
            JSONArray stmts = args.getJSONArray(0);
            JSONObject result = user.ulWrapper.executeQueriesSequentially(stmts);
            pluginResult = new PluginResult(Status.OK, result);
        } catch (Exception e) {
            pluginResult = new PluginResult(Status.ERROR, "error_query");
        }
        return pluginResult;
    }

    private void executeQueriesAsync(final MlUser user, JSONArray args,
                                     final CallbackContext callbackContext) {
        final PerformanceTimer pt = new PerformanceTimer();
        pt.start();

        try {
            final JSONArray stmts = args.getJSONArray(0);
            cordova.getThreadPool().execute(new Runnable() {
                public void run() {
                    try {
                        JSONObject result = user.ulWrapper.executeQueriesSequentially(stmts);
                        pt.interim("executed all queries: ");
                        callbackContext.success(result);
                    } catch (Exception e) {
                        callbackContext.error("error_query");
                    }
                }
            });

        } catch (JSONException e) {
            callbackContext.error("error_query");
        }
    }

    private PluginResult executeUpdateInsert(final MlUser user, JSONArray args) {
        PluginResult pluginResult;

        try {
            String sql = args.getString(0);
            user.ulWrapper.executeUpdate(sql);
            pluginResult = new PluginResult(Status.OK);
        } catch (Exception e) {
            pluginResult = new PluginResult(Status.ERROR, "error_query");
        }
        return pluginResult;
    }

    private PluginResult executeUpdatesSequentially(final MlUser user, JSONArray args) {
        PluginResult pluginResult;

        try {
            final JSONArray stmts = args.getJSONArray(0);
            user.ulWrapper.executeUpdatesSequentially(stmts);
            pluginResult = new PluginResult(Status.OK);
        } catch (Exception e) {
            pluginResult = new PluginResult(Status.ERROR, "error_query");
        }
        return pluginResult;
    }

    //endregion

    private PluginResult executeStatementsAndCommit(MlUser user, JSONArray args) {
        PluginResult pluginResult;
        try {
            JSONArray stmts = args.getJSONArray(0);
            user.ulWrapper.executeStatementsAndCommit(stmts);
            pluginResult = new PluginResult(Status.OK, user.ulWrapper.getNewObjectUuid());
        } catch (Exception e) {
            pluginResult = new PluginResult(Status.ERROR, "error_query");
        }
        return pluginResult;
    }

    //region DB sync
    private void syncDbAsync(final MlUser user, final CallbackContext callbackContext,final JSONArray args) {
        try {
            PowerManager pm = (PowerManager) _context.getSystemService(Context.POWER_SERVICE);
            final PowerManager.WakeLock wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK, "My wakelook");
            wakeLock.acquire();

            cordova.getThreadPool().execute(new Runnable() {
                public void run() {
                    PluginResult syncResult = new PluginResult(Status.OK);
                    Integer authenticationCode = 0;
                    Boolean hasError = false;

                    try {
                        MlConnectionInfo conInfo = user.getConnectionInfo();

                        String syncMode = "";
                        String publication = "";

                        try {
                            if(args.length() > 0) {
                                syncMode = args.getString(0);
                                publication = args.getString(1);
                            }
                        } catch(Exception e) {
                            user.logger.log(e.getMessage(), LogLevel.ERROR);
                        }

                        if (publication.length() == 0) {
                            publication = conInfo.publication;
                        }

                        authenticationCode = user.ulWrapper.syncDb(user.getName(),
                                user.getPw(), publication,
                                callbackContext,syncMode,false);
                    } catch (Exception ex) {
                        hasError = true;
                    } finally {
                        if (hasError || authenticationCode >= 4000 || authenticationCode != 1000) {
                            syncResult = new PluginResult(Status.ERROR, authenticationCode);
                        } else {
                            syncResult = new PluginResult(Status.OK, authenticationCode);
                        }

                        if (wakeLock != null) {
                            wakeLock.release();
                        }
                    }

                    callbackContext.sendPluginResult(syncResult);
                }
            });
        } catch (Exception e) {
            callbackContext.error(e.getMessage());
        }
    }

    //endregion

    private PluginResult cancelSync(MlUser user) {
        PluginResult result = new PluginResult(Status.OK);
        try {
            user.ulWrapper.cancelSync();
        } catch (Exception ex) {
            result = new PluginResult(Status.ERROR, ex.getMessage());
        }
        return result;
    }

    //region file transfer
    private void transferFileAsync(final ULTransferType type, final MlUser user, final JSONArray args,
                                   final CallbackContext cbContext) {
        try {
            try {
                PowerManager pm = (PowerManager) _context.getSystemService(Context.POWER_SERVICE);
                final PowerManager.WakeLock wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK, "My wakelook");
                wakeLock.acquire();

                cordova.getThreadPool().execute(new Runnable() {
                    public void run() {
                        //UI quirk: need to send a boolean true back in case of success
                        PluginResult result = new PluginResult(Status.OK, true);
                        long authenticationCode = 0;
                        Boolean hasError = false;

                        try {
                            String fileName = args.getString(0);
                            //files are stored in the root folder (not user specific folder)
                            File file = new File(user.getAttachmentsFolder(), fileName);
                            //upload: check if file exists
                            if (type == ULTransferType.Upload && !file.exists()) {
                                throw new Exception("file upload: specified file does not exist: "
                                        + file.getAbsolutePath());
                            }
                            //download: download only if file does not exist locally
                            if (type == ULTransferType.Download && file.exists()) {
                                cbContext.sendPluginResult(new PluginResult(Status.OK));
                                return;
                            }

                            //download file into root folder (not user specific folder)
                            MlConnectionInfo conInfo = user.getConnectionInfo();

                            authenticationCode = user.ulWrapper.transferFile(type, user.getName(), user.getPw(),
                                    conInfo.version, fileName, fileName, type == ULTransferType.Upload ? user.getAttachmentsFolder() : _filesFolder, cbContext);
                        } catch (Exception ex) {
                            hasError = true;
                        } finally {
                            if (hasError || authenticationCode >= 4000 || authenticationCode != 1000) {
                                result = new PluginResult(Status.ERROR, authenticationCode);
                            } else {
                                result = new PluginResult(Status.OK, authenticationCode);
                            }

                            if (wakeLock != null) {
                                wakeLock.release();
                            }
                        }
                        cbContext.sendPluginResult(result);
                    }
                });
            } catch (Exception e) {
                cbContext.error(e.getMessage());
            }
        } catch(Exception e) {
            cbContext.sendPluginResult(new PluginResult(Status.ERROR, 5001));
        }
    }
    //endregion

    private PluginResult cancelFileTransfer(MlUser user, JSONArray args) {
        PluginResult pluginResult = new PluginResult(Status.OK);
        String fileId = null;

        try {
            fileId = args.getString(0);
            user.ulWrapper.cancelFileTransfer(fileId);
        } catch (Exception ex) {
            String msg = String.format("error cancelling file transfer with id: %s; error: %s",
                    fileId, ex.getMessage());
            user.logger.log(msg, LogLevel.ERROR);
            pluginResult = new PluginResult(Status.ERROR, msg);
        }
        return pluginResult;
    }

    //region misc plugin functions
    private PluginResult setNewPassword(MlUser user, JSONArray args) {
        PluginResult result;
        try {
            String msg = "setting new password for user : " + user.getName();
            user.logger.log(msg, LogLevel.INFO);

            String pw = args.getString(0);
            saveUserCredentials(user.getName(), pw);
            result = new PluginResult(Status.OK);
        } catch (Exception ex) {
            String msg = "setting new password for user: " + user.getName() + " failed: " + ex.getMessage();
            user.logger.log(msg, LogLevel.ERROR);
            result = new PluginResult(Status.ERROR, msg);
        }
        return result;
    }

    //region misc plugin functions
    private PluginResult resetPassword(MlUser user, JSONArray args) {
        PluginResult result;
        try {
            String msg = "setting new password for user : " + user.getName();
            user.logger.log(msg, LogLevel.INFO);

            String newPw = args.getString(0);
            String oldPw = args.getString(1);

            if (!user.getPw().equals(oldPw)) {
                throw new Exception("oldPwdIncorrect");
            }

            saveUserCredentials(user.getName(), newPw);
            result = new PluginResult(Status.OK);
            user.setPw(newPw);
        } catch (Exception ex) {
            String msg = "setting new password for user: " + user.getName() + " failed: " + ex.getMessage();
            user.logger.log(msg, LogLevel.ERROR);
            result = new PluginResult(Status.ERROR, msg);
        }
        return result;
    }

    /**
     * empties the log files and deletes the user's DB
     *
     * @param user
     * @return
     */
    private PluginResult resetDb(MlUser user) {
        PluginResult result;
        String msg;
        try {
            msg = "resetting DB for user : " + user.getName();
            user.logger.log(msg, LogLevel.INFO);

            //soft close DB connections
            user.ulWrapper.close(false);
            user.logger.clearLog();
            user.transactionLogger.clearLog();

            //delete DB file
            if (user.getDbFile().delete()) {
                msg = "reset DB success";
                user.logger.log(msg, LogLevel.INFO);
                SharedPreferences sharedPrefs = _activity.getPreferences(Context.MODE_PRIVATE);

                SharedPreferences.Editor editor = sharedPrefs.edit();
                editor.remove("SAMEncKey" + user.getName());
                editor.commit();

                result = new PluginResult(Status.OK);
            } else {
                throw new Exception("could not delete DB file: " + user.getDbFile().getAbsolutePath());
            }
        } catch (Exception ex) {
            msg = "resetDb for user: " + user.getName() + " failed: " + ex.getMessage();
            user.logger.log(msg, LogLevel.ERROR);
            result = new PluginResult(Status.ERROR, msg);
        }
        return result;
    }

    private void uploadDbFileAsync(final MlUser user, final JSONArray args,
                                   final CallbackContext callbackContext) {
        try {
            PowerManager pm = (PowerManager) _context.getSystemService(Context.POWER_SERVICE);
            final PowerManager.WakeLock wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK, "My wakelook");
            wakeLock.acquire();

            cordova.getThreadPool().execute(new Runnable() {
                public void run() {
                    PluginResult syncResult = new PluginResult(Status.OK);
                    File zippedDb = null;
                    String msg = null;
                    try {
                        String dbCopyName = args.getString(0);
                        msg = String.format("uploading DB file, user: %s; DB copy name: %s", user.getName(), dbCopyName);
                        user.logger.log(msg, LogLevel.INFO);

                        //copy & zip DB
                        zippedDb = new File(user.getFolder(), dbCopyName);
                        Zipper.zip(user.getDbFile(), zippedDb);

                        //upload DB
                        MlConnectionInfo conInfo = user.getConnectionInfo();
                        user.ulWrapper.transferFile(ULTransferType.Upload, user.getName(), user.getPw(),
                                conInfo.version, dbCopyName, dbCopyName, user.getFolder(), callbackContext);

                    } catch (Exception ex) {
                        msg = "uploading DB file failed: " + ex.getMessage();
                        user.logger.log(msg, LogLevel.ERROR);
                        syncResult = new PluginResult(Status.ERROR, ex.getMessage());
                    } finally {
                        //delete DB copy
                        if (zippedDb != null && zippedDb.exists()) {
                            if (zippedDb.delete()) {
                                msg = "deleted DB copy";
                            } else {
                                msg = "could not delete DB copy";
                            }
                            user.logger.log(msg, LogLevel.INFO);
                        }

                        if (wakeLock != null) {
                            wakeLock.release();
                        }
                    }
                    callbackContext.sendPluginResult(syncResult);
                }
            });
        } catch (Exception e) {
            callbackContext.error(e.getMessage());
        }
    }
    //endregion
    //endregion

    private void uploadLogFilesAsync(final MlUser user, final JSONArray args,
                                     final CallbackContext cbContext) {
        try {
            PowerManager pm = (PowerManager) _context.getSystemService(Context.POWER_SERVICE);
            final PowerManager.WakeLock wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK, "My wakelook");
            wakeLock.acquire();

            cordova.getThreadPool().execute(new Runnable() {
                public void run() {
                    PluginResult syncResult = new PluginResult(Status.OK);
                    File zippedLogFile = null;
                    String msg = null;
                    List<File> filesToBeZipped = new ArrayList<File>();

                    try {
                        String logCopyName = args.getString(0);
                        msg = String.format("uploading log file, user: %s; DB copy name: %s", user.getName(), logCopyName);
                        user.logger.log(msg, LogLevel.INFO);

                        //copy & zip log file
                        zippedLogFile = new File(user.getFolder(), logCopyName);

                        filesToBeZipped.add(new File(user.logger.getLogFilePath()));
                        filesToBeZipped.add(new File(user.transactionLogger.getLogFilePath()));
                        Zipper.zip(filesToBeZipped, zippedLogFile);

                        //upload zipped log file
                        MlConnectionInfo conInfo = user.getConnectionInfo();
                        user.ulWrapper.transferFile(ULTransferType.Upload, user.getName(), user.getPw(),
                                conInfo.version, logCopyName, logCopyName, user.getFolder(), cbContext);

                    } catch (Exception ex) {
                        msg = "uploading log file failed: " + ex.getMessage();
                        user.logger.log(msg, LogLevel.ERROR);
                        syncResult = new PluginResult(Status.ERROR, ex.getMessage());
                    } finally {
                        //delete log file copy
                        if (zippedLogFile != null && zippedLogFile.exists()) {
                            if (zippedLogFile.delete()) {
                                msg = "deleted log file copy";
                            } else {
                                msg = "could not delete log file copy";
                            }
                            user.logger.log(msg, LogLevel.INFO);
                        }

                        if (wakeLock != null) {
                            wakeLock.release();
                        }
                    }
                    cbContext.sendPluginResult(syncResult);
                }
            });
        } catch (Exception e) {
            cbContext.error(e.getMessage());
        }
    }

    //region helper methods
    private void setLogLevelFromDb(final MlUser user) throws Exception {

        try {
            String sql = "SELECT appl_log_level as APPL_LOG_LEVEL, transac_log_on as TRANSAC_LOG_ON FROM SAM_USERS WHERE UPPER(user_name)=UPPER('" + user.getName() + "')";
            JSONObject queryResult = user.ulWrapper.executeQueryAsJsonArray(sql, true).getJSONObject(0);
            String logLvlAsString = queryResult.getString("APPL_LOG_LEVEL");
            String transactLogLvlAsString = queryResult.getString("TRANSAC_LOG_ON");
            LogLevel logLevel = LogLevel.parse(logLvlAsString);
            LogLevel transactionLogLevel = LogLevel.parse(transactLogLvlAsString);
            user.logger.setLogLevel(logLevel);
            user.transactionLogger.setLogLevel(transactionLogLevel);
        } catch (Exception ex) {
            String msg = "setLogLevelFromDb failed: " + ex.getMessage();
            throw new Exception(msg, ex);
        }
    }

    //region user helper functions
    //@Nullable
    private MlUser getUser(String name) {
        for (MlUser user : _allUsers) {
            if (user.getName().equals(name)) {
                return user;
            }
        }
        return null;
    }

    private MlUser createUser(String name, String pw) throws Exception {
        File folder = new File(_filesFolder, name);

        if (!folder.exists()) {
            if (!folder.mkdirs()) {
                throw new Exception("could not create user folder for: " + name);
            }

            File attachmentFolder = new File(folder, "attachments");
            if (!attachmentFolder.mkdirs()) {
                throw new Exception("could not create user folder for: " + name);
            }
        }
        MlUser user = new MlUser(name, pw, folder, _conInfo, _context);
        return user;
    }

    private void saveUserCredentials(String name, String pw) {
        SharedPreferences sharedPrefs = _activity.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        //editor.putString(name + "_name", name);
        //editor.putString(name + "_pw", pw);
        editor.putString("SAMPwd" + name,pw);
        editor.commit();
    }

    private boolean doUserCredentialsMatch(String name, String pw) throws Exception {
        String[] credentials = getCredentialsForUser(name);
        if (credentials != null) {
            if (credentials[0].equals(name) && credentials[1].equals(pw)) {
                return true;
            } else {
                return false;
            }
        }
        throw new Exception("credentials for user " + name + " not found");
    }

    /**
     * returns null if no credentials could be found
     *
     * @param name
     * @return
     */
    private String[] getCredentialsForUser(String name) {
        SharedPreferences sharedPrefs = _activity.getPreferences(Context.MODE_PRIVATE);

        String[] result = null;

        //String userName = sharedPrefs.getString(name + "_name", defaultValue);
        String userPw = sharedPrefs.getString("SAMPwd" + name, null);
        if (userPw != null) {
            result = new String[]{name, userPw};
        }


        return result;
    }
    //endregion
    //endregion
    //region garbage code

    /**
     * returns all users which have credentials set
     *
     * @return
     * @throws Exception
     */
    private List<MlUser> getAllUsers() throws Exception {
        //ConcurrentSkipListSet<MlUser> users = new ConcurrentSkipListSet<>();
        List<MlUser> users = Collections.synchronizedList(new ArrayList<MlUser>());

        for (File file : _filesFolder.listFiles()) {
            if (!file.isDirectory()) {
                continue;
            }
            String[] credentials = getCredentialsForUser(file.getName());
            if (credentials != null) {
                MlUser user = createUser(credentials[0], credentials[1]);
                users.add(user);
            }
        }

        return users;
    }

    /**
     * @return JsonArray[0] = boolean : has Internet Connection; JsonArray[1] =
     * String : Connection Type in lower case, e.g. "wifi"
     */
    private JSONArray hasInternetConnection() {
        JSONArray connection = new JSONArray();
        ConnectivityManager connManager = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo netInfo = connManager.getActiveNetworkInfo();
        boolean isConnected;
        try {
            isConnected = netInfo.isConnected();
            connection.put(isConnected);
            connection.put(netInfo.getTypeName().toLowerCase());
        } catch (Exception e) {
            e.printStackTrace();
            isConnected = false;
            connection.put(isConnected);
            connection.put("noConnetion");
        }
        return connection;
    }

    //endregion

}





﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Windows.Data.Json;
using Windows.Foundation;
using Windows.Storage;
using Windows.Storage.Streams;

namespace UtilComponent
{
    public static class Util
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="time">string in the format YYYYMMDDHHMMSSssss (YearMonthDayHourMinutesSecondsMilliseconds)</param>
        /// <returns></returns>
        public static DateTimeOffset GetDateTime(string time)
        {
            var year = int.Parse(time.Substring(0, 4));
            var month = int.Parse(time.Substring(4, 2));
            var day = int.Parse(time.Substring(6, 2));
            var hour = int.Parse(time.Substring(8, 2));
            var minute = int.Parse(time.Substring(10, 2));
            var second = int.Parse(time.Substring(12, 2));

            var date = new DateTime(year, month, day, hour, minute, second);
            var dateTimeoffset = TimeZoneInfo.ConvertTime(date, TimeZoneInfo.Local);

            return dateTimeoffset;
        }

        /// <summary>
        /// returns the current UTC time as YYYY-MM-DD_HH-MM-SS
        /// example: 2016-09-20_16-50-31
        /// </summary>
        /// <returns></returns>
        public static string GetCurrentUtcTimeStamp()
        {
            var now = DateTime.UtcNow;
            var result = string.Format("{0}-{1}-{2}_{3}-{4}-{5}", now.Year, now.Month, now.Day, now.Hour, now.Minute, now.Second);
            return result;
        }

        /// <summary>
        /// returns a UTF-16 encoded string as byte array
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static byte[] StringToByteArray(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        /// <summary>
        /// returns a UTF-16 encoded string
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static string ByteArrayToString([ReadOnlyArray()] byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }

        /// <summary>
        /// converts str to a UTF-8 encoded string and returns it as a byte array
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static byte[] StringToUtf8ByteArray(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            var utf8Bytes = Encoding.Convert(Encoding.Unicode, Encoding.UTF8, bytes);
            return utf8Bytes;
        }

        /// <summary>
        /// returns a UTF-8 encoded string
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static string Utf8ByteArrayToString([ReadOnlyArray()] byte[] bytes)
        {
            var utf8String = Encoding.UTF8.GetString(bytes, 0, bytes.Length);
            return utf8String;
        }

        public static string ToJsonString(IList<IReadOnlyDictionary<object, object>> list)
        {
            var result = new JsonArray();

            foreach (var dic in list)
            {
                var jsonObj = new JsonObject();
                foreach (var pair in dic)
                {
                    jsonObj.Add(pair.Key.ToString(), JsonValue.CreateStringValue(pair.Value.ToString()));
                }
                result.Add(jsonObj);
            }
            return result.Stringify();
        }

        public static string DictionaryToJsonString(IDictionary<string, string> dic)
        {
            var result = new JsonObject();
            foreach (KeyValuePair<string, string> entry in dic)
            {
                result.Add(entry.Key, JsonValue.CreateStringValue(entry.Value));
            }
            return result.Stringify();
        }

        public static string RemoveNewlineChars(string text)
        {
            var cleanedText = Regex.Replace(text, @"\t|\n|\r", "");
            return cleanedText;
        }

        /// <summary>
        /// replaces all '/' characters with '\\'
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string ToWindowsConformPath(string path)
        {
            var conformPath = path.Replace('/', '\\');
            return conformPath;
        }

        /// <summary>
        /// removes folder separation chars (/ or \) at the end of path
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string CleanPathEnd(string path)
        {
            var result = path;
            if (path.EndsWith(@"\") || path.EndsWith(@"/"))
            {
                result = path.Substring(0, path.Length - 1);
            }
            return result;
        }

        public static IDictionary<string, string> GetObjectProperties(object obj)
        {
            var dic = new Dictionary<string, string>();
            foreach (var prop in obj.GetType().GetRuntimeProperties())
            {
                dic.Add(prop.Name, prop.GetValue(obj, null).ToString());
                //Console.WriteLine("{0} = {1}", prop.Name, prop.GetValue(obj, null));
            }
            return dic;
        }

        /// <summary>
        /// reads the content of <paramref name="file"/> and returns it as a byte array
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static IAsyncOperation<IEnumerable<byte>> GetFileContent(StorageFile file)
        {
            return Task.Run(async () =>
            {
                byte[] result = null;
                try
                {
                    var buffer = await FileIO.ReadBufferAsync(file);
                    using (var reader = DataReader.FromBuffer(buffer))
                    {
                        result = new byte[buffer.Length];
                        reader.ReadBytes(result);
                    }
                    //return result;
                    return result.AsEnumerable<byte>();
                }
                catch (Exception ex)
                {
                    var msg = string.Format("could not read content of file: {0}", file.Path);
                    throw new Exception(msg, ex);
                }
            }).AsAsyncOperation();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoggerLib;
using Windows.Networking.Connectivity;

namespace InternetConnectionInfoComponent
{
    public sealed class ConnectionInfo
    {
        public static InternetConnectionType GetInternetConnectionType()
        {
            return GetInternetConnectionType(null);
        }

        /// <summary>
        /// see http://www.iana.org/assignments/ianaiftype-mib/ianaiftype-mib for possible IanaInterfaceType values
        /// </summary>
        /// <returns></returns>
        public static InternetConnectionType GetInternetConnectionType(Logger logger)
        {
            var currentconnection = NetworkInformation.GetInternetConnectionProfile();
            if (currentconnection == null)
            {
                return InternetConnectionType.None;
            }

            var connectionType = currentconnection.NetworkAdapter.IanaInterfaceType;
            if (logger != null)
            {
                logger.Log(string.Format("Detected following internet connection iana interface type: {0}", connectionType), LogLevel.INFO);
            }

            switch (connectionType)
            {
                case 6:
                    return InternetConnectionType.Cable;
                case 71:
                    return InternetConnectionType.WiFi;
                case 243:
                    return InternetConnectionType.Mobile;
                case 244:
                    return InternetConnectionType.Mobile;
                default:
                    return InternetConnectionType.Unknown;
            }
        }


        public static bool HasCableOrWifiInternetConnection(Logger logger, out InternetConnectionType type)
        {
            type = InternetConnectionType.Unknown;
            var result = false;

            var currentconnection = NetworkInformation.GetInternetConnectionProfile();
            if (currentconnection != null)
            {
                result = currentconnection.GetNetworkConnectivityLevel() >= NetworkConnectivityLevel.InternetAccess;

                var connectionType = currentconnection.NetworkAdapter.IanaInterfaceType;
                if (logger != null)
                {
                    logger.Log(string.Format("Detected internet connection iana interface type: '{0}'. Connectivity level : '{1}'",
                        connectionType, currentconnection.GetNetworkConnectivityLevel()), LogLevel.INFO);
                }

                switch (connectionType)
                {
                    case 6: //Cable
                        type = InternetConnectionType.Cable;
                        break;
                    case 71: //WiFi
                        type = InternetConnectionType.WiFi;
                        break;
                }
            }
            return result;
        }
    }

    public enum InternetConnectionType
    {
        None,
        Cable,
        WiFi,
        Mobile,
        Unknown
    }
}

using System;
using System.Threading.Tasks;
using Windows.Data.Json;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.System.Threading;

namespace PluginComponent
{
    public sealed class GeoTrackingService
    {
        private static String TRACKING_PUBLICATION = "LIVE_TRACKING";
        private static int DEFAULT_SYNC_INTERVAL = 3 * 60000;
        private static uint DEFAULT_LOCATION_REPORT_INTERVAL = 45 * 1000;

        private ThreadPoolTimer timer;

        private MlUser currentUser;
        private int customInterval;

        private GeoLocation currentLocation;
        private Geolocator geolocator;

        private EventHandler<object> locationTrackingEvent;

        public GeoTrackingService(EventHandler<object> locationTrackingEvent)
        {
            this.currentUser = null;
            this.locationTrackingEvent = locationTrackingEvent;
        }

        public void start(MlUser user, int interval)
        {
            this.customInterval = interval != null ? interval * 60000 : DEFAULT_SYNC_INTERVAL;
            this.currentUser = user;

            // Create geolocator object
            this.geolocator = new Geolocator();
            this.geolocator.ReportInterval = DEFAULT_LOCATION_REPORT_INTERVAL;

            this.geolocator.PositionChanged += new TypedEventHandler<Geolocator, PositionChangedEventArgs>(OnPositionChanged);
            this.geolocator.StatusChanged += new TypedEventHandler<Geolocator, StatusChangedEventArgs>(OnStatusChanged);

            this.timer = ThreadPoolTimer.CreatePeriodicTimer(this.OnTimerTick, TimeSpan.FromMilliseconds(this.customInterval));
        }

        public void stop()
        {
            this.currentUser.Logger.Log(string.Format("GeoTrackingService - stop - Stopping location tracking"), LoggerLib.LogLevel.INFO);

            // Setting the ReportInterval to 0 does not trigger updates anymore
            this.geolocator.ReportInterval = default(uint);
            this.currentUser = null;
            this.currentLocation = null;

            if (this.timer != null)
            {
                this.timer.Cancel();
            }
        }

        private void OnTimerTick(ThreadPoolTimer timer)
        {
            if (this.currentUser == null)
            {
                return;
            }

            try
            {
                this.handlePreDbSync();
                this.syncDbAsync();
            }
            catch(Exception ex)
            {
                this.currentUser.Logger.Log(string.Format("GeoTrackingService - OnTimerTick - Error when trying to sync against LT sync model: {0}", ex.Message), LoggerLib.LogLevel.ERROR);
            }
        }

        private void insertLocation(GeoLocation location)
        {
            String latitudeString = location.Latitude.ToString().Length > 18 ? location.Latitude.ToString().Substring(0, 18) : location.Latitude.ToString();
            String longitudeString = location.Longitude.ToString().Length > 18 ? location.Longitude.ToString().Substring(0, 18) : location.Longitude.ToString();

            String currentDateTime = DateTime.Now.ToString("yyyyMMddHHmmss");

            String deleteOldLocationStmt = "DELETE FROM SAM_LT_COORD";
            String newLocationStmt = string.Format("INSERT INTO SAM_LT_COORD (LATITUDE, LONGITUDE, USER_NAME, CREATED_ON, ACTION_FLAG)" +
                        "VALUES('{0}', '{1}', '{2}', '{3}', '{4}')", latitudeString, longitudeString, this.currentUser.Name, currentDateTime, "N");

            this.currentUser.Logger.Log(string.Format("GeoTrackingService - insertLocation - Insert new location: {0}", newLocationStmt), LoggerLib.LogLevel.INFO);

            this.currentUser.UlWrapper.ExecuteUpdate(deleteOldLocationStmt);
            this.currentUser.UlWrapper.ExecuteUpdate(newLocationStmt);
        }

        private void handlePreDbSync()
        {
            var queryResult = this.currentUser.UlWrapper.ExecuteQueryAsDictionary("SELECT * FROM SAM_LT_COORD WHERE ACTION_FLAG != 'O'");

            if (queryResult.Count > 0 || this.currentLocation == null)
            {
                return;
            }

            this.insertLocation(this.currentLocation);
        }

        private void syncDbAsync()
        {
            Task.Run(() =>
            {
                try
                {
                    this.currentUser.UlWrapper.SyncDbWithObserver(this.currentUser.Name, this.currentUser.Pw, null, false, "", TRACKING_PUBLICATION);
                }
                catch (Exception ex)
                {
                    this.currentUser.Logger.Log(string.Format("GeoTrackingService - syncDbAsync - Error when trying sync a new location: {0}", ex.Message), LoggerLib.LogLevel.ERROR);
                }
                finally
                {
                    try
                    {
                        this.checkForNewObjects();
                    }
                    catch(Exception ex)
                    {
                        this.currentUser.Logger.Log(string.Format("GeoTrackingService - syncDbAsync - Error when trying to check for new objects: {0}", ex.Message), LoggerLib.LogLevel.ERROR);
                    }
                }
            });
        }

        private void checkForNewObjects()
        {
            string queryResult = this.currentUser.UlWrapper.ExecuteQuery("SELECT * FROM SAM_USER_OBJECT_TRACKING");

            if (queryResult.Length <= 2)
            {
                return;
            }

            this.locationTrackingEvent(this, GeoTrackingServicePluginResults.newObjects(queryResult).Stringify());
        }

        /// <summary>
        /// This is the event handler for PositionChanged events.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnPositionChanged(Geolocator sender, PositionChangedEventArgs e)
        {
            if (this.currentUser == null)
            {
                return;
            }

            Geoposition pos = e.Position;
            GeoLocation newLocation = new GeoLocation(pos.Coordinate.Point.Position.Latitude, pos.Coordinate.Point.Position.Longitude);
            this.currentUser.Logger.Log(string.Format("GeoTrackingService - OnPositionChanged - New Location received"), LoggerLib.LogLevel.INFO);

            if (this.currentUser.UlWrapper.isSyncInProgress())
            {
                this.currentUser.Logger.Log(string.Format("GeoTrackingService - OnPositionChanged - Another Sync currently in progress, not writing it to the database. Caching the location."), LoggerLib.LogLevel.INFO);
                this.currentLocation = newLocation;
                return;
            }

            try
            {
                this.insertLocation(newLocation);
                this.currentLocation = newLocation;
            }
            catch(Exception ex)
            {
                this.currentUser.Logger.Log(string.Format("GeoTrackingService - OnPositionChanged - Error when trying handle new location: {0}", ex.Message), LoggerLib.LogLevel.ERROR);
            }

        }

        /// <summary>
        /// This is the event handler for StatusChanged events.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnStatusChanged(Geolocator sender, StatusChangedEventArgs e)
        {
            if (this.currentUser == null)
            {
                return;
            }

            switch (e.Status)
            {
                case PositionStatus.Ready:
                    this.currentUser.Logger.Log(string.Format("GeoTrackingService - OnStatusChanged - Location platform is providing valid data"), LoggerLib.LogLevel.INFO);
                    break;

                case PositionStatus.Initializing:
                    this.currentUser.Logger.Log(string.Format("GeoTrackingService - OnStatusChanged - Location platform is acquiring a fix. It may or may not have data. Or the data may be less accurate."), LoggerLib.LogLevel.INFO);
                    break;

                case PositionStatus.NoData:
                    this.currentUser.Logger.Log(string.Format("GeoTrackingService - OnStatusChanged - Location platform could not obtain location data."), LoggerLib.LogLevel.ERROR);
                    break;

                case PositionStatus.Disabled:
                    this.currentUser.Logger.Log(string.Format("GeoTrackingService - OnStatusChanged - The permission to access location data is denied by the user or other policies."), LoggerLib.LogLevel.ERROR);
                    break;

                case PositionStatus.NotInitialized:
                    this.currentUser.Logger.Log(string.Format("GeoTrackingService - OnStatusChanged - The location platform is not initialized. This indicates that the application has not made a request for location data."), LoggerLib.LogLevel.ERROR);
                    break;

                case PositionStatus.NotAvailable:
                    this.currentUser.Logger.Log(string.Format("GeoTrackingService - OnStatusChanged - The location platform is not available on this version of the OS."), LoggerLib.LogLevel.ERROR);
                    break;

                default:
                    break;
            }
        }
    }

    public sealed class GeoTrackingServicePluginResults
    {
        public static MobilinkPluginResult serviceStarted()
        {
            return new MobilinkPluginResult();
        }

        public static MobilinkPluginResult locationChanged(double latitude, double longitude)
        {
            return new MobilinkPluginResult();
        }

        public static JsonObject newObjects(string jsonResult)
        {
            var progress = new JsonObject();
            progress.Add("EVENT", JsonValue.CreateNumberValue(3));
            progress.Add("OBJECTS", JsonValue.CreateStringValue(jsonResult));

            return progress;
        }
    }

    public sealed class GeoLocation
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public GeoLocation(double latitude, double longitude)
        {
            this.Longitude = longitude;
            this.Latitude = latitude;
        }
    }
}

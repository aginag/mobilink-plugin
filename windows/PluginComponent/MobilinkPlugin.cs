using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Windows.Foundation;
using LoggerLib;
using Windows.Storage;
using UltraLiteComponent;
using Windows.System.Display;
using Windows.Foundation.Metadata;
using InternetConnectionInfoComponent;
using Windows.Security.ExchangeActiveSyncProvisioning;
using Windows.Data.Json;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel;
using Windows.Foundation.Collections;
using System.Collections.Concurrent;
using System.IO;
using ZipComponent;
using UtilComponent;
using Windows.Storage.Streams;
using Windows.Graphics.Imaging;

namespace PluginComponent
{
    #region helper classes
    /// <summary>
    /// due to limitation in passing exceptions to WinJS, this class is beeing used
    /// signalling success or failure with an exception message to WinJS
    /// </summary>
    public sealed class MobilinkPluginResult
    {
        /// <summary>
        /// Default constructor with 'Success = true;'
        /// </summary>
        public MobilinkPluginResult()
        {
            Success = true;
            ErrorMessage = "";
            Value = "";
        }

        public MobilinkPluginResult(object result, bool success, string errorMessage)
        {
            Value = result;
            Success = success;
            ErrorMessage = errorMessage;
        }

        public object Value { get; set; }
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
    }

    public sealed class KeyValuePair
    {
        public KeyValuePair(object key, object val)
        {
            this.Key = key;
            this.Value = val;
        }

        public object Key { get; set; }
        public object Value { get; set; }
    }


    public sealed class MlUser
    {

        private const string _logFileName = "Log.txt";
        private const string _transactionLogFileName = "TransactionLog.txt";
        private MlUser() { }
        public static MlUser CreateUser(string name, string pw, StorageFolder folder, StorageFolder attachmentFolder, MlConnectionInfo conInfo)
        {
            var user = new MlUser();
            user.ConInfo = conInfo;
            user.Name = name;
            user.Pw = pw;
            user.Folder = folder;
            user.AttachmentFolder = attachmentFolder;
            if (conInfo != null)
            {
                //Bitbucket
                // change DataBase file path - STP fix
                //user.ConInfo.DbFilePath = Path.Combine(ApplicationData.Current.LocalFolder.Path, name, conInfo.DbFileName);
                user.ConInfo.DbFilePath = Path.Combine(user.Folder.Path, conInfo.DbFileName);
                //Bitbucket
            }
            return user;
        }
        public IAsyncOperation<bool> DbFileExistsAsync()
        {
            return Task.Run(async () =>
            {
                try
                {
                    var file = await Folder.GetFileAsync(ConInfo.DbFileName);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }).AsAsyncOperation();
        }

        public MlConnectionInfo ConInfo { get; set; }
        public string Name { get; private set; }
        public string Pw { get; set; }
        public Logger Logger { get; set; }
        public Logger TransactionLogger { get; set; }
        public StorageFolder Folder { get; set; }
        public StorageFolder AttachmentFolder { get; set; }
        public ULWrapper UlWrapper { get; set; }

        /// <summary>
        /// (re)creates Logger, TransactionLogger and UlWrapper;
        /// call this method whenever ConInfo changes
        /// </summary>
        /// <param name="initializeCompletely">if true, also creates the DB connections and allows DB access</param>
        /// <returns></returns>
        public IAsyncAction InitializeAsync(bool initializeCompletely)
        {
            return Task.Run(async () =>
            {
                Logger = await Logger.CreateAsync(Folder, _logFileName, LogLevel.VERBOSE);
                TransactionLogger = await Logger.CreateAsync(Folder, _transactionLogFileName, LogLevel.VERBOSE);

                if (UlWrapper != null)
                {
                    UlWrapper.Dispose(hardCloseConnections: true);
                }
                //Bitbucket
                //var encryptionKey = "";
                //if (MobilinkPlugin.getKeyValue("SAMEncKey" + Name).Value != null)
                //    encryptionKey = MobilinkPlugin.getKeyValue("SAMEncKey" + Name).Value.ToString();
                var encryptionKey = MobilinkPlugin.sGetKeyValue("SAMEncKey" + Name).ToString();
                //Bitbucket
                UlWrapper = new ULWrapper(Logger, TransactionLogger, Folder, ConInfo, encryptionKey);
                if (initializeCompletely)
                {
                    UlWrapper.CreateDbConnections();
                    UlWrapper.AllowDbAccess();
                }
            }).AsAsyncAction();
        }

        public StorageFolder GetAttachmentFolder()
        {
            return this.AttachmentFolder;
        }

        public void Release()
        {
            Logger = null;
            TransactionLogger = null;
            if (UlWrapper != null)
            {
                UlWrapper.Dispose(hardCloseConnections: true);
                UlWrapper = null;
            }
        }
    }

    public sealed class MlSqlStatement
    {
        public MlSqlStatement(string tableName, string sql)
        {
            this.TableName = tableName;
            this.SqlStatement = sql;
        }

        public string TableName { get; set; }
        public string SqlStatement { get; set; }
    }

    #endregion

    //[Threading(ThreadingModel.STA)]
    //[MarshalingBehavior(MarshalingType.None)]
    public sealed class MobilinkPlugin
    {
        private static object _lock = new Object();
        private static bool _initialized = false;
        private static StorageFolder _usersFolder = null;
        private static ConcurrentBag<MlUser> _allUsers = null;
        private static MlUser _curUser = null;
        private static MlConnectionInfo _conInfo = null;
        // private static string _dbFileName = null;
        //private const string _dbFileName = "sam.udb";

        private static Logger _logger = null;
        private const string _logFileName = "Log.txt";
        private const LogLevel _logLevel = LogLevel.VERBOSE;

        private static readonly string _guid = GetId();
        private const string _debugGuid = "1234567890";

        //use complete lowercase naming for events otherwise WinJS may not find the event
        public event EventHandler<object> ftevent;
        public event EventHandler<object> syncevent;
        public event EventHandler<object> locationtrackingevent;

        private GeoTrackingService trackingService;

        #region initialization
        /// <summary>
        /// blocks calling thread
        /// </summary>
        public MobilinkPlugin()
        {
            lock (_lock)
            {
                if (!_initialized)
                {
                    var task = Task.Run(async () =>
                    {
                        try
                        {
                            //await CleanUpTempFolder();
                            var logFolder = ApplicationData.Current.LocalFolder;
                            _logger = await Logger.CreateAsync(logFolder, _logFileName, _logLevel);
                            var packageName = Package.Current.Id.Name;
                            var versionInfo = Package.Current.Id.Version;
                            var appVersion = string.Format("{0}.{1}.{2}.{3}", versionInfo.Major, versionInfo.Minor, versionInfo.Build, versionInfo.Revision);
                            _logger.Log(string.Format("'{0}', version '{1}' starting", packageName, appVersion), LogLevel.INFO);


                            _usersFolder = await ApplicationData.Current.LocalFolder.CreateFolderAsync("Users", CreationCollisionOption.OpenIfExists);
                            _allUsers = await GetAllUsers(_usersFolder, _conInfo);

                            _initialized = true;
                        }
                        catch (Exception ex)
                        {
                            var errorMessage = string.Format("Error: initializing MobilinkPlugin instance: {0}", ex.Message);

                            System.Diagnostics.Debug.WriteLine(errorMessage);
                            if (_logger != null)
                            {
                                _logger.Log(errorMessage, LogLevel.ERROR);
                                _logger.Log(ex.StackTrace, LogLevel.VERBOSE);
                            }
                            throw ex;
                        }
                    });
                    task.Wait();
                }
            }
        }

        public MobilinkPluginResult DefineMlInfo(string server, string port, string subscription, string version, string publication, string dbName, string protocol, string urlSuffix)
        {
            var task = DefineMlInfoInternal(server, port, subscription, version, publication, dbName, protocol, urlSuffix);
            task.Wait();
            var taskResult = task.Result;
            return taskResult;
        }

        private Task<MobilinkPluginResult> DefineMlInfoInternal(string server, string port, string subscription, string version, string publication, string dbName, string protocol, string urlSuffix)
        {
            return Task.Run(async () =>
            {
                //_dbFileName = dbName + ".udb";
                _conInfo = new MlConnectionInfo();
                _conInfo.HostName = server;
                _conInfo.Port = int.Parse(port);
                _conInfo.Subscription = subscription;
                _conInfo.Publication = publication;
                _conInfo.Version = version;
                _conInfo.DbFileName = dbName + ".udb";
                _conInfo.Protocol = protocol;
                _conInfo.UrlSuffix = urlSuffix;

                foreach (var user in _allUsers)
                {
                    var conInfo = new MlConnectionInfo();
                    conInfo.HostName = server;
                    conInfo.Port = int.Parse(port);
                    conInfo.Subscription = subscription;
                    conInfo.Publication = publication;
                    conInfo.Version = version;
                    conInfo.DbFileName = _conInfo.DbFileName;
                    conInfo.Protocol = _conInfo.Protocol;
                    conInfo.UrlSuffix = _conInfo.UrlSuffix;

                    //Bitbucket
                    // change DataBase file path - STP fix
                    //conInfo.DbFilePath = Path.Combine(ApplicationData.Current.LocalFolder.Path, user.Name, _conInfo.DbFileName);
                    conInfo.DbFilePath = Path.Combine(user.Folder.Path, _conInfo.DbFileName);
                    //Bitbucket
                    user.ConInfo = conInfo;
                }
                //TODO: what shall happen with the current logged in user? does he need to re-login?
                if (_curUser != null)
                {
                    await _curUser.InitializeAsync(initializeCompletely: true);
                }

                return new MobilinkPluginResult();
            });
        }
        #endregion

        #region check new user
        public MobilinkPluginResult CheckNewUser(string userName)
        {
            var task = CheckNewUserInternal(userName);
            task.Wait();
            var taskResult = task.Result;
            return taskResult;
        }

        public IAsyncOperation<MobilinkPluginResult> CheckNewUserAsync(string userName)
        {
            return CheckNewUserInternal(userName).AsAsyncOperation<MobilinkPluginResult>();
        }

        /// <summary>
        /// returns true if user has not successfully downloaded the DB yet, false otherwise
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        private Task<MobilinkPluginResult> CheckNewUserInternal(string userName)
        {
            return Task.Run(async () =>
            {
                var result = new MobilinkPluginResult();
                result.Value = true;
                MlUser user = null;

                if (TryGetUser(userName, _allUsers, out user))
                {
                    if (await user.DbFileExistsAsync())
                    {
                        result.Value = false;
                    }
                }
                return result;
            });
        }
        #endregion

        #region login

        /// <summary>
        /// async Login
        /// </summary>
        /// <param name="user"></param>
        /// <param name="pwd"></param>
        /// <returns></returns>
        public IAsyncOperation<MobilinkPluginResult> LoginAsync(string user, string pwd)
        {
            return LoginInternal(user, pwd).AsAsyncOperation<MobilinkPluginResult>();
        }

        /// <summary>
        /// sync, blocks calling thread
        /// </summary>
        /// <param name="user"></param>
        /// <param name="pwd"></param>
        /// <returns></returns>
        public MobilinkPluginResult Login(string user, string pwd)
        {
            var task = LoginInternal(user, pwd);
            task.Wait();
            var taskResult = task.Result;
            return taskResult;
        }

        public MobilinkPluginResult setUserPwd(string user, string pwd)
        {
            var result = new MobilinkPluginResult();
            var task = setUserPwdInternal(user, pwd);
            //Bitbucket
            task.Wait();
            //Bitbucket
            var taskResult = task.Result;
            return taskResult;
        }

        public MobilinkPluginResult getKeyValue(string key)
        {
            return MobilinkPlugin.sGetKeyValue(key);
        }

        public static MobilinkPluginResult sGetKeyValue(string key)
        {
            var result = new MobilinkPluginResult();
            var localSettings = ApplicationData.Current.LocalSettings;
            result.Value = (string)localSettings.Values[key];
            result.Success = true;
            return result;
        }

        private Task<MobilinkPluginResult> setUserPwdInternal(string userName, string pwd)
        {
            _logger.Log(string.Format("MobilinkPlugin.LoginInternal: user {0}", userName), LogLevel.VERBOSE);

            return Task.Run<MobilinkPluginResult>(async () =>
            {
                var result = new MobilinkPluginResult();
                var localSettings = ApplicationData.Current.LocalSettings;

                SaveUserCredentials(userName, pwd);
                localSettings.Values["SAMPwd" + userName] = pwd;

                if (!TryGetUser(userName, _allUsers, out _curUser))
                {
                    // Create new user
                    _curUser = await CreateUser(userName, pwd, _usersFolder, _conInfo);
                    _allUsers.Add(_curUser);
                }
                else
                {
                    // Set entered password
                    _curUser.Pw = pwd;
                }

                await _curUser.InitializeAsync(initializeCompletely: false);

                return result;
            });
        }

        private Task<MobilinkPluginResult> LoginInternal(string userName, string pwd)
        {
            _logger.Log(string.Format("MobilinkPlugin.LoginInternal: user {0}", userName), LogLevel.VERBOSE);

            return Task.Run<MobilinkPluginResult>(async () =>
            {
                var result = new MobilinkPluginResult();
                try
                {
                    //case user changes -> release resources of previous user
                    if (_curUser != null && userName != _curUser.Name)
                    {
                        _curUser.Release();
                    }

                    //changes current user object;

                    //case: existing user
                    //user already synched, check credentials
                    if (!DoUserCredentialsMatch(userName, pwd))
                    {
                        result.Success = false;
                        result.ErrorMessage = "User credentials wrong";
                        _logger.Log(string.Format("MobilinkPlugin.LoginInternal: user '{0}'; wrong credentials", userName),
                            LogLevel.VERBOSE);
                    }
                    else
                    {
                        _curUser.Release(); //in case the current user logs in again, for whatever reasons...
                        //user has been authenticated -> ready for accessing the DB
                        await _curUser.InitializeAsync(initializeCompletely: true);
#if !DEBUG
                        SetLogLevelFromDb();
#endif
                    }


                }
                catch (Exception ex)
                {
                    var message = string.Format("MobilinkPlugin.LoginInternal: user '{0}'; error: {1}", userName, ex.Message);
                    _logger.Log(message, LogLevel.ERROR);

                    result.Success = false;
                    result.ErrorMessage = "Login error: " + ex.Message;
                }

                return result;
            });
        }

        #endregion

        #region queries
        public MobilinkPluginResult ExecuteQuery(string sql)
        {
            var result = new MobilinkPluginResult();
            try
            {
                result.Value = _curUser.UlWrapper.ExecuteQuery(sql);
            }
            catch (Exception ex)
            {
                result.ErrorMessage = "error_query";
                result.Success = false;
            }
            return result;
        }

        public IAsyncOperation<MobilinkPluginResult> ExecuteQueryAsync(string sql)
        {
            return Task.Run(() =>
            {
                var result = new MobilinkPluginResult();
                try
                {

                    result.Value = _curUser.UlWrapper.ExecuteQuery(sql);
                }
                catch (Exception ex)
                {
                    result.ErrorMessage = "error_query";
                    result.Success = false;
                }
                return result;
            }).AsAsyncOperation<MobilinkPluginResult>();
        }


        public MobilinkPluginResult ExecuteQueriesSequentially([ReadOnlyArray()] MlSqlStatement[] sqlStatements)
        {
            var task = ExecuteQueriesSequentiallyInternal(sqlStatements);
            task.Wait();
            return task.Result;
        }

        public IAsyncOperation<MobilinkPluginResult> ExecuteQueriesSequentiallyAsync([ReadOnlyArray()] MlSqlStatement[] sqlStatements)
        {
            return ExecuteQueriesSequentiallyInternal(sqlStatements).AsAsyncOperation<MobilinkPluginResult>();
        }

        public IAsyncOperation<MobilinkPluginResult> ExecuteUpdatesSequentiallyAsync(string sql)
        {
            return Task.Run(() =>
            {
                var result = new MobilinkPluginResult();
                try
                {
                    var statements = JsonArray.Parse(sql);
                    _curUser.UlWrapper.ExecuteUpdatesSequentially(statements);
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.ErrorMessage = "error_query";
                    result.Success = false;
                }
                return result;
            }).AsAsyncOperation<MobilinkPluginResult>();
        }

        public IAsyncOperation<MobilinkPluginResult> StartLocationTrackingAsync(string interval)
        {
            return Task.Run(() =>
            {
                var result = new MobilinkPluginResult();
                try
                {
                    if (trackingService == null)
                    {
                        trackingService = new GeoTrackingService(this.locationtrackingevent);
                    }

                    trackingService.start(_curUser, int.Parse(interval));
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.ErrorMessage = "error_query";
                    result.Success = false;
                }
                return result;
            }).AsAsyncOperation<MobilinkPluginResult>();
        }

        public IAsyncOperation<MobilinkPluginResult> StopLocationTrackingAsync()
        {
            return Task.Run(() =>
            {
                var result = new MobilinkPluginResult();
                try
                {
                    if (trackingService != null)
                    {
                        trackingService.stop();
                    }

                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.ErrorMessage = "error_query";
                    result.Success = false;
                }
                return result;
            }).AsAsyncOperation<MobilinkPluginResult>();
        }

        private Task<MobilinkPluginResult> ExecuteQueriesSequentiallyInternal([ReadOnlyArray()] MlSqlStatement[] sqlStatements)
        {
            return Task.Run(() =>
            {
                var result = new MobilinkPluginResult();
                try
                {
                    var statements = GetStatements(sqlStatements);
                    var results = _curUser.UlWrapper.ExecuteQueriesSequentiallyAsJsonArray(statements);
                    result.Value = CreateQueryResultStructure(sqlStatements, results).Stringify();
                }
                catch (Exception ex)
                {
                    result.ErrorMessage = "error_query";
                    result.Success = false;
                }
                return result;
            });
        }

        public IAsyncOperation<MobilinkPluginResult> ExecuteUpdate(string sql)
        {
            return Task.Run(() =>
            {
                var result = new MobilinkPluginResult();
                try
                {
                    result.Value = _curUser.UlWrapper.ExecuteUpdate(sql);
                }
                catch (Exception ex)
                {
                    result.ErrorMessage = "error_query";
                    result.Success = false;
                }
                return result;
            }).AsAsyncOperation<MobilinkPluginResult>();
        }

        public IAsyncOperation<MobilinkPluginResult> ExecuteInsert(string sql)
        {
            return Task.Run(() =>
            {
                var result = new MobilinkPluginResult();
                try
                {
                    result.Value = _curUser.UlWrapper.ExecuteUpdate(sql);
                }
                catch (Exception ex)
                {
                    result.ErrorMessage = "error_query";
                    result.Success = false;
                }
                return result;
            }).AsAsyncOperation<MobilinkPluginResult>();
        }

        public IAsyncOperation<MobilinkPluginResult> ExecuteStatementsAndCommit(string sqlStatements)
        {
            return Task.Run(() =>
            {
                var result = new MobilinkPluginResult();
                try
                {
                    var statements = JsonArray.Parse(sqlStatements);
                    _curUser.UlWrapper.ExecuteStatementsAndCommit(statements);
                    var newId = _curUser.UlWrapper.NewObjectUuid;
                    result.Value = newId;
                }
                catch (Exception ex)
                {
                    result.ErrorMessage = "error_query";
                    result.Success = false;
                }
                return result;
            }).AsAsyncOperation<MobilinkPluginResult>();
        }
        #endregion

        #region  db sync

        public IAsyncOperation<MobilinkPluginResult> SyncAsync(string syncMode, string syncModel, bool pingMode)
        {
            return SyncInternal(syncMode, syncModel, pingMode).AsAsyncOperation<MobilinkPluginResult>();
        }

        private Task<MobilinkPluginResult> SyncInternal(string syncMode, string syncModel, bool pingMode)
        {
            return Task.Run<MobilinkPluginResult>(() =>
            {
                var result = new MobilinkPluginResult();
                try
                {
                    _curUser.UlWrapper.SyncDbWithObserver(_curUser.Name, _curUser.Pw, syncevent, pingMode, syncMode, syncModel);
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.ErrorMessage = ex.Message;
                }

                try
                {
                    SetLogLevelFromDb();
                    //await UpdateSyncedFiles();
                }
                catch (Exception ex)
                {
                    var message = "error_sync : " + ex.Message;
                    result.Success = false;
                    result.ErrorMessage = message;
                }

                if (result.Success == true)
                {
                    result.Value = "1000";
                }

                return result;
            });
        }

        public MobilinkPluginResult CancelSync()
        {
            _curUser.Logger.Log("MobilinkPlugin.CancelSync called.", LogLevel.INFO);
            var result = new MobilinkPluginResult();
            _curUser.UlWrapper.CancelSync();
            return result;
        }
        #endregion

        #region file sync
        public IAsyncOperation<MobilinkPluginResult> DownloadFileAsync(string fileName)
        {
            return DownloadFileInternal(fileName).AsAsyncOperation<MobilinkPluginResult>();
        }

        private Task<MobilinkPluginResult> DownloadFileInternal(string fileName)
        {
            return Task.Run(async () =>
            {
                var result = new MobilinkPluginResult();
                result.Value = true;

                try
                {
                    var destFolder = ApplicationData.Current.LocalFolder;
                    var fileExists = false;
                    try
                    {
                        var file = await destFolder.GetItemAsync(fileName);
                        fileExists = true;
                    }
                    catch { }

                    if (!fileExists)
                    {
                        _curUser.UlWrapper.TransferFileWithObserver(ULTransferType.Download,
                            _curUser.Name, _curUser.Pw, fileName, fileName, fileName, destFolder, ftevent);
                    }

                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.ErrorMessage = ex.Message;
                }
                return result;
            });
        }

        public IAsyncOperation<MobilinkPluginResult> UploadFileAsync(string fileName)
        {
            return UploadFileInternal(fileName).AsAsyncOperation<MobilinkPluginResult>();
        }

        private Task<MobilinkPluginResult> UploadFileInternal(string fileName)
        {
            return Task.Run(async () =>
            {
                var result = new MobilinkPluginResult();
                result.Value = true;

                try
                {
                    var destFolder = _curUser.GetAttachmentFolder();
                    var file = await destFolder.GetItemAsync(fileName);
                    if (file == null)
                    {
                        throw new Exception("file not found: " + destFolder.Path + @"\" + fileName);
                    }
                    else
                    {
                        _curUser.UlWrapper.TransferFileWithObserver(ULTransferType.Upload,
                            _curUser.Name, _curUser.Pw, fileName, fileName, fileName, destFolder, ftevent);
                    }
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.ErrorMessage = ex.Message;
                }
                return result;
            });
        }

        public MobilinkPluginResult CancelFileTransfer(string fileId)
        {
            _logger.Log(string.Format("MobilinkPlugin.CancelFileTransfer fileId: '{0}'", fileId), LogLevel.INFO);
            var result = new MobilinkPluginResult();
            result.Success = _curUser.UlWrapper.CancelFileTransfer(fileId);
            return result;
        }


        public IAsyncOperation<MobilinkPluginResult> UploadDbFileAsync(string fileName)
        {
            return UploadDbFileInternal(fileName).AsAsyncOperation<MobilinkPluginResult>();
        }

        /// <summary>
        /// don't access the DB while this method is executing
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private Task<MobilinkPluginResult> UploadDbFileInternal(string fileName)
        {
            StorageFile zipFile = null;
            return Task.Run(async () =>
            {
                var result = new MobilinkPluginResult();
                var timeStamp = Util.GetCurrentUtcTimeStamp();
                try
                {
                    _curUser.Logger.Log(string.Format("MobilinkPlugin.UploadDbFileInternal called"), LogLevel.INFO);

                    //close all connections to the DB, as the zipping needs exclusive access
                    _curUser.UlWrapper.StopDbAccess();
                    _curUser.UlWrapper.CloseDbConnections(hardClose: false);

                    //zip the DB file
                    var dbFile = await _curUser.Folder.GetFileAsync(_curUser.ConInfo.DbFileName);
                    zipFile = await Zipper.ZipAsync(dbFile, _curUser.Folder, fileName);

                    //upload zipped DB
                    _curUser.UlWrapper.TransferFileWithObserver(ULTransferType.Upload, _curUser.Name, _curUser.Pw, fileName, fileName, fileName, _curUser.Folder, ftevent);

                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.ErrorMessage = ex.Message;
                    var message = string.Format("MobilinkPlugin.UploadDbFileInternal: error: {0}", ex.Message);
                    _curUser.Logger.Log(message, LogLevel.ERROR);
                    _curUser.Logger.Log(ex.StackTrace, LogLevel.VERBOSE);
                }
                finally
                {
                    _curUser.UlWrapper.CreateDbConnections();
                    _curUser.UlWrapper.AllowDbAccess();
                    if (zipFile != null)
                    {
                        try
                        {
                            //no need to wait til the file gets deleted
                            zipFile.DeleteAsync();
                        }
                        catch { }
                    }
                }
                return result;
            });
        }

        public IAsyncOperation<MobilinkPluginResult> UploadLogFileAsync(string logCopyName)
        {
            return UploadLogFileInternal(logCopyName).AsAsyncOperation<MobilinkPluginResult>();
        }

        private Task<MobilinkPluginResult> UploadLogFileInternal(string logCopyName)
        {
            StorageFile zipFile = null;
            return Task.Run(async () =>
            {
                var result = new MobilinkPluginResult();
                try
                {
                    _curUser.Logger.Log(string.Format("MobilinkPlugin.UploadLogFileInternal called, logCopyName: ", logCopyName), LogLevel.INFO);

                    //zip the log file
                    zipFile = await Zipper.ZipAsync(_curUser.Logger.GetLogFile(), _curUser.Folder, logCopyName);

                    //upload zipped log file
                    _curUser.UlWrapper.TransferFile(ULTransferType.Upload, _curUser.Name, _curUser.Pw, zipFile.Name, zipFile.Name, zipFile.Name, _curUser.Folder);

                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.ErrorMessage = ex.Message;
                    var message = string.Format("MobilinkPlugin.UploadLogFileInternal: error: {0}", ex.Message);
                    _curUser.Logger.Log(message, LogLevel.ERROR);
                    _curUser.Logger.Log(ex.StackTrace, LogLevel.VERBOSE);
                }
                finally
                {
                    if (zipFile != null)
                    {
                        try
                        {
                            //no need to wait til the file gets deleted
                            zipFile.DeleteAsync();
                        }
                        catch { }
                    }
                }
                return result;
            });
        }

         public MobilinkPluginResult ResizeImage(string sFileUrlString, double dCompressionQuality, int fileTargetSize)
        {
            var task = ResizeImageInternal(sFileUrlString, dCompressionQuality, fileTargetSize);
            task.Wait();
            return task.Result;
        }

        private Task<MobilinkPluginResult> ResizeImageInternal(string sFileUrlString, double dCompressionQuality, int fileTargetSize)
        {
            return Task.Run(async () =>
            {
                var result = new MobilinkPluginResult();

                try
                {
                    _curUser.Logger.Log("Compression Request received for file: " + sFileUrlString, LogLevel.INFO);

                    if (FileHasSupportedType(sFileUrlString))
                    {
                        Uri uri = new Uri(sFileUrlString);
                        StorageFile file = await StorageFile.GetFileFromPathAsync(uri.AbsoluteUri);

                        using (IRandomAccessStream fileStream = await file.OpenAsync(FileAccessMode.ReadWrite))
                        {
                            var propertySet = new BitmapPropertySet();
                            var qualityValue = new BitmapTypedValue(dCompressionQuality, PropertyType.Single);
                            propertySet.Add("ImageQuality", qualityValue);

                            var encoder = await BitmapEncoder.CreateAsync(BitmapEncoder.JpegEncoderId, fileStream, propertySet);
                            BitmapDecoder decoder = await BitmapDecoder.CreateAsync(fileStream);
                            uint oriWidth = decoder.PixelWidth;
                            uint oriHeight = decoder.PixelHeight;

                            uint newWidth = oriWidth;
                            uint newHeight = oriHeight;

                            float widthRatio = (float) fileTargetSize / oriWidth;
                            float heightRatio = (float) fileTargetSize / oriHeight;

                            // Figure out what our orientation is, and use that to form the rectangle
                            if (widthRatio > heightRatio)
                            {
                                newWidth = (uint)Math.Round(oriWidth * heightRatio, 0);
                                newHeight = (uint)Math.Round(oriHeight * heightRatio, 0);
                            }
                            else
                            {
                                newWidth = (uint)Math.Round(oriWidth * widthRatio, 0);
                                newHeight = (uint)Math.Round(oriHeight * widthRatio, 0);
                            }

                            PixelDataProvider pd = await decoder.GetPixelDataAsync();
                            encoder.BitmapTransform.ScaledHeight = newHeight;
                            encoder.BitmapTransform.ScaledWidth = newWidth;

                            encoder.SetPixelData(decoder.BitmapPixelFormat, decoder.BitmapAlphaMode, oriWidth, oriHeight, decoder.DpiX, decoder.DpiY, pd.DetachPixelData());
                            decoder = null;

                            // BitmapEncoder doesn't clear any existing data in the stream
                            // So we have to set the size to 0
                            fileStream.Seek(0);
                            fileStream.Size = 0;

                            await encoder.FlushAsync();
                            await fileStream.FlushAsync();

                            fileStream.Dispose();
                        }
                    }
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    var message = string.Format("MobilinkPlugin.ResizeImageInternal: error resizing files for user: {0}; error: {1}",
                        _curUser.Name, ex.Message);
                    _curUser.Logger.Log(message, LogLevel.ERROR);
                    _curUser.Logger.Log(ex.StackTrace, LogLevel.ERROR);
                    result.ErrorMessage = message;
                }
                return result;
            });

        }

        private Boolean FileHasSupportedType (string sFileName)
        {
           return sFileName.ToUpper().EndsWith(".JPEG") || sFileName.ToUpper().EndsWith(".JPG") || sFileName.ToUpper().EndsWith(".PNG");
        }

        #endregion

        #region unzip
        public MobilinkPluginResult UnzipFile(string filePath, string destFolder)
        {
            _curUser.Logger.Log(string.Format("MobilinkPlugin.UnzipFile"), LogLevel.INFO);
            var task = UnzipFileInternal(filePath, destFolder);
            task.Wait();
            var taskResult = task.Result;
            return taskResult;
        }

        public IAsyncOperation<MobilinkPluginResult> UnzipFileAsync(string filePath, string destFolder)
        {
            _curUser.Logger.Log(string.Format("MobilinkPlugin.UnzipFileAsync"), LogLevel.INFO);
            return UnzipFileInternal(filePath, destFolder).AsAsyncOperation<MobilinkPluginResult>();
        }

        private Task<MobilinkPluginResult> UnzipFileInternal(string filePath, string destFolderPath)
        {
            return Task.Run<MobilinkPluginResult>(async () =>
            {
                var result = new MobilinkPluginResult();
                try
                {
                    var cleanedPath = Util.ToWindowsConformPath(destFolderPath);
                    var destFolder = await StorageFolder.GetFolderFromPathAsync(cleanedPath);

                    cleanedPath = Util.ToWindowsConformPath(filePath);
                    var zipFile = await Windows.Storage.StorageFile.GetFileFromPathAsync(cleanedPath);

                    //result.Success = await Zipper.UnZipFile(zipFile, destFolder);
                    result.Success = await Zipper.UnZipAsync(zipFile, destFolder);
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.ErrorMessage = "error_file_unzip";

                    var message = string.Format("MobilinkPlugin.UnzipFileInternal: Error unzipping file: {0}", ex.Message);
                    _logger.Log(message, LogLevel.ERROR);
                    _logger.Log(ex.StackTrace, LogLevel.VERBOSE);
                }

                return result;
            });
        }
        #endregion

        #region misc
        public MobilinkPluginResult SetNewPassword(string newPw, string currentPw)
        {
            var result = new MobilinkPluginResult();
            try
            {
                _curUser.Logger.Log(string.Format("MobilinkPlugin.SetNewPassword setting new password for user {0}", _curUser), LogLevel.INFO);
                if (DoUserCredentialsMatch(_curUser.Name, currentPw))
                {
                    SaveUserCredentials(_curUser.Name, newPw);
                    _curUser.Pw = newPw;
                }
                else
                {
                    throw new Exception("provided current password does not match");
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.ErrorMessage = "Error setting password: " + ex.Message;
                var message = string.Format("MobilinkPlugin.SetNewPassword: error setting new pw: {0}", ex.Message);
                _curUser.Logger.Log(message, LogLevel.ERROR);
                _curUser.Logger.Log(ex.StackTrace, LogLevel.VERBOSE);
            }
            return result;
        }

        public MobilinkPluginResult DeleteLogFiles()
        {
            var task = DeleteLogFilesInternal();
            task.Wait();
            return task.Result;
        }

        private Task<MobilinkPluginResult> DeleteLogFilesInternal()
        {
            return Task.Run(async () =>
            {
                var result = new MobilinkPluginResult();
                try
                {
                    await _curUser.Logger.Empty();
                    await _curUser.TransactionLogger.Empty();

                    SetLogLevelFromDb();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    var message = string.Format("MobilinkPlugin.DeleteLogFilesInternal: error delete log files for user: {0}; error: {1}",
                        _curUser.Name, ex.Message);
                    _curUser.Logger.Log(message, LogLevel.ERROR);
                    _curUser.Logger.Log(ex.StackTrace, LogLevel.ERROR);
                    result.ErrorMessage = message;
                }
                return result;
            });
        }


        public MobilinkPluginResult ResetDb()
        {
            var task = ResetDbInternal();
            task.Wait();
            return task.Result;
        }

        public IAsyncOperation<MobilinkPluginResult> ResetDbAsync()
        {
            return ResetDbInternal().AsAsyncOperation<MobilinkPluginResult>();
        }

        private Task<MobilinkPluginResult> ResetDbInternal()
        {
            return Task.Run(async () =>
            {
                var result = new MobilinkPluginResult();
                try
                {
                    _curUser.UlWrapper.StopDbAccess();
                    _curUser.UlWrapper.CloseDbConnections(hardClose: false);
                    var dbFile = await _curUser.Folder.GetFileAsync(_curUser.ConInfo.DbFileName);
                    await dbFile.DeleteAsync(StorageDeleteOption.PermanentDelete);
                    //_curUser.UlWrapper.AllowDbAccess();

                    await _curUser.Logger.Empty();
                    await _curUser.TransactionLogger.Empty();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    var message = string.Format("MobilinkPlugin.ResetDbInternal: error resetting DB for user: {0}; error: {1}",
                        _curUser.Name, ex.Message);
                    _curUser.Logger.Log(message, LogLevel.ERROR);
                    _curUser.Logger.Log(ex.StackTrace, LogLevel.ERROR);
                    result.ErrorMessage = message;
                }
                return result;
            });
        }

        public MobilinkPluginResult ClearUserData()
        {
            var task = ClearUserDataInternal();
            task.Wait();
            return task.Result;
        }

        private Task<MobilinkPluginResult> ClearUserDataInternal()
        {
            return Task.Run(async () =>
            {
                var result = new MobilinkPluginResult();
                try
                {
                    var destFolder = ApplicationData.Current.LocalFolder;
                    var files = await destFolder.GetItemsAsync();
                    var localSettings = ApplicationData.Current.LocalSettings;

                    foreach (var user in _allUsers)
                    {
                        localSettings.Values.Remove("SAMPwd" + user.Name);
                        localSettings.Values.Remove(user.Name + "_name");
                        localSettings.Values.Remove(user.Name + "_pw");
                    }

                    foreach (var file in files)
                    {
                        await file.DeleteAsync(StorageDeleteOption.PermanentDelete);
                    }
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    var message = string.Format("MobilinkPlugin.ClearUserDataInternal: error cleaning all user data; error: {0}", ex.Message);
                    _curUser.Logger.Log(message, LogLevel.ERROR);
                    _curUser.Logger.Log(ex.StackTrace, LogLevel.ERROR);
                    result.ErrorMessage = message;
                }
                return result;
            });
        }

        #endregion

        #region tools
        /// <summary>
        /// logs the exception and set Success and ErrorMessage property in <paramref name="result"/>
        /// </summary>
        /// <param name="result"></param>
        /// <param name="ex"></param>
        /// <param name="message"></param>
        private static void HandleException(MobilinkPluginResult result, Exception ex, string message)
        {
            result.Success = false;
            result.ErrorMessage = message;
            _logger.Log(message, LogLevel.ERROR);
            _logger.Log(ex.StackTrace, LogLevel.VERBOSE);
        }

        private static string GetId()
        {
            string result = null;

#if DEBUG
            return _debugGuid;
#endif

            //stored in AppData\Local\Packages\com.gea.salesplus.odb_*\Settings\settings.dat
            var localSettings = ApplicationData.Current.LocalSettings;
            result = (string)localSettings.Values["guid"];
            if (string.IsNullOrEmpty(result))
            {
                byte[] gb = Guid.NewGuid().ToByteArray();
                result = BitConverter.ToInt64(gb, 0).ToString();
                localSettings.Values["guid"] = result;
            }
            return result;
        }

        private static bool TryGetUser(string name, ConcurrentBag<MlUser> users, out MlUser user)
        {
            var result = false;
            user = null;

            foreach (var usr in _allUsers)
            {
                if (usr.Name == name)
                {
                    user = usr;
                    result = true;
                    break;
                }
            }
            return result;
        }

        private static async Task<MlUser> CreateUser(string name, string pwd, StorageFolder rootUserFolder, MlConnectionInfo conInfo)
        {
            var userFolder = await rootUserFolder.CreateFolderAsync(name, CreationCollisionOption.OpenIfExists);
            var attachmentFolder = await userFolder.CreateFolderAsync("attachments", CreationCollisionOption.OpenIfExists);
            var user = MlUser.CreateUser(name, pwd, userFolder, attachmentFolder, _conInfo);
            return user;
        }

        private static void SaveUserCredentials(string name, string pw)
        {
            //stored in AppData\Local\Packages\com.gea.salesplus.odb_*\Settings\settings.dat
            var localSettings = ApplicationData.Current.LocalSettings;
            localSettings.Values[name + "_name"] = name;
            localSettings.Values[name + "_pw"] = pw;
            localSettings.Values["SAMPwd" + name] = pw;
        }

        public MobilinkPluginResult setKeyValue(string key, string val)
        {
            return MobilinkPlugin.sSetKeyValue(key, val);

        }
        public static MobilinkPluginResult sSetKeyValue(string key, string val)
        {
            var result = new MobilinkPluginResult();
            var localSettings = ApplicationData.Current.LocalSettings;
            localSettings.Values[key] = val;
            result.Value = true;
            result.Success = true;
            return result;
        }

        public MobilinkPluginResult removeKey(string key)
        {
            var result = new MobilinkPluginResult();
            var localSettings = ApplicationData.Current.LocalSettings;
            result.Value = localSettings.Values.Remove(key);
            result.Success = true;
            return result;
        }

        private static bool DoUserCredentialsMatch(string name, string pw)
        {
            var localSettings = ApplicationData.Current.LocalSettings;
            var userName = (string)localSettings.Values[name + "_name"];
            var userPw = (string)localSettings.Values[name + "_pw"];

            if (userName == null || userPw == null)
            {
                throw new Exception(string.Format("User: {0} does not exist", name));
            }

            if (name.Equals(userName, StringComparison.Ordinal) && pw.Equals(userPw, StringComparison.Ordinal))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// returns the credentials for a given user;
        /// if the credentials could be retrieved returns true, false otherwise
        /// </summary>
        /// <param name="name"></param>
        /// <param name="userName"></param>
        /// <param name="userPw"></param>
        /// <returns>true if the credentials could be retrieved, false otherwise</returns>
        private static bool GetCredentialsForUser(string name, out string userName, out string userPw)
        {
            var localSettings = ApplicationData.Current.LocalSettings;
            userName = (string)localSettings.Values[name + "_name"];
            userPw = (string)localSettings.Values[name + "_pw"];

            if (userName == null || userPw == null)
            {
                return false;
            }
            return true;
        }

        private static async Task<ConcurrentBag<MlUser>> GetAllUsers(StorageFolder rootUserFolder, MlConnectionInfo conInfo)
        {
            var userFolders = await rootUserFolder.GetFoldersAsync();
            var users = new ConcurrentBag<MlUser>();

            foreach (var folder in userFolders)
            {
                string userName = null;
                string userPw = null;
                if (GetCredentialsForUser(folder.Name, out userName, out userPw))
                {
                    var user = await CreateUser(userName, userPw, rootUserFolder, conInfo);
                    users.Add(user);
                }
            }
            return users;
        }

        private static string[] GetStatements(MlSqlStatement[] sqlStatements)
        {
            try
            {
                var result = new string[sqlStatements.Length];
                for (var i = 0; i < sqlStatements.Length; i++)
                {
                    result[i] = sqlStatements[i].SqlStatement;
                }
                return result;
            }
            catch (Exception ex)
            {
                var msg = string.Format("GetStatements error: {0}", ex.Message);
                _curUser.Logger.Log(msg, LogLevel.ERROR);
                _curUser.Logger.Log(ex.StackTrace, LogLevel.ERROR);
                throw new Exception(msg, ex);
            }
        }

        private static JsonObject CreateQueryResultStructure(MlSqlStatement[] sqlStatements, JsonArray queryResults)
        {
            var result = new JsonObject();
            try
            {
                for (var i = 0; i < sqlStatements.Length; i++)
                {
                    result.Add(sqlStatements[i].TableName, queryResults[i]);
                }
                return result;
            }
            catch (Exception ex)
            {
                var msg = string.Format("CreateQueryResultStructure error: {0}", ex.Message);
                _curUser.Logger.Log(msg, LogLevel.ERROR);
                _curUser.Logger.Log(ex.StackTrace, LogLevel.ERROR);
                throw new Exception(msg, ex);
            }
        }

        private void SetLogLevelFromDb()
        {
            try
            {
                //var sql = "SELECT appl_log_level, transac_log_on FROM SAMUsers WHERE UPPER(user_name)=UPPER('" + _curUser.Name + "')"; //DPenchev
                var sql = "SELECT appl_log_level, transac_log_on FROM SAM_USERS WHERE UPPER(user_name)=UPPER('" + _curUser.Name + "')"; //DPenchev
                var queryResult = _curUser.UlWrapper.ExecuteQueryAsDictionary(sql);
                if (queryResult.Count == 1)
                {
                    //var logLvlAsString = queryResult[0]["appl_log_level"].ToString();//DPenchev
                    var logLvlAsString = queryResult[0]["APPL_LOG_LEVEL"].ToString();//DPenchev
                    //var transactLogLvlAsString = queryResult[0]["transac_log_on"].ToString();//DPenchev
                    var transactLogLvlAsString = queryResult[0]["TRANSAC_LOG_ON"].ToString();//DPenchev

                    var logLvl = LogLevelExtension.ParseFromString(logLvlAsString);
                    _curUser.Logger.SetLogLevel(logLvl);

                    //if (transactLogLvlAsString == "1")
                    {
                        _curUser.TransactionLogger.SetLogLevel(LogLevel.DO_NOT_LOG);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("SetLogLevelFromDb: could not set log level: {0}", ex.Message));
            }
        }

        private async Task UpdateSyncedFiles()
        {
            var success = true;
            var updateQueries = new JsonArray();

            //create query for updating attachments
            var updateQuery = CreateAttachmentUpdateQueryStructure();
            var whereConditions = "";

            var localFolder = ApplicationData.Current.LocalFolder;
            var sql = "SELECT a.mobile_id, b.LINKID, b.FILENAME, b.FILEEXT FROM SAMObjectAttachments AS a INNER JOIN SAMObjectAttachments as b WHERE a.ACTION_FLAG='A' AND a.NEW_MOBILE_ID=b.mobile_id";
            var attachmentList = _curUser.UlWrapper.ExecuteQueryAsDictionary(sql);
            foreach (var att in attachmentList)
            {
                //search if the file already exists
                var linkId = att["LINKID"].ToString();
                var fileName = att["FILENAME"].ToString();
                var fileExt = att["FILEEXT"].ToString();
                var mobileId = att["mobile_id"].ToString();
                var oldPath = fileName + "." + fileExt;
                var newPath = linkId + "." + oldPath;

                var oldFile = await localFolder.GetItemAsync(oldPath);
                if (oldFile != null)
                {
                    try
                    {
                        await oldFile.RenameAsync(newPath, NameCollisionOption.FailIfExists);
                        whereConditions = "mobile_id='" + mobileId + "'";
                        updateQuery.SetNamedValue("whereConditions", JsonValue.CreateStringValue(whereConditions));
                        updateQueries.Add(updateQuery);
                    }
                    catch (Exception ex)
                    {
                        var message = string.Format("UpdateSyncedFiles: could not rename '{0}' to '{1}': {2}", oldPath, newPath, ex.Message);
                        _curUser.Logger.Log(message, LogLevel.ERROR);
                        _curUser.Logger.Log(ex.StackTrace, LogLevel.VERBOSE);
                        success = false;
                    }
                }
            }

            if (updateQueries.Count > 0)
            {
                _curUser.UlWrapper.ExecuteStatementsAndCommit(updateQueries);
            }
            if (!success)
            {
                throw new Exception("UpdateSyncedFiles: could not rename one or more files.");
            }
        }

        private static JsonObject CreateAttachmentUpdateQueryStructure()
        {
            var updateQuery = new JsonObject();
            updateQuery.SetNamedValue("type", JsonValue.CreateStringValue("update"));
            updateQuery.SetNamedValue("mainTable", JsonValue.CreateBooleanValue(false));
            updateQuery.SetNamedValue("tableName", JsonValue.CreateStringValue("SAMObjectAttachments"));
            var values = new JsonArray();
            values.Add(JsonValue.CreateStringValue("ACTION_FLAG"));
            values.Add(JsonValue.CreateStringValue("B"));
            var dummyArray = new JsonArray();
            dummyArray.Add(values);
            updateQuery.SetNamedValue("values", dummyArray);
            updateQuery.SetNamedValue("replacementValues", new JsonArray());
            updateQuery.SetNamedValue("mainObjectValues", new JsonArray());
            return updateQuery;
        }

        private static MobilinkPluginResult IsReadyForFilesDownload()
        {
            var result = new MobilinkPluginResult();

            try
            {
                InternetConnectionType connectionType;
                var hasInternetConnection = ConnectionInfo.HasCableOrWifiInternetConnection(_logger, out connectionType);
                //no wifi or cable connection
                if (connectionType != InternetConnectionType.Cable && connectionType != InternetConnectionType.WiFi)
                {
                    result.Success = false;
                    result.ErrorMessage = "error_synch_file_no_wifi";
                    var message = string.Format("MobilinkPlugin.IsReadyForFilesDownload: Invalid internet connection.");
                    _curUser.Logger.Log(message, LogLevel.ERROR);
                }
                //wifi or cable connected but no internet access
                if (!hasInternetConnection && (connectionType == InternetConnectionType.Cable || connectionType == InternetConnectionType.WiFi))
                {
                    result.Success = false;
                    result.ErrorMessage = "error_synch_network";
                    var message = string.Format("MobilinkPlugin.IsReadyForFilesDownload: No internet connection.");
                    _curUser.Logger.Log(message, LogLevel.ERROR);
                }
                //wifi or cable connected and internet access
                if (hasInternetConnection && (connectionType == InternetConnectionType.Cable || connectionType == InternetConnectionType.WiFi))
                {
                    result.Success = true;
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                var message = string.Format("MobilinkPlugin.IsReadyForFilesDownload: error: ", ex.Message);
                _curUser.Logger.Log(message, LogLevel.ERROR);
            }
            return result;
        }
        #endregion
    }
}

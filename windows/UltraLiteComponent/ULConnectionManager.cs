﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UltraLite;

namespace UltraLiteComponent
{
    class ULConnectionInfo
    {
        private volatile bool _inUse = false;
        private object _criticalSectionLock = new object();

        internal ULConnectionInfo(Connection conn)
        {
            Connection = conn;
        }

        internal Connection Connection { get; private set; }

        internal bool TryGetConnection(out Connection conn)
        {
            lock (_criticalSectionLock)
            {
                conn = null;
                if (_inUse)
                {
                    return false;
                }
                else
                {
                    conn = Connection;
                    _inUse = true;
                    return true;
                }
            }
        }

        internal void ReleaseConnection(Connection conn)
        {
            lock (_criticalSectionLock)
            {
                if (_inUse)
                {
                    if (conn == Connection)
                    {
                        _inUse = false;
                    }
                    else
                    {
                        throw new Exception("trying to release a wrong connection");
                    }
                }
                else
                {
                    throw new InvalidOperationException("cannot release connection which is not in use");
                }
            }
        }
    }

    class ULConnectionManager : IDisposable
    {
        private string _connParams = null;
        private volatile ULConnectionInfo[] _connections = null;
        private SemaphoreSlim _conSemaphore = null;
        private object _lock = new object();

        private volatile bool _connectionsLocked = false;
        private object _connectionsLock = new object();

        internal ULConnectionManager(string connParams)
        {
            _connParams = connParams;
            _connections = GetConnections(_connParams);
            _conSemaphore = new SemaphoreSlim(_connections.Length, _connections.Length);
        }

        #region internal methods
        internal Connection GetConnection()
        {
            _conSemaphore.Wait();
            Connection conn = null;

            foreach (var ci in _connections)
            {
                if (ci.TryGetConnection(out conn))
                {
                    Debug.WriteLine("aquired connection. connections available: " + _conSemaphore.CurrentCount);
                    return conn;
                }
            }
            throw new InvalidOperationException("Could not get a connection: all connections are in use");

        }

        /// <summary>
        /// if conn != null releases the connection, so that the connection is available again for other requests
        /// </summary>
        /// <param name="conn"></param>
        internal void ReleaseConnection(Connection conn)
        {
            if (conn == null)
            {
                return;
            }

            foreach (var ci in _connections)
            {
                if (ci.Connection == conn)
                {
                    ci.ReleaseConnection(conn);
                    _conSemaphore.Release();
                    Debug.WriteLine("released connection. connections available: " + _conSemaphore.CurrentCount);
                    return;
                }
            }
            throw new Exception("Could not release connection as provided connection is unknown");

        }

        internal void ReleaseConnections()
        {
            lock (_connectionsLock)
            {
                if (_connectionsLocked)
                {
                    foreach (var conn in _connections)
                    {
                        conn.ReleaseConnection(conn.Connection);
                    }
                    _connectionsLocked = false;
                }
            }
        }

        /// <summary>
        /// aquires all connections; waits til they are awailable -> blocking
        /// </summary>
        /// <returns></returns>
        internal void LockAllConnections()
        {
            lock (_connectionsLock)
            {
                if (!_connectionsLocked)
                {
                    var locks = _connections.Length;
                    //aquire all locks on the semaphore
                    for (var i = 0; i < locks; i++)
                    {
                        _conSemaphore.Wait();
                    }
                    _connectionsLocked = true;
                }
            }
        }



        /// <summary>
        /// closes all connections held by the manager;
        /// after this the manager is not usable any more.
        /// create a new manager object for accessing the DB
        /// </summary>
        /// <param name="hard">true: closes the connections immediately; false: waits for the connections to be awailable before closing them</param>
        internal void Close(bool hard)
        {
            if (!Disposed)
            {
                if (hard)
                {
                    DisposeInternal();
                }
                else
                {
                    var locks = _connections.Length;
                    //aquire all locks on the semaphore
                    for (var i = 0; i < locks; i++)
                    {
                        _conSemaphore.Wait();
                    }
                    //then close the connections
                    foreach (var conn in _connections)
                    {
                        conn.Connection.CloseObject();
                    }
                    _connections = null;
                    Disposed = true;
                    _conSemaphore.Release(locks);
                    _conSemaphore = null;
                }
            }
        }
        #endregion

        #region dispose
        internal bool Disposed { get; private set; }

        public void Dispose()
        {
            DisposeInternal();
            GC.SuppressFinalize(this);
        }

        ~ULConnectionManager()
        {
            DisposeInternal();
        }

        /// <summary>
        /// hard (immediately) closes all DB connections
        /// </summary>
        private void DisposeInternal()
        {
            if (!Disposed)
            {
                if (_connections != null)
                {
                    foreach (var conn in _connections)
                    {
                        try
                        {
                            conn.Connection.CloseObject();
                        }
                        catch { }
                    }
                }

                Disposed = true;
                _conSemaphore = null;
            }
        }
        #endregion

        #region private functions
        private ULConnectionInfo[] GetConnections(string connParams)
        {
            List<ULConnectionInfo> conns = new List<ULConnectionInfo>();
            try
            {
                while (true)
                //for(var i = 0; i < 4; i++)
                {
                    var conn = DatabaseManager.OpenConnection(connParams);
                    conns.Add(new ULConnectionInfo(conn));
                }
            }
            catch { }

            if (conns.Count == 0)
            {
                throw new Exception("Could not aquire any DB connection for: " + connParams);
            }
            return conns.ToArray<ULConnectionInfo>();
        }

        #endregion
    }
}
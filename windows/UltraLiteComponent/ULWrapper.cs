﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Windows.Data.Json;
using Windows.Foundation;
using Windows.Storage;
using UltraLite;
using LoggerLib;
using Windows.System.Profile;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation.Collections;
using System.IO;
using Windows.Foundation.Metadata;
using UtilComponent;

namespace UltraLiteComponent
{
    #region helper classes
    public enum ULUpdateType
    {
        Update,
        Insert,
        Delete
    }

    public enum ULTransferType
    {
        Upload,
        Download,
    }

    public sealed class MlConnectionInfo
    {
        public string Protocol { get; set; }
        public string HostName { get; set; }
        public int Port { get; set; }
        public string Publication { get; set; }
        public string Version { get; set; }
        public string Subscription { get; set; }
        public string UrlSuffix { get; set; }

        /// <summary>
        /// individual for each user
        /// </summary>
        public string DbFilePath { get; set; }
        public string DbFileName { get; set; }
    }

    public sealed class ULFileTransfer
    {
        public ULFileTransfer(ULTransferType type, string fileId,
            string fileName, IStorageFolder fileFolder)
        {
            FileId = fileId;
            FileName = fileName;
            FileFolder = fileFolder;
            CancelRequested = false;
        }
        public ULTransferType Type { get; private set; }
        public string FileId { get; private set; }
        public string FileName { get; private set; }
        public IStorageFolder FileFolder { get; private set; }
        public FileTransferStatus TransferStatus { get; set; }
        public bool CancelRequested { get; set; }
    }

    /// <summary>
    /// result structure for transferring files to/from mobilink server
    /// </summary>
    public sealed class ULFileTransferResult
    {
        public ULFileTransferResult()
        {
            Success = false;
            FileChanged = false;
        }

        public ULFileTransferResult(bool transferSuccess, bool fileChanged)
        {
            Success = transferSuccess;
            FileChanged = fileChanged;
        }


        public bool Success { get; set; }
        public bool FileChanged { get; set; }
        public string ErrorMessage { get; set; }
    }
    #endregion

    //sql anywhere 17 error messages by sql error code:
    //http://dcx.sap.com/index.html#sqla170/en/html/81133cb16ce210148ebed01b8c288d75.html

    //MobiLink server error messages sorted by error code:
    //http://dcx.sap.com/index.html#sqla170/en/html/80f5fb086ce21014ba1bec2569d4de5c.html

    //MobiLink communication error messages sorted by error code:
    //http://dcx.sap.com/index.html#sqla170/en/html/80f5d2ed6ce210149fe6964b2cbd8a16.html

    public sealed class ULWrapper : IDisposable
    {
        private static object _initLock = new object();
        private static volatile bool _initialized = false;

        private ULConnectionManager _connManager = null;
        /// <summary>
        /// controls access to getting a connection from the ULConnectionManager
        /// </summary>
        private ManualResetEventSlim _dbAccessMre = new ManualResetEventSlim(false);
        private MlConnectionInfo _conInfo = null;
        private string _connParams = null;

        private Logger _logger = null;
        private Logger _transactionLogger = null;

        private static object _ongoingFileTransfersLock = new object();
        private static List<ULFileTransfer> _ongoingFileTransfers = new List<ULFileTransfer>();

        private static object _ongoingSyncLock = new object();
        private static bool _isSyncInProgress = false;
        private static bool _syncCancelRequest = false;

        public string NewObjectUuid { get; private set; }

        public ULWrapper(Logger logger, Logger transactionLogger, StorageFolder filesFolder, MlConnectionInfo conInfo, string encryptionKey)
        {
            _logger = logger;
            _transactionLogger = transactionLogger;
            if (conInfo != null)
            {
                SetConnectionParams(conInfo, encryptionKey);
            }

            lock (_initLock)
            {
                if (!_initialized)
                {
                    if (!DatabaseManager.Init())
                    {
                        var message = "UltraLite DatabaseManager initialize failed.";
                        _logger.Log(message, LogLevel.ERROR);
                        throw new Exception(message);
                    }
                    _initialized = true;
                }
            }

        }

        public bool isSyncInProgress()
        {
            return _isSyncInProgress == true;
        }

        #region initialization

        public void StopDbAccess()
        {
            lock (_initLock)
            {
                _dbAccessMre.Reset();
            }
        }

        public void AllowDbAccess()
        {
            lock (_initLock)
            {
                _dbAccessMre.Set();
            }
        }

        public void SetConnectionParams(MlConnectionInfo conInfo, string encryptionKey)
        {
            lock (_initLock)
            {
                _conInfo = conInfo;
                /*//DPenchev
                var localSettings = ApplicationData.Current.LocalSettings;
                List<String> listStrLineElements = conInfo.DbFilePath.Split('\\').ToList();
                for (int i = 0; i < listStrLineElements.Count; i++)
                {
                    if (listStrLineElements[i].Equals(conInfo.DbFileName, StringComparison.Ordinal))
                    {
                        var name = listStrLineElements[i - 1];
                        var userName = (string)localSettings.Values[name + "_name"];
                        var userPw = (string)localSettings.Values[name + "_pw"];

                        if (encryptionKey != "")
                        {
                            _connParams = string.Format("DBF={0};UID={1};PWD={2};;DBKEY={3}", conInfo.DbFilePath, userName, userPw, encryptionKey);
                        }
                        else
                        {
                            _connParams = string.Format("dbf={0};uid={1};pwd={2};", conInfo.DbFilePath, userName, userPw);
                        }
                        return;
                    }
                }
                //DPenchev*/
                //_connParams = string.Format("dbf={0};DBKEY={1}", _dbFilePath, "");
                //DPenchev
                _connParams = string.Format("dbf={0};uid={1};pwd={1};", conInfo.DbFilePath, "''");
                return;
                //DPenchev
                if (encryptionKey != "")
                {
                    _connParams = string.Format("dbf={0};uid={1};pwd={1};;DBKEY={2}", conInfo.DbFilePath, "''", encryptionKey);
                }
                else
                {
                    _connParams = string.Format("dbf={0};uid={1};pwd={1};", conInfo.DbFilePath, "''");
                }

            }
        }

        public void CreateDbConnections()
        {
            lock (_initLock)
            {
                if (_connManager != null)
                {
                    throw new Exception("Cannot create new DB connections. Close existing ones first.");
                }
                _connManager = new ULConnectionManager(_connParams);
            }
        }

        public void CloseDbConnections(bool hardClose)
        {
            lock (_initLock)
            {
                if (_connManager != null)
                {
                    _connManager.Close(hardClose);
                    _connManager = null;
                }
            }
        }

        #endregion

        #region queries
        public string ExecuteQuery(string sqlStatement)
        {
            Connection connection = null;
            PreparedStatement ps = null;
            ResultSet rs = null;

            try
            {
                _logger.Log(string.Format("ULWrapper.ExecuteQuery: {0}", sqlStatement), LogLevel.VERBOSE);

                connection = GetConnection();
                ps = connection.PrepareStatement(sqlStatement);
                if (ps == null)
                {
                    throw new Exception("Failed preparing Statement: " + sqlStatement);
                }

                rs = ps.ExecuteQuery();
                var result = ConvertResultSetToJson(rs);
                return result;
            }
            catch (Exception ex)
            {
                var defaultLogMsg = string.Format("ExecuteQuery: '{0}' failed", sqlStatement);
                var exMessage = HandleException(connection, ex, defaultLogMsg);
                throw new Exception(exMessage, ex);
            }
            finally
            {
                if (rs != null)
                {
                    rs.CloseObject();
                }
                if (ps != null)
                {
                    ps.CloseObject();
                }
                ReleaseConnection(connection);
            }
        }

        public string ExecuteQueriesSequentially([ReadOnlyArray()] string[] sqlStatements)
        {
            var results = ExecuteQueriesSequentiallyInternal(sqlStatements);
            return results.Stringify();
        }

        public JsonArray ExecuteQueriesSequentiallyAsJsonArray([ReadOnlyArray()] string[] sqlStatements)
        {
            return ExecuteQueriesSequentiallyInternal(sqlStatements);
        }

        private JsonArray ExecuteQueriesSequentiallyInternal([ReadOnlyArray()] string[] sqlStatements)
        {
            Connection connection = null;
            PreparedStatement ps = null;
            ResultSet rs = null;
            var i = 0;
            var jsonArr = new JsonArray();

            try
            {
                _logger.Log(string.Format("ULWrapper.ExecuteQueriesSequentiallyInternal: #queries: {0}", sqlStatements.Length), LogLevel.VERBOSE);

                connection = GetConnection();
                for (; i < sqlStatements.Length; i++)
                {
                    var sql = sqlStatements[i];
                    _logger.Log(string.Format("Query: {0}: {1}", i, sql), LogLevel.VERBOSE);

                    //don't use the using pattern on ps and rs as it has side effects on the connection,
                    //causing connection.CloseObject() to throw an exception
                    ps = connection.PrepareStatement(sql);
                    if (ps == null)
                    {
                        throw new Exception("Failed preparing Statement: " + i);
                    }
                    rs = ps.ExecuteQuery();
                    var rsAsJsonArr = ConvertResultSetToJsonInternal(rs);
                    jsonArr.Add(rsAsJsonArr);

                    rs.CloseObject();
                    rs = null;
                    ps.CloseObject();
                    ps = null;
                }
                return jsonArr;
            }
            catch (Exception ex)
            {
                var defaultLogMsg = string.Format("ULWrapper.ExecuteQueriesSequentiallyInternal: query number {0} failed; query: '{1}';", i, sqlStatements[i]);
                var exMessage = HandleException(connection, ex, defaultLogMsg);
                throw new Exception(exMessage, ex);
            }
            finally
            {
                if (rs != null)
                {
                    rs.CloseObject();
                }
                if (ps != null)
                {
                    ps.CloseObject();
                }
                ReleaseConnection(connection);
            }
        }

        public IList<IReadOnlyDictionary<string, object>> ExecuteQueryAsDictionary(string sqlStatement)
        {
            Connection connection = null;
            PreparedStatement ps = null;
            ResultSet rs = null;

            try
            {
                connection = GetConnection();
                ps = connection.PrepareStatement(sqlStatement);
                if (ps == null)
                {
                    throw new Exception("Failed preparing Statement: " + sqlStatement);
                }

                rs = ps.ExecuteQuery();
                var result = ConvertResultSetToDictionary(rs);
                return result;
            }
            catch (Exception ex)
            {
                var defaultLogMsg = string.Format("ExecuteQuery: '{0}' failed", sqlStatement);
                var exMessage = HandleException(connection, ex, defaultLogMsg);
                throw new Exception(exMessage, ex);
            }
            finally
            {
                if (rs != null)
                {
                    rs.CloseObject();
                }
                if (ps != null)
                {
                    ps.CloseObject();
                }
                ReleaseConnection(connection);
            }
        }

        public string ExecuteUpdate(string sqlStatement)
        {
            Connection connection = null;
            PreparedStatement ps = null;

            try
            {
                _logger.Log(string.Format("ULWrapper.ExecuteUpdate: {0}", sqlStatement), LogLevel.VERBOSE);
                _transactionLogger.Log(string.Format("ULWrapper.ExecuteUpdate: {0}", sqlStatement), LogLevel.VERBOSE);

                //_dbConnSemaphore.Wait();
                connection = GetConnection();
                ps = connection.PrepareStatement(sqlStatement);

                if (ps == null)
                {
                    throw new Exception("Failed preparing Statement: " + sqlStatement);
                }

                ps.ExecuteStatement();
                connection.Commit();
                return "Update executed.";
            }
            catch (Exception ex)
            {
                var defaultLogMsg = string.Format("ExecuteUpdate: '{0}' failed", sqlStatement);
                var exMessage = HandleException(connection, ex, defaultLogMsg);
                throw new Exception(exMessage, ex);
            }
            finally
            {
                if (ps != null)
                {
                    ps.CloseObject();
                }
                ReleaseConnection(connection);
            }
        }

        public void ExecuteUpdatesSequentially(JsonArray sqlStatements)
        {
            Connection connection = null;
            PreparedStatement ps = null;

            try
            {
                connection = GetConnection();

                foreach (var sqlStatement in sqlStatements)
                {
                    _logger.Log(string.Format("ULWrapper.ExecuteUpdatesSequentially: {0}", sqlStatement.GetString()), LogLevel.VERBOSE);
                    _transactionLogger.Log(string.Format("ULWrapper.ExecuteUpdatesSequentially: {0}", sqlStatement.GetString()), LogLevel.VERBOSE);

                    ps = connection.PrepareStatement(sqlStatement.GetString());

                    if (ps == null)
                    {
                        throw new Exception("Failed preparing Statement: " + sqlStatement.GetString());
                    }

                    ps.ExecuteStatement();
                    ps.CloseObject();
                    ps = null;
                }

                connection.Commit();
            }
            catch (Exception ex)
            {
                if (connection != null)
                {
                    connection.Rollback();
                }

                var defaultLogMsg = string.Format("ExecuteUpdatesSequentially: '{0}' failed", ex.StackTrace);
                var exMessage = HandleException(connection, ex, defaultLogMsg);
                throw new Exception(exMessage, ex);
            }
            finally
            {
                if (ps != null)
                {
                    ps.CloseObject();
                }

                ReleaseConnection(connection);
            }
        }

        public void ExecuteStatementsAndCommit(JsonArray sqlStatements)
        {
            Connection connection = null;
            string mainObjectUUID = null;
            string mainObjectTable = null;
            NewObjectUuid = "";
            try
            {
                //TODO: log statement in a readable way
                _logger.Log(string.Format("ULWrapper.ExecuteStatementsAndCommit: {0}", sqlStatements), LogLevel.VERBOSE);
                _transactionLogger.Log(string.Format("ULWrapper.ExecuteStatementsAndCommit: {0}", sqlStatements), LogLevel.VERBOSE);

                //_dbConnSemaphore.Wait();
                connection = GetConnection();
                foreach (var statement in sqlStatements)
                {
                    var jObj = statement.GetObject();
                    var type = jObj.GetNamedValue("type").GetString();
                    var queryType = (ULUpdateType)Enum.Parse(typeof(ULUpdateType), type, ignoreCase: true);
                    var isMainTable = jObj.GetNamedValue("mainTable").GetBoolean();
                    var tableName = jObj.GetNamedValue("tableName").GetString();
                    var whereConditions = jObj.GetNamedValue("whereConditions").GetString();
                    var values = jObj.GetNamedValue("values").GetArray();
                    var replacementValues = jObj.GetNamedValue("replacementValues").GetArray();
                    var mainObjectValues = jObj.GetNamedValue("mainObjectValues").GetArray();

                    ExecuteUpdateInsert(connection, queryType, isMainTable, tableName,
                        whereConditions, values, replacementValues, mainObjectValues,
                        mainObjectUUID, mainObjectTable, out mainObjectUUID, out mainObjectTable);
                }

                connection.Commit();
            }
            catch (Exception ex)
            {
                var defaultLogMsg = string.Format("ExecuteStatementsAndCommit: failed");
                var exMessage = HandleException(connection, ex, defaultLogMsg);
                throw new Exception(exMessage, ex);
            }
            finally
            {
                ReleaseConnection(connection);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="conn"></param>
        /// <param name="type"></param>
        /// <param name="tableName"></param>
        /// <param name="whereConditions"></param>
        /// <param name="values"></param>           [[key,value],[key,value]...]
        /// <param name="replacementValues"></param>[[key,value],[key,value]...]
        /// <param name="mainObjectValues"></param> [mainObjectTable,mainObjectUUID]
        /// <returns></returns>
        public void ExecuteUpdateInsert(Connection conn, ULUpdateType type, bool isMainTable,
            string tableName, string whereConditions, JsonArray values, JsonArray replacementValues,
            JsonArray mainObjectValues, string mainObjectUUID, string mainObjectTable,
            out string newMainObjectUUID, out string newMainObjectTable)
        {
            PreparedStatement ps = null;
            string sql = null;
            string uuid = null;

            try
            {
                switch (type)
                {
                    //delete
                    case ULUpdateType.Delete:
                        sql = string.Format("DELETE FROM {0} WHERE {1}", tableName, whereConditions);
                        break;

                    //update
                    case ULUpdateType.Update:
                        sql = string.Format("UPDATE {0} SET ", tableName);
                        foreach (var jv in values)
                        {
                            sql += jv.GetArray().GetStringAt(0) + "=?, ";
                        }
                        //remove last ','
                        sql = sql.Remove(sql.Length - 2);
                        sql += " WHERE " + whereConditions;
                        break;

                    //insert
                    case ULUpdateType.Insert:
                        //get new UUID
                        Dictionary<string, object> uuidResult = (Dictionary<string, object>)ExecuteQueryAsDictionary("SELECT NEWID() AS new_uuid")[0];
                        uuid = uuidResult["new_uuid"].ToString();
                        sql = string.Format("INSERT INTO {0} (mobile_id, ", tableName);

                        var valuesString = "";
                        foreach (var jv in replacementValues)
                        {
                            sql += jv.GetArray().GetStringAt(0) + ", ";
                            valuesString += "?, ";
                        }
                        foreach (var jv in values)
                        {
                            sql += jv.GetArray().GetStringAt(0) + ", ";
                            valuesString += "?, ";
                        }

                        //remove last ','
                        sql = sql.Remove(sql.Length - 2);
                        valuesString = valuesString.Remove(valuesString.Length - 2);
                        sql += ") Values('" + uuid + "', ";
                        sql += valuesString + ")";
                        if (isMainTable)
                        {
                            mainObjectUUID = uuid;
                            mainObjectTable = tableName;
                        }
                        break;
                }

                ps = conn.PrepareStatement(sql);
                uint i = 0; uint j = 0;
                for (i = 0; i < replacementValues.Count; i++)
                {
                    var rv = replacementValues.GetArrayAt(i);
                    string value = rv.GetStringAt(1);
                    if (value.Equals("mobile_id") || value.Equals("MOBILE_ID")) //Bitbucket
                    {
                        value = uuid.Substring(0, 18);
                    }
                    var insertPosition = i;
                    if (insertPosition > ushort.MaxValue)
                    {
                        throw new InvalidCastException("insertPosition: " + insertPosition + " must be lower than " + ushort.MaxValue);
                    }
                    SetStringParameterInPs(ps, (ushort)insertPosition, value);
                }

                for (j = 0; j < values.Count; j++)
                {
                    string value = values.GetArrayAt(j).GetStringAt(1);
                    var insertPosition = i + j;
                    if (insertPosition > ushort.MaxValue)
                    {
                        throw new InvalidCastException("insertPosition: " + insertPosition + " must be lower than " + ushort.MaxValue);
                    }
                    SetStringParameterInPs(ps, (ushort)insertPosition, value);
                }
                try
                {
                    ps.ExecuteStatement();
                }
                catch (Exception ex)
                {
                    if (conn != null && ex.HResult == (int)ErrorCodes.E_ULTRALITE_ERROR && !isMainTable)
                    {
                        string errorParams = "";
                        int errorCode = 0;
                        conn.GetLastError(out errorCode, out errorParams);
                        if (errorCode == -193)
                        {
                            ExecuteUpdateForFailedInsert(conn, tableName, values);
                        }
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                var message = string.Format("ULWrapper.ExecuteUpdateInsert failed : {0}", ex.Message);
                _logger.Log(message, LogLevel.ERROR);
                _logger.Log(ex.StackTrace, LogLevel.VERBOSE);

                throw new Exception(message);
            }
            finally
            {
                if (ps != null)
                {
                    ps.CloseObject();
                }
                newMainObjectUUID = mainObjectUUID;
                newMainObjectTable = mainObjectTable;
                NewObjectUuid = uuid;
            }
        }

        private void ExecuteUpdateForFailedInsert(Connection conn, string tableName, JsonArray values)
        {
            PreparedStatement ps = null;
            var whereClause = "";
            //var keyColumnsSqlString = string.Format(@"SELECT SY.column_name,ST.object_id AS TABLE_ID, SY.object_id as COL_ID,SI.index_name FROM syscolumn AS SY INNER JOIN systable AS ST ON SY.table_id = ST.object_id INNER JOIN sysixcol AS SX ON ST.object_id = SX.table_id AND SX.column_id = SY.object_id INNER JOIN sysindex AS SI ON SI.table_id = ST.object_id AND SX.index_id = SI.object_id WHERE SI.type = 'key' AND SI.index_name LIKE '{0}' AND ST.table_name = '{1}'", @"%_sapkey", tableName);
            var keyColumnsSqlString = string.Format(@"SELECT SY.column_name as column_name ,ST.object_id AS TABLE_ID, SY.object_id as COL_ID,SI.index_name as index_name FROM syscolumn AS SY INNER JOIN systable AS ST ON SY.table_id = ST.object_id INNER JOIN sysixcol AS SX ON ST.object_id = SX.table_id AND SX.column_id = SY.object_id INNER JOIN sysindex AS SI ON SI.table_id = ST.object_id AND SX.index_id = SI.object_id WHERE SI.type = 'key' AND SI.index_name LIKE '{0}' AND ST.table_name = '{1}'", @"%_sapkey", tableName); //Bitbucket
            var keyColumnsResult = ExecuteQueryAsDictionary(keyColumnsSqlString);

            //var sql = string.Format("UPDATE {0} SET ", tableName);
            for (uint i = 0; i < keyColumnsResult.Count; i++)
            {
                whereClause += keyColumnsResult[(int)i]["column_name"];
                for (uint j = 0; j < values.Count; j++)
                {
                    if (values.GetArrayAt(j).GetStringAt(0) == keyColumnsResult[(int)i]["column_name"].ToString())
                    {
                        whereClause += string.Format(@"='{0}' AND ", values.GetArrayAt(j).GetStringAt(1));
                        break;
                    }
                }
            }
            //delete last ' AND '
            whereClause = whereClause.Substring(0, whereClause.Length - 5);

            var sql = string.Format("UPDATE {0} SET ", tableName);
            for (uint i = 0; i < values.Count; i++)
            {
                sql += values.GetArrayAt(i).GetStringAt(0) + @"=?, ";
            }
            //remove last ','
            sql = sql.Substring(0, sql.Length - 2);
            sql += " WHERE " + whereClause;

            try
            {
                //prepare statement and add replacement values
                ps = conn.PrepareStatement(sql);
                for (uint i = 0; i < values.Count; i++)
                {
                    var value = values.GetArrayAt(i).GetStringAt(1);
                    if (values.GetArrayAt(i).GetStringAt(0) == "ACTION_FLAG")
                    {
                        value = "C";
                    }
                    var insertPosition = i;
                    if (insertPosition > ushort.MaxValue)
                    {
                        throw new InvalidCastException("insertPosition: " + insertPosition + " must be lower than " + ushort.MaxValue);
                    }
                    SetStringParameterInPs(ps, (ushort)insertPosition, value);
                }
                ps.ExecuteStatement();
            }
            catch (Exception ex)
            {
                var defaultLogMsg = "ExecuteUpdateForFailedInsert: ";
                var exMessage = HandleException(conn, ex, defaultLogMsg);
                throw new Exception(exMessage);
            }
            finally
            {
                if (ps != null)
                {
                    ps.CloseObject();
                }
            }
        }
        #endregion



        /// <summary>
        /// use this if you want to insert strings with more than 32KB length
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="whereConditions"></param>
        /// <param name="values"></param>
        public void ExecuteUpdateInsert(ULUpdateType type, string tableName, string whereConditions, PropertySet values)
        {
            //TODO: log statement in a readable way
            var sql = "";
            Connection connection = null;
            PreparedStatement ps = null;
            var propertySetValues = new string[values.Values.Count];
            values.Values.CopyTo(propertySetValues, 0);

            if (type == ULUpdateType.Update)
            {
                //UPDATE
                sql = string.Format("UPDATE {0} SET ", tableName);
                foreach (var i in values)
                {
                    sql += i.Key + "=?, ";
                }
                //remove last ','
                sql = sql.Remove(sql.Length - 2);
                sql += " " + whereConditions;
            }
            else
            {
                //INSERT
                sql = string.Format("INSERT INTO {0} (", tableName);
                var valuesString = "";
                foreach (var i in values)
                {
                    sql += i.Key + ", ";
                    valuesString += "?, ";
                }
                //remove last ','
                sql = sql.Remove(sql.Length - 2);
                valuesString = valuesString.Remove(valuesString.Length - 2);
                sql += ") Values(" + valuesString + ")";
            }

            try
            {
                //_dbConnSemaphore.Wait();
                connection = GetConnection();
                ps = connection.PrepareStatement(sql);

                for (ushort i = 0; i < values.Count; i++)
                {
                    var value = propertySetValues[i];
                    SetStringParameterInPs(ps, i, value);
                }
                ps.ExecuteStatement();
                connection.Commit();
            }
            catch (Exception ex)
            {
                var defaultLogMsg = string.Format("ExecuteUpdateInsert: failed");
                var exMessage = HandleException(connection, ex, defaultLogMsg);
                throw new Exception(exMessage, ex);
            }
            finally
            {
                if (ps != null)
                {
                    ps.CloseObject();
                }
                ReleaseConnection(connection);
            }
        }

        #region Synchronize DB
        // sync Params: http://dcx.sybase.com/index.html#sa160/en/uladmin/ulcs-utilities-s-5166130.html

        public void SyncDbWithObserver(string user, string password, EventHandler<object> progressHandler, bool pingMode, string syncMode, string syncModel)
        {
            SyncDbInternal(user, password, progressHandler, pingMode, syncMode, syncModel);
        }

        private void SyncDbInternal(string user, string password, EventHandler<object> progressHandler = null, bool pingMode = false, string syncMode = null, string publication = null)
        {
            Connection connection = null;
            SyncObserver syncObserver = null;

            lock (_ongoingSyncLock)
            {
                if (_isSyncInProgress)
                {
                    throw new Exception("Cannot start new DB sync as another sync is still in progress");
                }
                _syncCancelRequest = false;
                _isSyncInProgress = true;
            }
            //Bitbucket
            var syncParams = "";
            syncMode = syncMode != null ? syncMode : "";
            publication = publication != null && publication.Length > 0 ? publication : _conInfo.Publication;

            syncObserver = new SyncObserver((target) =>
            {
                if (progressHandler != null)
                {
                    var status = target;
                    var state = status.State; //see SyncState enum

                    var msg = string.Format("state: {0}; table count: {1}; table index: {2}", state, status.TableCount, status.TableIndex);
                    //_logger.Log(msg, LogLevel.VERBOSE);
                    System.Diagnostics.Debug.WriteLine(msg);

                    var progress = new JsonObject();
                    progress.Add("State", JsonValue.CreateStringValue(converSyncStateToText(state)));
                    progress.Add("TableCount", JsonValue.CreateStringValue(status.TableCount.ToString()));
                    progress.Add("TableIndex", JsonValue.CreateStringValue(status.TableIndex.ToString()));
                    progressHandler(this, progress.Stringify());
                }

                //return true for sync abort, false to continue
                if (_syncCancelRequest)
                {
                    return true;
                }
                return false;
            });

            try
            {
                // Get sync model from sync profile
                var syncModelInfo = ExecuteQueryAsDictionary(string.Format("SELECT SYNC_PROFILE_OPTION_VALUE('{0}', 'ScriptVersion')", publication));
                var syncModel = syncModelInfo[0][""];
                var urlSuffix = _conInfo.UrlSuffix.Length > 0 ? string.Format(";url_suffix={0}", _conInfo.UrlSuffix) : "";

                if (pingMode)
                {
                    syncParams = string.Format("MobiLinkUid={0};MobiLinkPwd={1};ScriptVersion={2};Ping=True;Publications={3};AuthParms={4},;Stream={7}{{host={5};port={6}{8}}}",
                    user, password, syncModel, publication, syncMode, _conInfo.HostName, _conInfo.Port, _conInfo.Protocol.ToLower(), urlSuffix);
                }
                else
                {
                    //Bitbucket
                    syncParams = string.Format("MobiLinkUid={0};MobiLinkPwd={1};ScriptVersion={2};Publications={3};AuthParms={4},;Stream={7}{{host={5};port={6}{8}}}",
                    user, password, syncModel, publication, syncMode, _conInfo.HostName, _conInfo.Port, _conInfo.Protocol.ToLower(), urlSuffix);
                }

                _logger.Log(string.Format("ULWrapper.SyncDbAsync synchronizing user: '{0}'", user), LogLevel.VERBOSE);
                _logger.Log("syncParams: " + syncParams, LogLevel.VERBOSE);

                //_dbConnSemaphore.Wait();
                connection = GetConnection();
                //Bitbucket
                if (pingMode)
                    connection.Synchronize(syncParams, null);
                else //Bitbucket
                    connection.Synchronize(syncParams, syncObserver); //blocking

                //at this point the sync is finished
                var syncResult = connection.GetSyncResult();

                if (syncResult.ErrorCode != 0)
                {
                    throw new Exception("synchronization failed");
                }
            }
            catch (Exception ex)
            {
                var defaultLogMessage = "SyncDb failed";
                var errorMessage = HandleException(connection, ex, defaultLogMessage);

                if (connection != null)
                {
                    if (connection.GetSyncResult().AuthValue >= 2000)
                    {
                        throw new Exception(connection.GetSyncResult().AuthValue.ToString());
                    }
                    else
                    {
                        var errorCode = connection.GetSyncResult().StreamErrorCode != 0 ? connection.GetSyncResult().StreamErrorCode : connection.GetSyncResult().AuthValue;
                        throw new Exception(errorCode.ToString());
                    }
                }


                throw new Exception(errorMessage, ex);
            }
            finally
            {
                ReleaseConnection(connection);
                lock (_ongoingSyncLock)
                {
                    _isSyncInProgress = false;
                }
            }
        }

        public void CancelSync()
        {

            lock (_ongoingSyncLock)
            {
                if (_isSyncInProgress)
                {
                    _syncCancelRequest = true;
                }
            }

        }

        private string converSyncStateToText(int state)
        {
            switch (state)
            {
                case 0:
                    return "STATE_STARTING";
                case 1:
                    return "STATE_CONNECTING";
                case 2:
                    return "STATE_RESUMING_DOWNLOAD";
                case 3:
                    return "STATE_SENDING_HEADER";
                case 4:
                    return "STATE_SENDING_CHECK_SYNC_REQUEST";
                case 5:
                    return "STATE_WAITING_FOR_CHECK_SYNC_RESPONSE";
                case 6:
                    return "STATE_PROCESSING_CHECK_SYNC_RESPONSE";
                case 7:
                    return "STATE_SENDING_TABLE";
                case 8:
                    return "STATE_SENDING_DATA";
                case 9:
                    return "STATE_FINISHING_UPLOAD";
                case 10:
                    return "STATE_WAITING_FOR_UPLOAD_ACK";
                case 11:
                    return "STATE_PROCESSING_UPLOAD_ACK";
                case 12:
                    return "STATE_WAITING_FOR_DOWNLOAD";
                case 13:
                    return "STATE_RECEIVING_TABLE";
                case 14:
                    return "STATE_RECEIVING_DATA";
                case 15:
                    return "STATE_CHECKING_RI";
                case 16:
                    return "STATE_COMMITTING_DOWNLOAD";
                case 17:
                    return "STATE_REMOVING_ROWS";
                case 18:
                    return "STATE_CHECKPOINTING";
                case 19:
                    return "STATE_ROLLING_BACK_DOWNLOAD";
                case 20:
                    return "STATE_SENDING_DOWNLOAD_ACK";
                case 21:
                    return "STATE_DISCONNECTING";
                case 22:
                    return "STATE_DONE";
                case 23:
                    return "STATE_ERROR";
                default:
                    return state.ToString();
            }
        }
        #endregion

        #region TransferFile
        public bool TransferFile(ULTransferType type, string user, string password,
            string localFileName, string remoteFileName, string fileId, StorageFolder folder)
        {
            var fileChanged = false;
            TransferFileWithObserver(type, user, password, localFileName, remoteFileName, fileId, folder, out fileChanged, null);
            return fileChanged;
        }

        public bool TransferFileWithObserver(ULTransferType type, string user, string password,
            string localFileName, string remoteFileName, string fileId, StorageFolder folder,
            EventHandler<object> progressHandler)
        {
            var fileChanged = false;
            TransferFileWithObserver(type, user, password, localFileName, remoteFileName, fileId, folder, out fileChanged, progressHandler);
            return fileChanged;
        }

        private void TransferFileWithObserver(ULTransferType type, string user, string password,
            string localFileName, string remoteFileName, string fileId, StorageFolder folder,
            out bool fileChanged, EventHandler<object> progressHandler = null)
        {
            fileChanged = false;
            string message = null;
            ULFileTransfer transferInfo = null;
            FileTransferResult ftResult = null;
            FileTransferObserver observer = null;

            try
            {
                message = string.Format("Transferring file: '{0}'; type: {1}; folder:{2}", localFileName, type.ToString(), folder.Path);
                _logger.Log(message, LogLevel.VERBOSE);

                StreamType streamType = StreamType.HTTP;
                var urlSuffix = _conInfo.UrlSuffix.Length > 0 ? string.Format(";url_suffix={0}", _conInfo.UrlSuffix) : "";

                if (_conInfo.Protocol.Equals("HTTPS", StringComparison.CurrentCultureIgnoreCase))
                {
                    streamType = StreamType.HTTPS;
                } else if (_conInfo.Protocol.Equals("HTTP", StringComparison.CurrentCultureIgnoreCase))
                {
                    streamType = StreamType.HTTP;
                } else if (_conInfo.Protocol.Equals("TCPIP", StringComparison.CurrentCultureIgnoreCase))
                {
                    streamType = StreamType.TCPIP;
                }

                var fileTransfer = DatabaseManager.CreateFileTransfer(remoteFileName, (ushort)streamType, user, _conInfo.Subscription);
                fileTransfer.Password = password;
                fileTransfer.StreamParms = string.Format("host={0};port={1};compression=zlib{2}", _conInfo.HostName, _conInfo.Port, urlSuffix);
                fileTransfer.LocalFileName = localFileName;
                fileTransfer.LocalPath = CleanPath(folder.Path);

                transferInfo = new ULFileTransfer(type, fileId, localFileName, folder);
                lock (_ongoingFileTransfersLock)
                {
                    _ongoingFileTransfers.Add(transferInfo);
                }

                observer = new FileTransferObserver((status) =>
                {
                    transferInfo.TransferStatus = status;
                    if (progressHandler != null)
                    {
                        var msg = FileTransferProgressToJsonString(fileId, status.FileSize, status.BytesReceived, "");
                        progressHandler(this, msg);
                    }
                    return transferInfo.CancelRequested; // => true: continue downloading; false: abort
                });

                var transSuccess = false;
                //download
                if (type == ULTransferType.Download)
                {
                    //tested to work even when observer is null;
                    transSuccess = fileTransfer.DownloadFile(observer); //blocking
                }
                //upload
                else
                {
                    transSuccess = fileTransfer.UploadFile(observer); //blocking
                }

                //at this point the file transfer is finished
                ftResult = fileTransfer.GetResult();
                if (ftResult.StreamErrorCode == 0)
                {
                    if (ftResult.TransferredFile == 1)
                    {
                        fileChanged = true;
                    }
                    message = string.Format("Successfully transferred file: '{0}'; type: {1}", localFileName, type.ToString());
                    _logger.Log(message, LogLevel.INFO);
                }
                else
                {
                    throw new Exception("file transfer failed");
                }
            }
            catch (Exception ex)
            {
                int? streamError = null;
                if (ftResult != null)
                {
                    streamError = ftResult.StreamErrorCode;
                }
                message = string.Format("ULWrapper.TransferFileWithObserver: failed transfering file: '{0}'. StreamErrorCode: '{1}'", localFileName, streamError);
                _logger.Log(message, LogLevel.ERROR);
                _logger.Log(ex.Message, LogLevel.VERBOSE);
                _logger.Log(ex.StackTrace, LogLevel.VERBOSE);

                if (ftResult != null)
                {
                    if (ftResult.AuthValue >= 2000)
                    {
                        throw new Exception(ftResult.AuthValue.ToString());
                    }
                    else if (ftResult.AuthCode >= 2000)
                    {
                        throw new Exception(ftResult.AuthCode.ToString());
                    }
                    else
                    {
                        var errorCode = ftResult.StreamErrorCode != 0 ? ftResult.StreamErrorCode : ftResult.AuthValue;
                        throw new Exception(errorCode.ToString());
                    }
                }

                throw new Exception("could not transfer file " + fileId);
            }
            finally
            {
                if (transferInfo != null)
                {
                    lock (_ongoingFileTransfersLock)
                    {
                        _ongoingFileTransfers.Remove(transferInfo);
                    }
                }
            }
        }

        /// <summary>
        /// signals the cancels of the file transfer if a file transfer with <paramref name="fileId"/> is in progress
        /// </summary>
        /// <param name="fileId"></param>
        /// <returns> true if the file transfer was found and the cancel could be triggered successfully, false otherwise</returns>
        public bool CancelFileTransfer(string fileId)
        {
            try
            {
                lock (_ongoingFileTransfersLock)
                {

                    var fileDownloads = _ongoingFileTransfers.Where(y => y.FileId == fileId).ToList();
                    foreach (var fileDownload in fileDownloads)
                    {
                        _ongoingFileTransfers.Remove(fileDownload);

                        _logger.Log("Cancel file transfer: " + fileDownload.FileName, LogLevel.VERBOSE);
                        fileDownload.CancelRequested = true;
                    }
                }
            }
            catch (Exception ex)
            {
                var defaultLogMessage = string.Format("CancelFileTransfer for fileId '{0}' failed", fileId);
                HandleException(null, ex, defaultLogMessage);
                return false;
            }
            return true;
        }
        #endregion

        #region private functions
        private Connection GetConnection()
        {
            _dbAccessMre.Wait();
            return _connManager.GetConnection();
        }

        private void ReleaseConnection(Connection conn)
        {
            _connManager.ReleaseConnection(conn);
        }

        private string CleanPath(string path)
        {
            var result = path;
            if (path.EndsWith(@"\") || path.EndsWith(@"/"))
            {
                result = path.Substring(0, path.Length - 1);
            }
            return result;
        }

        private string FileTransferProgressToJsonString(string fileId, ulong fileSize, ulong bytesTransferred, string errorMessage)
        {
            var result = new JsonObject();
            result.Add("fileId", JsonValue.CreateStringValue(fileId));
            //result.Add("status", JsonValue.CreateStringValue(status.ToString()));
            result.Add("fileSize", JsonValue.CreateStringValue(fileSize.ToString()));
            result.Add("bytesTransferred", JsonValue.CreateStringValue(bytesTransferred.ToString()));
            result.Add("error", JsonValue.CreateStringValue(errorMessage));
            return result.Stringify();
        }

        private String ConvertResultSetToJson(ResultSet rs)
        {
            return ConvertResultSetToJsonInternal(rs).Stringify();
        }

        private JsonArray ConvertResultSetToJsonInternal(ResultSet rs)
        {
            var rss = rs.GetResultSetSchema();
            var cCount = rss.GetColumnCount();
            //TODO: NSLog(@"Number of columns: %d", ccount);
            var tableRep = new JsonArray();

            //for each row...
            while (rs.Next())
            {
                ushort j = 0;
                var rowRep = new JsonObject();
                var contentAsString = "";

                //for every entry in the row...
                while (cCount > j)
                {
                    var columnType = rss.GetColumnType(j);
                    //var columnName = rss.GetColumnName(j, (ushort)ColumnNameType.name_type_sql);
                    var columnName = rss.GetColumnName(j, (ushort)ColumnNameType.name_type_sql_column_only);

                    //we expect all binary data to be a char[] representing a string
                    if (columnType == (int)ColumnStorageType.TYPE_BINARY || rss.GetColumnSQLType(j) == (int)ColumnSQLType.SQLTYPE_LONGVARCHAR)
                    {
                        //TODO: this needs a bullet proof solution
                        //field has length of type long, but we can only specify int for amount to be copied....
                        var fieldLength = (int)rs.GetBinaryLength(j);
                        var contentAsByteArray = new byte[fieldLength];
                        var bytesCopied = rs.GetBytes(j, 0, contentAsByteArray, 0, fieldLength);
                        contentAsString = Util.Utf8ByteArrayToString(contentAsByteArray);
                    }
                    else
                    {
                        contentAsString = rs.GetString(j);
                    }

                    if (rowRep.ContainsKey(columnName))
                    {
                        rowRep[columnName] = JsonValue.CreateStringValue(contentAsString);
                    }
                    else
                    {
                        rowRep.Add(columnName, JsonValue.CreateStringValue(contentAsString));
                    }

                    j++;
                }
                tableRep.Add(rowRep);
            }
            return tableRep;
        }

        private List<IReadOnlyDictionary<string, object>> ConvertResultSetToDictionary(ResultSet rs)
        {
            var rss = rs.GetResultSetSchema();
            var cCount = rss.GetColumnCount();
            var result = new List<IReadOnlyDictionary<string, object>>();

            //for each row...
            while (rs.Next())
            {
                ushort j = 0;
                var row = new Dictionary<string, object>();
                var contentAsString = "";
                byte[] contentAsByteArray = null;

                //for every entry in the row...
                while (cCount > j)
                {
                    var columnType = rss.GetColumnType(j);
                    var columnName = rss.GetColumnName(j, (ushort)ColumnNameType.name_type_sql_column_only);

                    if (columnType == (int)ColumnStorageType.TYPE_BINARY)
                    {
                        //TODO: this needs a bullet proof solution
                        //in assumption we will never have an overflow...
                        var entryLength = (int)rs.GetBinaryLength(j);
                        contentAsByteArray = new byte[entryLength];
                        var bytesCopied = rs.GetBytes(j, 0, contentAsByteArray, 0, entryLength);
                        row.Add(columnName, contentAsByteArray);

                    }
                    else
                    {
                        contentAsString = rs.GetString(j);
                        row.Add(columnName, contentAsString);
                    }
                    j++;
                }
                result.Add(row);
            }
            return result;
        }


        private void SetStringParameterInPs(PreparedStatement ps, ushort position, string value)
        {
            try
            {
                ps.SetParameterString(position, value);
            }
            catch
            {
                try
                {
                    //most likely the string was > 32KB, so we use the method for ultra long strings
                    ps.AppendParameterStringChunk(position, value);
                }
                catch (Exception ex)
                {
                    //if this still fails, we set the value to null
                    ps.SetParameterNull(position);
                    string msg = string.Format("SetStringParameterInPs: could not set value {0} into prepared statement; index: {1}; error: {2}", value, position, ex.Message);
                    _logger.Log(msg, LogLevel.VERBOSE);
                }
            }
        }

        private string HandleException(Connection connection, Exception ex, string defaultLogMessage)
        {
            int errorCode = 0;
            string errorParams = null;
            string exMessage = null;
            string exStackTrace = null;
            var logMessage = defaultLogMessage + ": ";

            if (ex != null)
            {
                exMessage = ex.Message;
                exStackTrace = ex.StackTrace;

                if (connection != null)
                {
                    //handle native Ultralite exception
                    if (ex.HResult == (int)ErrorCodes.E_ULTRALITE_ERROR)
                    {
                        connection.GetLastError(out errorCode, out errorParams);
                    }

                    //as the connection is reused, me must ensure its integrity; not sure if really needed
                    connection.Rollback();
                }
            }

            //log exception
            if (errorCode != 0)
            {
                logMessage += string.Format("error code: {0}; ", errorCode);
            }
            if (!string.IsNullOrEmpty(errorParams))
            {
                logMessage += string.Format("error parameters: {0}; ", errorParams);
            }
            if (!string.IsNullOrEmpty(exMessage))
            {
                logMessage += string.Format("exception message: {0};", exMessage);
            }

            //logMessage = string.Format("{0}: error code: '{1}';" + " error parameters: '{2}'; exception message: {3}",
            //    defaultLogMessage, errorCode, errorParams, exMessage);
            _logger.Log(logMessage, LogLevel.ERROR);
            if (!string.IsNullOrEmpty(exStackTrace))
            {
                _logger.Log(exStackTrace, LogLevel.VERBOSE);
            }

            return logMessage;
        }

        #endregion

        #region IDisposable Support
        private bool disposed = false; // To detect redundant calls

        public void Dispose(bool hardCloseConnections)
        {
            if (!disposed)
            {
                //lock (_initLock)
                //{
                if (_connManager != null)
                {
                    _dbAccessMre.Reset();
                    _connManager.Close(hardCloseConnections);
                    _connManager = null;
                }
                //}
                disposed = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        ~ULWrapper()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
﻿//cordova.define("msc-cordova-mobilink-plugin.MobiLinkPlugIn", function(require, exports, module) {
//#region helpers
    var _pluginInstance = getInstance();

    function getInstance() {
        try {
            return new PluginComponent.MobilinkPlugin();
        } catch (error) {
            var erMsg = "error_plugin_initialization";
            console.log(erMsg + ": " + error.toString());
            throw erMsg;
        }
    }

    /**
     * values is a collection of key/value pairs in the following format:
     * [[key, value],[key, value], ...]
     */
    function convertToWindowsDictionary(values) {
        var result = new Windows.Foundation.Collections.PropertySet();
        if (values == undefined) {
            return result;
        }

        for (var i in values) {
            var val = values[i];
            var key = val[0];
            var value = val[1];
            result[key] = value;
        }
        return result;
    }

    /**
     sqlObject = {tableName; statement};
     tableName type: string
     statement type: string

     returns a new PluginComponent.MlSqlStatement object;
     */
    function convertToMlSqlStatement(sqlObject) {
        return new PluginComponent.MlSqlStatement(sqlObject.tableName, sqlObject.statement);
    }

    /**
     returns an array of PluginComponent.MlSqlStatement objects;
     */
    function convertToMlSqlStatementsArray(sqlObjects) {
        var result = [];
        for (var i in sqlObjects) {
            result.push(convertToMlSqlStatement(sqlObjects[i]));
        }
        return result;
    }

    /**
     syntactic sugar for calling plugin methods
     */
    function callPluginAsync(promise, successCallback, errorCallback) {

        promise.done(
            function onSuccess(result) {
                if (result.success) {
                    successCallback(result.value);
                } else {
                    if (parseInt(result.errorMessage) == NaN) {
                        errorCallback(result.errorMessage);
                    } else {
                        errorCallback(parseInt(result.errorMessage));
                    }

                }
            },
            errorCallback
        );
    }

    function tryJsonParse(jsonString) {
        try {
            return JSON.parse(jsonString);
        } catch (ex) {
            return jsonString;
        }
    }

//#endregion

//#region plugin methods
    cordova.commandProxy.add(
        "MscMobiLinkPlugIn", {

            getPluginInstance: function (success, failure, args) {
                try {
                    success(_pluginInstance);
                } catch (ex) {
                    failure(ex);
                }
            },

            defineMLInfo: function (success, failure, args) {
                var server = args[0];
                var port = args[1];
                var subscription = args[2];
                var version = args[3];
                var publication = args[4];
                var dbName = args[5];
                var protocol = args[6];
                var urlSuffix = args[7];

                var result = _pluginInstance.defineMlInfo(server, port, subscription, version, publication, dbName, protocol, urlSuffix);
                if (result.success) {
                    success(result.value);
                } else {
                    failure(result.errorMessage);
                }
            },

            checkNewUser: function (success, failure, args) {
                var user = args[0];

                callPluginAsync(_pluginInstance.checkNewUserAsync(user), success, failure);
            },

            setUserPwd: function (success, failure, args) {
                var user = args[0];
                var pwd = args[1];

                var result = _pluginInstance.setUserPwd(user, pwd), success, failure;
                if (result.success) {
                    success(tryJsonParse(result.value));
                } else {
                    failure(result.errorMessage);
                }
            },

            setKeyValue: function (success, failure, args) {
                var key = args[0];
                var value = args[1];

                var result = _pluginInstance.setKeyValue(key, value), success, failure;
                if (result.success) {
                    success(tryJsonParse(result.value));
                } else {
                    failure(result.errorMessage);
                }
            },

            getKeyValue: function (success, failure, args) {
                var key = args[0];

                var result = _pluginInstance.getKeyValue(key), success, failure;
                if (result.success) {
                    success(tryJsonParse(result.value));
                } else {
                    failure(result.errorMessage);
                }

            },

            removeKey: function (success, failure, args) {
                var key = args[0];

                var result = _pluginInstance.removeKey(key), success, failure;
                if (result.success) {
                    success(tryJsonParse(result.value));
                } else {
                    failure(result.errorMessage);
                }

            },

            login: function (success, failure, args) {
                var user = args[0];
                var pwd = args[1];

                callPluginAsync(_pluginInstance.loginAsync(user, pwd), success, failure);
            },

            sync: function (success, failure, args) {
                var syncMode = args[0];
                var syncModel = args[1];

                callPluginAsync(_pluginInstance.syncAsync(syncMode, syncModel, false), function (result) {
                    success(result);
                }, function (result) {
                    failure(result);
                });
            },

            checkConnectivity: function (success, failure, args) {
                var syncModel = args[0];
                success(1000);
            },

            cancelSync: function (success, failure, args) {
                try {
                    var result = _pluginInstance.cancelSync();
                    if (result.success) {
                        success(result.value);
                    } else {
                        failure(result.errorMessage);
                    }
                } catch (error) {
                    failure(error);
                }
            },

            //#region queries
            executeQuery: function (success, failure, args) {
                var sql = args[0];
                callPluginAsync(
                    _pluginInstance.executeQueryAsync(sql),
                    function onSuccess(response) {
                        success(tryJsonParse(response));
                    },
                    failure
                );
            },

            executeQueries: function (success, failure, args) {
                var sqlStatements = convertToMlSqlStatementsArray(args[0]);
                callPluginAsync(
                    _pluginInstance.executeQueriesSequentiallyAsync(sqlStatements),
                    function onSuccess(response) {
                        success(tryJsonParse(response));
                    },
                    failure
                );
            },

            executeQueriesBackground: function (success, failure, args) {
                var sqlStatements = convertToMlSqlStatementsArray(args[0]);
                callPluginAsync(
                    _pluginInstance.executeQueriesSequentiallyAsync(sqlStatements),
                    function onSuccess(response) {
                        success(tryJsonParse(response));
                    },
                    failure
                );
            },

            executeUpdate: function (success, failure, args) {
                var sql = args[0];
                callPluginAsync(
                    _pluginInstance.executeUpdate(sql),
                    function onSuccess(response) {
                        success(tryJsonParse(response));
                    },
                    failure
                );
            },

            executeUpdatesSequentially: function (success, failure, args) {
                var sql = JSON.stringify(args[0]);
                callPluginAsync(
                    _pluginInstance.executeUpdatesSequentiallyAsync(sql),
                    function onSuccess(response) {
                        success(tryJsonParse(response));
                    },
                    failure
                );
            },

            startLocationTracking: function (success, failure, args) {
                var interval = args[0];
                callPluginAsync(
                    _pluginInstance.startLocationTrackingAsync(interval),
                    function onSuccess(response) {
                        success(tryJsonParse(response));
                    },
                    failure
                );
            },

            stopLocationTracking: function (success, failure, args) {
                callPluginAsync(
                    _pluginInstance.stopLocationTrackingAsync(),
                    function onSuccess(response) {
                        success(true);
                    },
                    failure
                );
            },

            executeInsert: function (success, failure, args) {
                var sql = args[0];
                callPluginAsync(
                    _pluginInstance.executeInsert(sql),
                    function onSuccess(response) {
                        success(tryJsonParse(response));
                    },
                    failure
                );
            },

            executeStatementsAndCommit: function (success, failure, args) {
                callPluginAsync(
                    _pluginInstance.executeStatementsAndCommit(JSON.stringify(args[0])),
                    function onSuccess(response) {
                        success(tryJsonParse(response));
                    },
                    failure
                );
            },
            //#endregion
            //#region files
            downloadFile: function (success, failure, args) {
                var fileName = args[0];
                callPluginAsync(_pluginInstance.downloadFileAsync(fileName), success, failure);
            },

            uploadFile: function (success, failure, args) {
                var fileName = args[0];
                callPluginAsync(_pluginInstance.uploadFileAsync(fileName), success, failure);
            },

            uploadDBFile: function (success, failure, args) {
                var fileName = args[0];
                callPluginAsync(_pluginInstance.uploadDbFileAsync(fileName), success, failure);
            },

            cancelFileTransfer: function (success, failure, args) {
                var fileId = args[0];
                var result = instance.CancelFileTransfer(fileId);
                if (result.success) {
                    success(result.value);
                } else {
                    failure(result.errorMessage);
                }
            },

            uploadLogFiles: function (success, failure, args) {
                var fileName = args[0];
                callPluginAsync(_pluginInstance.uploadLogFileAsync(fileName), success, failure);
            },

            unzipFile: function (success, failure, args) {
                var filePath = args[0];
                var destFolder = args[1];

                var result = _pluginInstance.unzipFile(filePath, destFolder);
                if (result.success) {
                    success(result.value);
                } else {
                    failure(result.errorMessage);
                }
            },

            openDownloadedFile: function (success, failure, args) {
                var fileName = args[0];

                //get downloads folder
                Windows.Storage.ApplicationData.current.localFolder.createFolderAsync(
                    documentsFolderName, Windows.Storage.CreationCollisionOption.openIfExists).then(
                    function onSuccess(folder) {
                        //get file
                        folder.getFileAsync(fileName).then(
                            function onSuccess(file) {
                                //open file, with app chooser popup
                                var options = new Windows.System.LauncherOptions();
                                options.displayApplicationPicker = true;

                                Windows.System.Launcher.launchFileAsync(file, options).then(
                                    function onComplete(result) {
                                        if (result) {
                                            success();
                                        } else {
                                            failure(result);
                                        }
                                    }
                                );
                            },
                            //get file error
                            failure
                        );
                    },
                    //get downloads folder error
                    failure
                );
            },

            resizeImage: function (success, failure, args) {
                var sFileUrlString = args[0];
                var dCompressionQuality = args[1];
                var iFileTargetSize = 1024;
                if (args.length >= 3) {
                    iFileTargetSize = args[2];
                }
                var result = _pluginInstance.resizeImage(sFileUrlString, dCompressionQuality, iFileTargetSize);
                if (result.success) {
                    success(result.value);
                } else {
                    failure(result.errorMessage);
                }
            },

            //#endregion

            //#region misc
            resetDB: function (success, failure, args) {
                var result = _pluginInstance.resetDb();
                if (result.success) {
                    success(result.value);
                } else {
                    failure(result.errorMessage);
                }
            },

            deleteLogFiles: function (success, failure, args) {
                var result = _pluginInstance.deleteLogFiles();
                if (result.success) {
                    success(result.value);
                } else {
                    failure(result.errorMessage);
                }
            },

            setNewPassword: function (success, failure, args) {
                var newPw = args[0];
                var oldPw = args[1];

                var result = _pluginInstance.setNewPassword(newPw, oldPw);
                if (result.success) {
                    success(result.value);
                } else {
                    failure(result.errorMessage);
                }
            },

            clearUserData: function (success, failure, args) {
                var result = _pluginInstance.clearUserData();
                if (result.success) {
                    success(result.value);
                } else {
                    failure(result.errorMessage);
                }
            },
            //#endregion
        });
//#endregion
//});
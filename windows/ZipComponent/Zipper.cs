﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using UtilComponent;
using Windows.Foundation;
using Windows.Storage;

namespace ZipComponent
{
    public sealed class Zipper
    {
        /*
        public IAsyncOperationWithProgress<int, double> Uncompress(StorageFile File, StorageFolder Destination)
        {
            return AsyncInfo.Run<int, double>((Token, Progress) =>
            {
                return Task.Run(async () =>
                {
                    Progress.Report(0);
                    await UnZipFile(File, Destination);
                    Token.ThrowIfCancellationRequested();
                    Progress.Report(100.0);
                    return 0;
                }, Token
                  );

            });
        }
         * */
        /*
        public static IAsyncOperation<bool> TestUnZipFile()
        {
            var fileName = "zippedPics.zip";
            fileName = "US10.zip";
            return Task.Run<bool>(async () =>
          {
              //StorageFile file = await Windows.Storage.StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///" + fileName));
              var file = await ApplicationData.Current.LocalFolder.GetFileAsync(fileName);

              var success = await UnZipFile(file, ApplicationData.Current.LocalFolder);
              return success;
          }).AsAsyncOperation<bool>();

        }
        */

        /// <summary>
        ///zips the file/folder (recursively and keeps folder structure) specified by <paramref name="source"/> into a zip file named <paramref name="archiveName"/> located in <paramref name="destinationFolder"/>.
        /// </summary>
        /// <param name="source">the file/folder to zip</param>
        /// <param name="destinationFolder">the folder where to put the zip archive; make sure this is not a folder/subfolder of <paramref name="source"/></param>
        /// <param name="archiveName">the name of the zip archive to be created</param>
        /// <returns></returns>
        public static IAsyncOperation<StorageFile> ZipAsync(IStorageItem source, StorageFolder destinationFolder, string archiveName)
        {
            return Task.Run(async () =>
            {
                try
                {
                    var zipFile = await destinationFolder.CreateFileAsync(archiveName, CreationCollisionOption.ReplaceExisting);
                    using (var zipStream = await zipFile.OpenStreamForWriteAsync())
                    {
                        using (var archive = new ZipArchive(zipStream, ZipArchiveMode.Create, leaveOpen: false))
                        {
                            if (source.IsOfType(StorageItemTypes.File))
                            {
                                var sourceFile = (StorageFile)source;
                                await AddFileToArchive(archive, sourceFile, "");
                            }
                            else
                            {
                                await AddFolderToArchive(archive, (StorageFolder)source, "");
                            }
                        }
                    }
                    return zipFile;
                }
                catch (Exception ex)
                {
                    var msg = string.Format("could not zip: {0} error: {1}", source.Path, ex.Message);
                    throw new Exception(msg, ex);
                }
            }).AsAsyncOperation();
        }

        /// <summary>
        /// Single threaded unzipping;
        /// ok: archives with lots of small files
        /// not ok: archives with big files -> do parallel implementation
        /// </summary>
        /// <param name="file"></param>
        /// <param name="destination"></param>
        /// <returns></returns>
        public static IAsyncOperation<bool> UnZipAsync(StorageFile file, StorageFolder destination)
        {
            return Task.Run<bool>(async () =>
           {
               try
               {
                   var folder = destination;
                   var filename = file.DisplayName;
                   var zipStream = await file.OpenStreamForReadAsync();
                   var archive = new ZipArchive(zipStream, ZipArchiveMode.Read);

                   //parallel solution -> http://stackoverflow.com/questions/16199557/am-i-doing-something-wrong-or-is-it-not-possible-to-extract-a-zip-file-in-parall
                   foreach (ZipArchiveEntry entry in archive.Entries)
                   {
                       try
                       {
                           if (entry.Name == "")
                           {
                               // Folder 
                               await CreateFolderRecursive(destination, entry);
                           }
                           else
                           {
                               // File 
                               await ExtractFile(destination, entry);
                           }
                       }

                       catch (Exception ex)
                       {
                           Debug.WriteLine(ex.Message);
                       }
                   }
                   return true;
               }
               catch (Exception)
               {
                   return false;
               }
           }).AsAsyncOperation<bool>();
        }

        #region private functions

        private static async Task CreateFolderRecursive(StorageFolder folder, ZipArchiveEntry entry)
        {
            var steps = entry.FullName.Split('/').ToList();

            steps.RemoveAt(steps.Count() - 1);

            foreach (var i in steps)
            {
                try
                {
                    var NewFolder = await folder.CreateFolderAsync(i, CreationCollisionOption.OpenIfExists);
                }
                catch (Exception ex)
                {
                    var x = ex;
                }
            }
        }

        private static async Task ExtractFile(StorageFolder folder, ZipArchiveEntry entry)
        {
            var steps = entry.FullName.Split('/').ToList();

            steps.RemoveAt(steps.Count() - 1);

            foreach (var i in steps)
            {
                folder = await folder.CreateFolderAsync(i, CreationCollisionOption.OpenIfExists);
            }

            using (Stream fileData = entry.Open())
            {
                StorageFile outputFile = await folder.CreateFileAsync(entry.Name, CreationCollisionOption.ReplaceExisting);

                using (Stream outputFileStream = await outputFile.OpenStreamForWriteAsync())
                {
                    await fileData.CopyToAsync(outputFileStream);
                    await outputFileStream.FlushAsync();
                }
            }
        }

        /// <summary>
        /// adds a single file into the archive
        /// </summary>
        /// <param name="archive"></param>
        /// <param name="file"></param>
        /// <param name="pathInArchive">the relative path in the archive</param>
        /// <returns></returns>
        private static async Task AddFileToArchive(ZipArchive archive, StorageFile file, string pathInArchive)
        {
            try
            {
                var zipEntryPath = Path.Combine(pathInArchive, file.Name);
                var fileContent = (await Util.GetFileContent(file)).ToArray();
                var zipEntry = archive.CreateEntry(zipEntryPath, CompressionLevel.Optimal);

                using (var zipStream = zipEntry.Open())
                {
                    zipStream.Write(fileContent, 0, fileContent.Length);
                }
            }
            catch (Exception ex)
            {
                var msg = string.Format("could not add file: {0} to archive", file.Path);
                throw new Exception(msg, ex);
            }

        }

        /// <summary>
        /// recursively adds a folder and all of its content (files, subfolders) into the archive
        /// </summary>
        /// <param name="archive"></param>
        /// <param name="folder"></param>
        /// <param name="pathInArchive">the relative path in the archive</param>
        /// <returns></returns>
        private static async Task AddFolderToArchive(ZipArchive archive, StorageFolder folder, string pathInArchive)
        {
            try
            {
                var zipEntryPath = Path.Combine(pathInArchive, folder.Name);
                var folderEntries = await folder.GetItemsAsync();
                foreach (var entry in folderEntries)
                {
                    if (entry.IsOfType(StorageItemTypes.File))
                    {
                        await AddFileToArchive(archive, (StorageFile)entry, zipEntryPath);
                    }
                    else if (entry.IsOfType(StorageItemTypes.Folder))
                    {
                        await AddFolderToArchive(archive, (StorageFolder)entry, zipEntryPath);
                    }
                    else
                    {
                        var msg = string.Format("unsupported StorageItemTypes for file/folder: {0}", entry.Path);
                        throw new Exception(msg);
                    }
                }
            }
            catch (Exception ex)
            {
                var msg = string.Format("could not add folder: {0} to archive", folder.Path);
                throw new Exception(msg, ex);
            }
        }
        #endregion
    }
}

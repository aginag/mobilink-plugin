﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Storage;

namespace LoggerLib
{

    public enum LogLevel
    {
        VERBOSE = 1,
        INFO = 2,
        ERROR = 3,
        DO_NOT_LOG = 4,
    }

    public static class LogLevelExtension
    {
        private static int _padding;
        private static Dictionary<string, LogLevel> _mappings = new Dictionary<string, LogLevel>()
        {
            {"0", LogLevel.ERROR },
            {"1", LogLevel.INFO},
            {"2", LogLevel.VERBOSE },
        };

        static LogLevelExtension()
        {
            foreach (var value in Enum.GetValues(typeof(LogLevel)))
            {
                var enumAsString = value.ToString();
                if (enumAsString.Length > _padding)
                {
                    _padding = enumAsString.Length;
                }
            }
        }

        public static string ToPaddedString(this LogLevel logLevel)
        {
            return logLevel.ToString().PadRight(_padding);
        }

        public static LogLevel ParseFromString(string logLevelAsString)
        {
            LogLevel result;
            if(!_mappings.TryGetValue(logLevelAsString, out result))
            {
                throw new Exception(string.Format("LogLevelExtension.ParseFromString: unsupported log level : {0}",
                    logLevelAsString));
            }
            return result;
        }
    }

    public sealed class Logger
    {
        private StorageFolder _logFolder = null;
        private string _logFileName = null;
        private StorageFile _logFile = null;
        private LogLevel _logLevel;
        private SemaphoreSlim _semaphore = new SemaphoreSlim(1);

        private Logger() { }

        public static IAsyncOperation<Logger> CreateAsync(StorageFolder folder, string logFileName, LogLevel logLevel)
        {
            var result = new Logger();
            return result.InitializeAsync(folder, logFileName, logLevel).AsAsyncOperation();
        }

        /// <summary>
        /// provides thread safe logging
        /// </summary>
        /// <param name="message"></param>
        /// <param name="logLevel"></param>
        public void Log(string message, LogLevel logLevel)
        {
            if (IsLogLevelOk(logLevel))
            {
                LogToFileAsync(DateAndLogLevel(logLevel) + message);
            }
        }

        public StorageFile GetLogFile()
        {
            return _logFile;
        }

        /// <summary>
        /// empties the log file
        /// </summary>
        public IAsyncAction Empty()
        {
            return Task.Run(async () =>
            {
                await _semaphore.WaitAsync();
                try
                {
                    var message = CreateWelcomeMessage(_logLevel);
                    message.Add(string.Format("{0}: resetted log file successfully.", DateTime.UtcNow));
                    await FileIO.WriteLinesAsync(_logFile, message);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(String.Format("{0}: Error emptying log file: {1}",
                           DateTime.Now, e.Message));
                }
                finally
                {
                    _semaphore.Release();
                }
            }).AsAsyncAction();
        }

        public void SetLogLevel(LogLevel logLevel)
        {
            Log(string.Format("Changing log level to: {0}", logLevel.ToString()), LogLevel.INFO);
            _logLevel = logLevel;
        }

        private async Task<Logger> InitializeAsync(StorageFolder folder, string logFileName, LogLevel logLevel)
        {
            _logFolder = folder;
            _logFileName = logFileName;
            _logLevel = logLevel;

            try
            {
                _logFile = await folder.CreateFileAsync(logFileName, CreationCollisionOption.OpenIfExists);
                var message = CreateWelcomeMessage(logLevel);
                await FileIO.AppendLinesAsync(_logFile, message);
                return this;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(String.Format("{0}: Error: Unable to access/create log file: {1}",
                       DateTime.Now, _logFileName));
                throw e;
            }
        }

        /// <summary>
        /// thread safe logging
        /// </summary>
        /// <param name="message"></param>
        private async void LogToFileAsync(string message)
        {
            await _semaphore.WaitAsync();
            try
            {
                await FileIO.AppendLinesAsync(_logFile, new string[1] {message});
                if (System.Diagnostics.Debugger.IsAttached)
                {
                    System.Diagnostics.Debug.WriteLine(message);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(String.Format("{0}: Error: writing to log file: {1}",
                       DateTime.Now, e.Message));
            }
            finally
            {
                _semaphore.Release();
            }
        }

        private bool IsLogLevelOk(LogLevel logLevel)
        {
            return (int)logLevel >= (int)_logLevel;
        }

        private string DateAndLogLevel(LogLevel logLevel)
        {
            return string.Format("{0}: {1}: ", DateTime.UtcNow, logLevel.ToPaddedString());
        }
        
        private static List<string> CreateWelcomeMessage(LogLevel logLevel)
        {
            var result = new List<string>() {
                        "########################################################################",
                        "",
                        string.Format("{0}: Initializing Log (Utc time) with LogLevel: {1}", DateTime.UtcNow, logLevel)
                    };
            return result;
        }
    }
}
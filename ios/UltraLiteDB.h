//
//  UltraLiteDB.h
//  GEAODB
//
//  Created by Alexander Ilg on 03/04/14.
//
//

#import <Foundation/Foundation.h>
#import <Cordova/CDV.h>

@interface UltraLiteDB : NSObject
@property (nonatomic, weak) id <CDVCommandDelegate> commandDelegate;
@property (nonatomic, strong) NSMutableDictionary* fileTransfersInfo;
@property (nonatomic, strong) NSMutableDictionary* syncInfo;
@property bool isSyncInProgress;

+ (UltraLiteDB *)sharedInstance;

+ (void)finalize;
+ (void)initialize;
- (void) defineMLInfo:(NSString*)hostName withPort:(NSString*) portNumber andSubscription:(NSString*) subscriptionName andVersion:(NSString*) versionName andPublication:(NSString*) publicationName andDBName: (NSString*) dbName andProtocol: (NSString*) dbName andUrlSuffix: (NSString*) urlSuffix;
- (void) startConnection:(NSString*)userName;
- (void) closeConnection;
- (void)setUserName:(NSString*)userName;
- (bool) getConnections: (NSString*)encryptionKey;
- (NSString *) executeQuery:(NSString *) sqlStatement;

- (NSMutableArray *) executeQueryInternal:(NSString *) sqlStatement;

- (NSString *) executeUpdate:(NSString *) sqlStatement;
- (bool) executeUpdatesSequentially:(NSArray *) sqlStatements;

- (int) syncUser:(NSString *) user withPassword:(NSString *) password callbackId: (NSString*)callbackId andSyncMode:(NSString *) syncMode andPingMode:(bool) pingMode andPublication:(NSString*) sPublication;
- (NSString*) getLastError;
-(int) downloadFile:(NSString *) user withPassword:(NSString *) password fileName:(NSString *) file directoryPath:(NSString *) directoryPath callbackId: (NSString*)callbackId;
-(int) uploadFile:(NSString *) user withPassword:(NSString *) password fileName:(NSString *) file directoryPath:(NSString *) directoryPath callbackId: (NSString*)callbackId;
- (bool) executeStatementsAndCommit:(NSArray *) sqlStatements ;
- (NSString*) getNewObjectUUID;
- (void) setLogger:(NSObject*) Logger;
- (void *) getNewConnection;
- (void) releaseConnection: (void *)conn ;
- (void) addNewFileTransferInfo: (NSMutableDictionary*)fileTransfer;
- (NSMutableDictionary*) readFileTransferInfo: (NSString*)fileId;
- (void) removeFileTransferInfo: (NSString*)fileId;
- (void) cancelFileTransfer: (NSString*)fileId;
- (void) cancelSync;
@end


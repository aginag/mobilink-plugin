//
//  Msc MobilinkPlugin.swift
//  SAM Mobilink Plug In
//
//  Created by Ganesh Subramanian on 31/05/16.
//
//

import Foundation
import SystemConfiguration
import QuartzCore
import MobileCoreServices

let kPaperSizeA4 = CGSize.init(width: 595.2, height: 841.8)
let kPaperSizeLetter = CGSize.init(width: 612, height: 792)
let pageMargins = UIEdgeInsets.init(top: 10, left: 5, bottom: 10, right: 5)


var dbFileName = "sam"
var logFolderName = "Logs"
var logFileName = "Log.txt"
var transactionLogFileName = "TransactionLog.txt"
var defaultLogLevel = logLevel.verbose
let logger:MobilinkLogger = MobilinkLogger()
var semaphore = DispatchSemaphore(value: 3)
let bundleID = Bundle.main.bundleIdentifier
let keychain = KeychainSwift(keyPrefix: bundleID!)

@objc(MscMobiLinkPlugIn) class MscMobilinkPlugIn: CDVPlugin, UIDocumentInteractionControllerDelegate, UIWebViewDelegate {
    fileprivate var user:String = ""
    fileprivate var pwd:String = ""
    fileprivate var cdvViewController: CDVViewController?
    fileprivate var filePath:String?
    fileprivate var callbackId:String = ""
    private var documentController: UIDocumentInteractionController?
    private var trackingService: GeoTrackingService?

    func fileHasSupportedType(fileName: String) -> Bool {
        return fileName.lowercased().hasSuffix(".jpeg") || fileName.lowercased().hasSuffix(".jpg") || fileName.lowercased().hasSuffix(".png") || fileName.lowercased().hasSuffix(".heic")
    }
    func handleFileCompression(image: UIImage, fileUrlString: String, fileUrl: URL, compressionQuality: CGFloat, fileTargetSize: CGFloat) -> CDVPluginResult {
        let compressedImage = image.resizeImage(CGSize(width: fileTargetSize, height: fileTargetSize))
        if let compressedImageData =  compressedImage.jpegData(compressionQuality: compressionQuality) {
            if (fileUrlString.lowercased().hasSuffix(".heic")) {
                let newFilePath = NSURL.fileURL(withPath: fileUrlString.replacingOccurrences(of: ".heic", with: ".jpeg", options: .caseInsensitive))
                logger.log(logLevel: logLevel.info, message: "Writting new .jpeg file:" + newFilePath.absoluteString)
                do { try compressedImageData.write(to: newFilePath) } catch {
                    logger.log(logLevel: logLevel.error, message: "Error on writting compressed file:" + newFilePath.absoluteString)
                    return CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: "Error on writing compressed file")
                }
                let fileManager = FileManager.default
                logger.log(logLevel: logLevel.info, message: "Delete .heic file:" + fileUrlString)
                do { try fileManager.removeItem(at: fileUrl) } catch {
                    logger.log(logLevel: logLevel.error, message: "Error on deleting .HEIC file:" + fileUrlString)
                    return CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: "Error on deleting .HEIC file")
                }
            } else {
                logger.log(logLevel: logLevel.info, message: "Writting compressed file:" + fileUrlString)
                do { try compressedImageData.write(to: fileUrl) } catch {
                    logger.log(logLevel: logLevel.error, message: "Error on writting compressed file:" + fileUrlString)
                    return CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: "Error on writing compressed file")
                }
            }
        }

        return CDVPluginResult(status: CDVCommandStatus_OK, messageAs: "done")
    }
    func resizeImage(_ command:CDVInvokedUrlCommand) -> Void {
        let fileUrlString = command.arguments[0] as! String
        let compressionQuality = command.arguments[1] as! CGFloat
        var fileTargetSize: CGFloat = 1024

        if (command.arguments.count >= 3) {
            fileTargetSize = command.arguments[2] as? CGFloat ?? 1024
        }

        var pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: "Generic Error")
        logger.log(logLevel: logLevel.info, message: "Compression Request received for file:" + fileUrlString)
        logger.log(logLevel: logLevel.info, message: "Compression quality: \(compressionQuality)")
        logger.log(logLevel: logLevel.info, message: "File target size: \(fileTargetSize)px")

        if (fileHasSupportedType(fileName: fileUrlString)) {
            if let fileUrl = URL(string: fileUrlString) {
                if let image = UIImage(contentsOfFile: fileUrl.path) {
                    pluginResult = handleFileCompression(image: image,
                                                         fileUrlString: fileUrlString,
                                                         fileUrl: fileUrl,
                                                         compressionQuality: compressionQuality,
                                                         fileTargetSize: fileTargetSize)
                }
            }
        }
        logger.log(logLevel: logLevel.info, message: "Compression done for file:" + fileUrlString)
        self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
    }

    func startLocationTracking(_ command:CDVInvokedUrlCommand) -> Void {
        guard let trackingInterval = command.arguments[0] as? String else {
            self.commandDelegate.send(CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: "NO_INTERVAL_PROVIDED"), callbackId: command.callbackId)
            return;
        }

        if (trackingService == nil) {
            trackingService = GeoTrackingService(withCommandDelegate: self.commandDelegate, andCommand: command)
        }

        do {
            try trackingService?.start(withInterval: trackingInterval, andUser: GeoTrackingService.User(name: user, password: pwd))
        } catch GeoTrackingService.LocationError.serviceNotEnabled {
            self.commandDelegate.send(CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: "LOCATION_SERIVCES_NOT_ENABLED"), callbackId: command.callbackId)
        } catch GeoTrackingService.LocationError.permissionNotGranted {
            self.commandDelegate.send(CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: "LOCATION_SERIVCES_PERMISSION_NOT_GRANTED"), callbackId: command.callbackId)
        } catch {
            self.commandDelegate.send(CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: "LOCATION_SERIVCES_UNDEFINED_START_ERROR"), callbackId: command.callbackId)
        }
    }

    func stopLocationTracking(_ command:CDVInvokedUrlCommand) -> Void {
        trackingService?.stop()
        trackingService = nil
        self.commandDelegate.send(CDVPluginResult(status: CDVCommandStatus_OK, messageAs: "OK"), callbackId: command.callbackId)
    }

    func executeQuery(_ command: CDVInvokedUrlCommand) -> Void {
        let semaphoreResult = semaphore.wait(timeout: .distantFuture)
        var result:NSArray? = nil
        let pluginResult:CDVPluginResult
        if(semaphoreResult == DispatchTimeoutResult.success){
            logger.log(logLevel: logLevel.verbose, message: "execute Query:" + (command.arguments[0] as! String))
            result = UltraLiteDB.sharedInstance().executeQueryInternal(command.arguments[0] as! String)
            if((result) != nil){
                pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: result as NSArray? as? [Any])
            }else{
                pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: "ERROR QUERY: \(UltraLiteDB.sharedInstance().getLastError() ?? ""))")
            }

            self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
            semaphore.signal()
        }
    }
    func runTest(_ command: CDVInvokedUrlCommand) -> Void {
        var pluginResult:CDVPluginResult
        let resultSet:NSMutableDictionary = NSMutableDictionary()
        var i = 0
        //command.argument(at: 0) as! String
        let queryStr = "SELECT TOP 200  MOBILE_ID,EQUNR,TPLNR,FUNCLOC_DISP,EQTYP,EQART,OBJNR,MATNR,WERK,LAGER,SHTXT,TPLNR_SHTXT,TPLMA_SHTXT,TIDNR,FLTYP,SERNR,STREET,POST_CODE1,CITY1,COUNTRY,ACTION_FLAG FROM Equipment ORDER BY EQUNR DESC"
        print("invoked " + String(Date().timeIntervalSinceReferenceDate) )
        while i < 25 {
            self.commandDelegate.run {
                let semaphoreResult = semaphore.wait(timeout: .distantFuture)
                if(semaphoreResult == DispatchTimeoutResult.success){

                    let queryResult:NSMutableArray = UltraLiteDB.sharedInstance().executeQueryInternal(queryStr)
                    print("Time for query " + " = " + String(Date().timeIntervalSinceReferenceDate))
                    semaphore.signal()
                }

            }
            i = i + 1
        }

        pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: "done")
        self.commandDelegate.send(pluginResult, callbackId: command.callbackId)


    }
    func executeQueries(_ command: CDVInvokedUrlCommand) -> Void {
        var pluginResult:CDVPluginResult
        let resultSet:NSMutableDictionary = NSMutableDictionary()
        var queryResult:NSMutableArray? = nil
        print("invoked " + String(Date().timeIntervalSinceReferenceDate) )
        let semaphoreResult = semaphore.wait(timeout: .distantFuture)
        if(semaphoreResult == DispatchTimeoutResult.success){
            for sqlStatement in command.argument(at: 0) as! [NSObject] {
                logger.log(logLevel: logLevel.verbose, message: "execute Query:" + (sqlStatement.value(forKey: "statement") as! String))
                queryResult = UltraLiteDB.sharedInstance().executeQueryInternal(sqlStatement.value(forKey: "statement") as! String)
                resultSet[sqlStatement.value(forKey: "tableName") as! String] = queryResult
            }
            if((queryResult) != nil){
                let res:NSDictionary = resultSet.copy() as! NSDictionary
                pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: res as NSDictionary? as? [AnyHashable: Any] ?? [:] )
            }else{
                pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: "ERROR QUERY: \(UltraLiteDB.sharedInstance().getLastError() ?? "")")
            }
            let res:NSDictionary = resultSet.copy() as! NSDictionary
            pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: res as NSDictionary? as? [AnyHashable: Any] ?? [:] )
            self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
            semaphore.signal()
            print("invoked " + String(Date().timeIntervalSinceReferenceDate) )
        }
    }
    func executeQueriesBackgroundInternal(_ sqlStatements: NSArray) -> NSDictionary? {
        let resultSet:NSMutableDictionary = NSMutableDictionary()
        var queryResult:NSMutableArray? = nil
        var queryFailed = false
        print("invoked " + String(Date().timeIntervalSinceReferenceDate) )
        for sqlStatement in sqlStatements {
            let semaphoreResult = semaphore.wait(timeout: .distantFuture)
            if(semaphoreResult == DispatchTimeoutResult.success){
                logger.log(logLevel: logLevel.verbose, message: "execute Query:" + ((sqlStatement as AnyObject).value(forKey: "statement") as! String))
                queryResult = UltraLiteDB.sharedInstance().executeQueryInternal((sqlStatement as AnyObject).value(forKey: "statement")  as! String)
                if((queryResult) != nil){
                    resultSet[(sqlStatement as AnyObject).value(forKey: "tableName") as! String] = queryResult
                }else{
                    queryFailed = true
                }

                semaphore.signal()
            }
        }
        print("invoked " + String(Date().timeIntervalSinceReferenceDate) )
        if(queryFailed){
            return nil
        }else{
            return resultSet.copy() as! NSDictionary
        }

    }
    func executeQueriesBackground(_ command: CDVInvokedUrlCommand) -> Void {
        self.commandDelegate.run  {
            var pluginResult:CDVPluginResult
            let result:NSDictionary? = self.executeQueriesBackgroundInternal(command.argument(at: 0) as! [NSObject] as NSArray)
            if((result) != nil){
                pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: result as NSDictionary? as? [AnyHashable: Any] ?? [:] )
            }else{
                pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: "Error with SQL query")
            }
            self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
        }
    }
    func executeUpdate(_ command: CDVInvokedUrlCommand) -> Void {
        let semaphoreResult = semaphore.wait(timeout: .distantFuture)
        if(semaphoreResult == DispatchTimeoutResult.success){
            logger.log(logLevel: logLevel.verbose, message: "execute Update:" + (command.arguments[0] as! String), isTransaction: true)
            let result:String = UltraLiteDB.sharedInstance().executeUpdate(command.arguments[0] as! String)
            let pluginResult:CDVPluginResult;

            if (result == "1") {
                pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: result)
            } else {
                pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: result)
            }

            self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
            semaphore.signal()
        }
    }

    func executeUpdatesSequentially(_ command: CDVInvokedUrlCommand) -> Void {
        guard let statements = command.arguments[0] as? NSArray else {
            self.commandDelegate.send(CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: "Array of statements required"), callbackId: command.callbackId);
            return;
        }

        let semaphoreResult = semaphore.wait(timeout: .distantFuture)
        if(semaphoreResult == DispatchTimeoutResult.success){
            let result:Bool = UltraLiteDB.sharedInstance()?.executeUpdatesSequentially(statements as! [Any]) ?? false;

            let pluginResult:CDVPluginResult;

            if (result) {
                pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: result)
            } else {
                pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: result)
            }

            self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
            semaphore.signal()
        }
    }

    func executeStatementsAndCommit( _ command: CDVInvokedUrlCommand) -> Void {
        let SQLStatements:NSArray = (command.argument(at: 0)) as! NSArray
        var pluginResult:CDVPluginResult
        let semaphoreResult = semaphore.wait(timeout: .distantFuture)
        if(semaphoreResult == DispatchTimeoutResult.success){
            logger.log(logLevel: logLevel.verbose, messageStructure: SQLStatements as? [NSMutableDictionary], isTransaction: true)
            let result:Bool = UltraLiteDB.sharedInstance().executeStatementsAndCommit(SQLStatements as [AnyObject])
            if(result){
                let resultString: String = UltraLiteDB.sharedInstance().getNewObjectUUID();
                pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: resultString)
            }else{

                let returnString:String = UltraLiteDB.sharedInstance().getLastError()
                pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: returnString)
            }

            self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
            semaphore.signal()
        }
    }
    func executeInsert(_ command: CDVInvokedUrlCommand) -> Void {
        //print("execute Insert:" + (command.arguments[0] as! String))
        let semaphoreResult = semaphore.wait(timeout: .distantFuture)
        if(semaphoreResult == DispatchTimeoutResult.success){
            logger.log(logLevel: logLevel.verbose, message: "execute Insert:" + (command.arguments[0] as! String))
            let result:String = UltraLiteDB.sharedInstance().executeUpdate(command.arguments[0] as! String)
            let pluginResult:CDVPluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: result)
            self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
            semaphore.signal()
        }
    }
    func checkNewUser(_ command:CDVInvokedUrlCommand) -> Void{
        var pluginResult:CDVPluginResult;
        var documentsPathUser:String = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        documentsPathUser.append("/" + (command.arguments[0] as! String) + "/" + dbFileName + ".udb")
        if FileManager.default.fileExists(atPath: documentsPathUser) {
            pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: false)
            self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
        }
        else{
            pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: true)
            self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
        }
    }
    func checkConnectivity(_ command: CDVInvokedUrlCommand) -> Void {
        var pluginResult:CDVPluginResult
        pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: 1000)
        self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
    }
    func sync(_ command:CDVInvokedUrlCommand){
        self.commandDelegate.run( inBackground:  {
            //print("Sync Called")
            let semaphoreResult = semaphore.wait(timeout: .distantFuture)
            if(semaphoreResult == DispatchTimeoutResult.success){
                logger.log(logLevel: logLevel.info, message: "Sync called")
                var pluginResult:CDVPluginResult
                var syncMode: String = ""
                var publication: String = ""

                if(command.arguments.count > 0) {
                    if let arg = command.argument(at: 0) as? String {
                        syncMode = arg
                    }
                }
                if(command.arguments.count > 1) {
                    if let arg = command.argument(at: 1) as? String {
                        publication = arg
                    }
                }

                let result:Int32 = UltraLiteDB.sharedInstance().syncUser(self.user, withPassword: self.pwd, callbackId: command.callbackId, andSyncMode: syncMode, andPingMode: false, andPublication: publication)
                if(result == 1000){
                    //rename uploaded files with correct link id
                    logger.log(logLevel: logLevel.info, message: "Sync finished, updating attachments")
                    //let updatedFiles:Bool = updateSyncedFiles()
                    //let resultAfterSync:Bool = self.updateSyncedFiles() && self.setLogLevelFromDB()
                    let resultAfterSync:Bool = self.setLogLevelFromDB()
                    pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: resultAfterSync)
                }else{
                    pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: result)
                }

                self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
                semaphore.signal()
            }
        })
    }
    func cancelSync(_ command:CDVInvokedUrlCommand){
        logger.log(logLevel: logLevel.info, message: "CancelSync called ")
        var pluginResult:CDVPluginResult
        UltraLiteDB.sharedInstance().cancelSync()
        pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: true)
        self.commandDelegate.send(pluginResult, callbackId: command.callbackId)

    }

    @objc func willTerminate() {
        print("Cancel Sync")
        UltraLiteDB.sharedInstance().cancelSync()
        //wait for a second (for sync observer to kick in)
        sleep(1)
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        UltraLiteDB.sharedInstance().closeConnection()
        UltraLiteDB.finalize()
    }

    @objc func defineMLInfo(_ command:CDVInvokedUrlCommand){
        NotificationCenter.default.addObserver(self, selector: #selector(willTerminate), name: UIApplication.willTerminateNotification, object: nil)

        UltraLiteDB.sharedInstance().commandDelegate = self.commandDelegate
        dbFileName = command.argument(at: 5) as! String
        UltraLiteDB.sharedInstance().defineMLInfo(command.argument(at: 0) as! String, withPort:  command.argument(at: 1) as! String, andSubscription: command.argument(at: 2) as! String, andVersion: command.argument(at: 3) as! String, andPublication: command.argument(at: 4) as! String, andDBName: command.argument(at: 5) as! String + ".udb", andProtocol: command.argument(at: 6) as! String, andUrlSuffix: command.argument(at: 7) as! String)
        let pluginResult:CDVPluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: true)
        self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
    }

    func syncDBFile(_ userName:String, password:String, callbackId: String) -> Int32 {
        print("Downloading File from Server")
        let destFolder = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.allDomainsMask, true).first!
        var result:Int32 = UltraLiteDB.sharedInstance().downloadFile(userName, withPassword: password, fileName: dbFileName + ".udb", directoryPath: destFolder, callbackId: callbackId)
        var isDir: ObjCBool = false
        if result == 1000 {
            //copy file into user specific folder
            do {
                var documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
                documentsPath.append("/" + dbFileName + ".udb")
                var documentsPathUser = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
                documentsPathUser.append("/" + user + "/" + dbFileName + ".udb")
                let docFolderPathUser = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] + "/" + user
                if(!FileManager.default.fileExists(atPath: docFolderPathUser, isDirectory: &isDir)){
                    if(!isDir.boolValue){
                        try FileManager.default.createDirectory(atPath: docFolderPathUser, withIntermediateDirectories: false, attributes: nil)
                    }

                }
                try FileManager.default.copyItem(atPath: documentsPath, toPath: documentsPathUser)
                try FileManager.default.removeItem(atPath: documentsPath)
            } catch _ as NSError {
                result = 4000
            }
            //remove file after copy
        }
        return result

    }

    func createEncryptionKeyFile(user:String) -> Bool {
        let encKey: String = "SAMEncKey" + self.user
        let encrKeyValue = keychain.get(encKey)
        if(encrKeyValue == nil){
            return false
        }
        var isDir: ObjCBool = false
        let filePath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] + "/" + self.user + "/sam_key.txt"


        if(!FileManager.default.fileExists(atPath: filePath, isDirectory: &isDir)){
            if(!isDir.boolValue){
                FileManager.default.createFile(atPath: filePath, contents: encrKeyValue!.data(using: String.Encoding.utf8), attributes: nil)
                return true
            }
        }
        return true
    }
    func uploadDBFile(_ command:CDVInvokedUrlCommand) -> Void {
        self.commandDelegate.run( inBackground:  {
            logger.log(logLevel: logLevel.info, message: "Uploading UDB file to server")
            var pluginResult:CDVPluginResult
            //let timeString: String = NSDate().timeIntervalSince1970.description.replacingOccurrences(of: ".", with: "-")

            var result:Int32 = 0
            let pwdKey: String = "SAMPwd" + self.user
            let password = keychain.get(pwdKey)!

            let folderPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] + "/" + self.user
            let zipFileName = command.argument(at: 0) as! String
            let documentPath:String = folderPath + "/" + dbFileName + ".udb"
            let zipPath:String = folderPath + "/" + zipFileName //dbFileName + timeString + ".zip"
            do {
                // Create zip file
                var resultZip:Bool
                if(self.createEncryptionKeyFile(user: self.user)){
                    //encryption key file created
                    let encryptionKeyFilePath:String = folderPath + "/sam_key.txt"
                    resultZip = SSZipArchive.createZipFile(atPath: zipPath, withFilesAtPaths: [documentPath, encryptionKeyFilePath])
                    //remove file
                    try FileManager.default.removeItem(atPath: encryptionKeyFilePath)
                }else{
                    resultZip = SSZipArchive.createZipFile(atPath: zipPath, withFilesAtPaths: [documentPath])
                }

                if(resultZip){
                    result = UltraLiteDB.sharedInstance().uploadFile(self.user, withPassword: password, fileName: zipFileName, directoryPath:folderPath, callbackId: command.callbackId)
                    try FileManager.default.removeItem(atPath: zipPath)
                }
                else{
                    pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: "Error creating zip file")
                }

            } catch _ as NSError {
                pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: "error")
            }

            if(result == 1000){
                pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: result)
                logger.log(logLevel: logLevel.info, message: "Uploaded UDB file to server")
            }else{
                pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: result)
                logger.log(logLevel: logLevel.error, message: "Upload UDB failed")
            }

            self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
        })
    }
    func uploadLogFiles(_ command:CDVInvokedUrlCommand) -> Void {
        self.commandDelegate.run( inBackground:  {
            logger.log(logLevel: logLevel.info, message: "Uploading Log files to server")
            var pluginResult:CDVPluginResult

            var result:Int32 = 0
            var resultZip:Bool = false
            let pwdKey: String = "SAMPwd" + self.user
            let password = keychain.get(pwdKey)!
            let newLogFileName = command.argument(at: 0) as! String
            let folderPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] + "/" + self.user + "/" + logFolderName

            do {
                let zipPath:String = folderPath + "/" + newLogFileName
                // Create zip file
                resultZip = SSZipArchive.createZipFile(atPath: zipPath, withContentsOfDirectory: folderPath)
                if(resultZip){
                    result = UltraLiteDB.sharedInstance().uploadFile(self.user, withPassword: password, fileName: newLogFileName, directoryPath:folderPath, callbackId: command.callbackId)
                    try FileManager.default.removeItem(atPath: zipPath)
                }

            } catch _ as NSError {
                pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: "error")
            }

            if(result == 1000 && resultZip){
                pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: result)
                logger.log(logLevel: logLevel.info, message: "Uploaded log files to server")
            }else{
                pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: result)
                logger.log(logLevel: logLevel.error, message: "Upload log files failed")
            }

            self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
        })
    }

    func deleteLogFiles(_ command:CDVInvokedUrlCommand) -> Void {
        var pluginResult: CDVPluginResult!

        self.commandDelegate.run( inBackground:  {
            // Delete Log Files and re-init logger
            if logger.removeLogFiles() && self.initLogger(user: self.user){
                // Successfully created
                pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: "Log Files removed successfully")
            } else {
                pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: "Log Files could not be removed")
            }

            self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
        })
    }

    func downloadFile(_ command:CDVInvokedUrlCommand) -> Void {
        self.commandDelegate.run( inBackground:  {
            var pluginResult:CDVPluginResult
            var result:Int32 = 1000
            let pwdKey: String = "SAMPwd" + self.user
            let password = keychain.get(pwdKey)!
            let fileName = command.argument(at: 0) as! String
            let folderPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            let documentPath:String = folderPath + "/" + fileName
            if(!FileManager.default.fileExists(atPath: documentPath)){
                logger.log(logLevel: logLevel.info, message: "Downloading file from server:" + fileName)
                let destFolder = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.allDomainsMask, true).first!
                result = UltraLiteDB.sharedInstance().downloadFile(self.user, withPassword: password, fileName: fileName, directoryPath: destFolder, callbackId: command.callbackId)
            }

            if(result == 1000){
                pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: result)
            }else{
                pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: result)
            }

            self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
        })
    }

    func uploadFile(_ command:CDVInvokedUrlCommand) -> Void {
        print("file upload")
        self.commandDelegate.run  ( inBackground:  {
            var pluginResult:CDVPluginResult
            var result:Int32 = 1000
            let pwdKey: String = "SAMPwd" + self.user
            let password = keychain.get(pwdKey)!
            let fileName = command.argument(at: 0) as! String
            let folderPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] + "/" + self.user + "/attachments"
            let documentPath:String = folderPath + "/" + fileName
            print(documentPath)
            if(FileManager.default.fileExists(atPath: documentPath)){
                logger.log(logLevel: logLevel.info, message: "Uploading file to server:" + fileName)
                result = UltraLiteDB.sharedInstance().uploadFile(self.user, withPassword: password, fileName: fileName, directoryPath: folderPath, callbackId: command.callbackId)
            }

            if(result == 1000){
                pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: result)
            }else{
                pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: result)
            }

            self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
        } )
    }
    func cancelFileTransfer(_ command:CDVInvokedUrlCommand){
        self.commandDelegate.run( inBackground:  {
            logger.log(logLevel: logLevel.info, message: "MobilinkPlugin.CancelFileTransfer: fileId " + (command.arguments[0] as! String))
            var pluginResult:CDVPluginResult
            UltraLiteDB.sharedInstance().cancelFileTransfer(command.argument(at: 0) as! String!)
            pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: true)
            self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
        })
    }
    func setNewPassword(_ command:CDVInvokedUrlCommand) -> Void {
        var pluginResult:CDVPluginResult
        //Here we want to save the new password for this user
        logger.log(logLevel: logLevel.info, message: "Saving new password to keychain")
        let newPwd: String = command.argument(at: 0) as! String
        let oldPwd: String = command.argument(at: 1) as! String

        let pwdKey: String = "SAMPwd" + self.user
        let oldSavedPwd = keychain.get(pwdKey)
        if(oldSavedPwd != oldPwd){
            pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: false)
            logger.log(logLevel: logLevel.error, message: "oldPwdIncorrect")
        }else{
            let result: Bool = keychain.set(newPwd, forKey: pwdKey)
            self.pwd = newPwd
            if(result){
                pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: result)
                logger.log(logLevel: logLevel.info, message: "newPwdSaved")
            }else{
                pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: result)
                logger.log(logLevel: logLevel.error, message: "New password not saved")
            }
        }

        self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
    }
    func setKeyValue(_ command:CDVInvokedUrlCommand) -> Void {
        let key: String = command.argument(at: 0) as! String
        let value: String = command.argument(at: 1) as! String
        let saved: Bool = keychain.set(value, forKey: key, withAccess: KeychainSwiftAccessOptions.accessibleAlwaysThisDeviceOnly )
        let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: saved)
        self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
    }
    func setUserPwd(_ command:CDVInvokedUrlCommand) -> Void {
        self.user = command.argument(at: 0) as! String
        self.pwd = command.argument(at: 1) as! String
        let key:String = "SAMPwd" + self.user
        let saved: Bool = keychain.set(self.pwd, forKey: key, withAccess: KeychainSwiftAccessOptions.accessibleAlwaysThisDeviceOnly )
        var pluginResult:CDVPluginResult
        var isDir: ObjCBool = false
        let docFolderPathUser = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] + "/" + self.user
        let attachmentsFolderPathUser = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] + "/" + self.user + "/attachments"

        if (saved) {
            do{
                if(!FileManager.default.fileExists(atPath: docFolderPathUser, isDirectory: &isDir)){
                    if(!isDir.boolValue){
                        try FileManager.default.createDirectory(atPath: docFolderPathUser, withIntermediateDirectories: false, attributes: nil)
                        try FileManager.default.createDirectory(atPath: attachmentsFolderPathUser, withIntermediateDirectories: false, attributes: nil)
                    }

                }
                UltraLiteDB.sharedInstance().setUserName(self.user)
                initLogger(user: self.user)
                pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: saved)
            } catch _ as NSError {
                pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: "Cannot create user folder")
            }


        }else{
            pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: saved)
        }
        //let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: connectionsOK)
        self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
    }
    func getKeyValue(_ command:CDVInvokedUrlCommand) -> Void {
        let key: String = command.argument(at: 0) as! String
        let value: String = keychain.get(key)!
        let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: value)
        self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
    }
    func removeKey(_ command:CDVInvokedUrlCommand) -> Void {
        let key: String = command.argument(at: 0) as! String
        let result:Bool = keychain.delete(key)
        let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: result)
        self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
    }
    func login(_ command: CDVInvokedUrlCommand) -> Void {
        let key: String = "SAMPwd" + (command.argument(at: 0) as! String)
        let pwd: String = command.argument(at: 1) as! String
        var pluginResult:CDVPluginResult
        let savedPwd: String = keychain.get(key)!
        let encrKey: String = "SAMEncKey" + (command.argument(at: 0) as! String)
        //let encrKeyValue:String = keychain.get(encrKey) as! String
        print(keychain.get(encrKey))
        if savedPwd == self.pwd {
            //Password matched, try and get connections
            let connectionsOK: Bool = UltraLiteDB.sharedInstance().getConnections(keychain.get(encrKey))
            if(connectionsOK){
                //All good, set log level from DB
                setLogLevelFromDB()
                pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: "Login Successful")
            }else{
                pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: "Cannot connect to DB")
            }
        }
        else{
            pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: "Wrong password")
        }
        self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
    }

    func initLogger(user:String) -> Bool {
        if( logger.checkAndCreateLogFolder(logFolder: (user + "/" + logFolderName)) ){
            logger.setupLogger(logFolder: (user + "/" + logFolderName), logFileName: logFileName, logLevel: defaultLogLevel, transactionLogFileName: transactionLogFileName)
            logger.log(logLevel: logLevel.info, message: "########################################################################")
            logger.log(logLevel: logLevel.info, message: "Logger initialized in GMT Timezone (Device: \(logger.deviceType), OS: \(logger.deviceOS))")
            UltraLiteDB.sharedInstance().setLogger(logger)
            logger.setLogLevel(logLevel: logLevel.error, transactionLog: false)
            return true
            //return setLogLevelFromDB()
        }
        else{
            return false
        }
    }
    func setLogLevelFromDB() -> Bool {
        logger.log(logLevel: logLevel.info, message: "Setting log level from database")
        let logLevelSQLString = "SELECT appl_log_level AS APPL_LOG_LEVEL, transac_log_on AS TRANSAC_LOG_ON FROM SAM_USERS WHERE UPPER(user_name)=UPPER('" + self.user + "')"
        let semaphoreResult = semaphore.wait(timeout: .distantFuture)
        if(semaphoreResult == DispatchTimeoutResult.success){
            let resultList:NSArray = UltraLiteDB.sharedInstance().executeQueryInternal(logLevelSQLString)
            if resultList.count == 1 {
                let level = logLevel(rawValue: Int.init((resultList.firstObject as! NSObject).value(forKey: "APPL_LOG_LEVEL") as! String)!)
                let transactLogOn:Bool = ((resultList.firstObject as! NSObject).value(forKey: "TRANSAC_LOG_ON") as! String) == "1"
                logger.setLogLevel(logLevel: level!, transactionLog: transactLogOn)
                semaphore.signal()
                return true
            }
            semaphore.signal()
        }
        return false
    }
    func resetDB(_ command:CDVInvokedUrlCommand) -> Void {
        logger.log(logLevel: logLevel.info, message: "########################################################################")
        logger.log(logLevel: logLevel.info, message: "Reset DB")
        var pluginResult:CDVPluginResult
        var documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        documentsPath.append("/" + self.user + "/" + dbFileName + ".udb")
        print(documentsPath)
        if FileManager.default.fileExists(atPath: documentsPath) {

            do {
                UltraLiteDB.sharedInstance().closeConnection()
                try FileManager.default.removeItem(atPath: documentsPath)
                //Delete encryption key if exists
                keychain.delete("SAMEncKey" + self.user)
                let result = logger.resetLogFiles()
                pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: result)
            } catch _ as NSError {
                pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: "error")
            }
        }
        else{
            pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: "success")
        }
        self.commandDelegate.send(pluginResult, callbackId: command.callbackId)

    }
    func clearUserData(_ command: CDVInvokedUrlCommand) -> Void {
        var pluginResult:CDVPluginResult

        do {
            let dir = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.allDomainsMask, true).first
            if (dir != nil) {
                let filePaths = try FileManager.default.contentsOfDirectory(atPath: dir!)
                for filePath in filePaths {
                    var isDir: ObjCBool = false
                    let fullPath: String = dir! + "/" + filePath
                    if FileManager.default.fileExists(atPath: fullPath, isDirectory:&isDir) {
                        if isDir.boolValue {
                            // in case it is a directory, it might be a user directory. Hence, remove the key from the keychain
                            keychain.delete("SAMEncKey" + filePath)
                            keychain.delete("SAMPwd" + filePath)
                        }
                    }
                    print("Removing file: " + fullPath)
                    try FileManager.default.removeItem(atPath: fullPath)
                }
            }
        } catch {
            pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: "Could not clear directory")
        }

        pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: "success")
        self.commandDelegate.send(pluginResult, callbackId: command.callbackId)

    }
    func log(_ command:CDVInvokedUrlCommand) -> Void {
        let level:logLevel = logLevel(rawValue: Int.init(command.argument(at: 0) as! String)!)!
        let message:String = command.argument(at: 1) as! String
        let isTransaction:Bool = (command.argument(at: 2) as! String == "1")
        logger.log(logLevel: level, message:message, isTransaction: isTransaction)
        let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: "success")
        self.commandDelegate.send(pluginResult, callbackId: command.callbackId)

    }
    // MARK: FileOpener
    func openFile(_ command: CDVInvokedUrlCommand) -> Void {

        let path:String = command.argument(at: 0) as! String
        var contentType:String = ""
        var showPreview:Bool = true
        if(command.arguments.count > 1){
            contentType = command.argument(at: 1) as! String
        }
        if(command.arguments.count > 2){
            showPreview = command.argument(at: 2) as! Bool
        }
        let cont: CDVViewController = super.viewController as! CDVViewController
        self.cdvViewController = cont
        let dotParts = path.components(separatedBy: ".")
        let fileExt:String = dotParts.last!
        let uti:NSString = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, fileExt as CFString, nil)?.takeRetainedValue() as! NSString
        DispatchQueue.main.async {
            var pluginResult:CDVPluginResult
            let path:String = (command.argument(at: 0) as! String).replacingOccurrences(of: "file://", with: "")
            let fileUrl:URL = URL.init(fileURLWithPath: path)
            if(!FileManager.default.fileExists(atPath: fileUrl.path)){
                let res:NSDictionary = ["status":"9", "message": "File does not exist"]
                pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: res as NSDictionary? as? [AnyHashable: Any] ?? [:] )
                self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
                return
            }
            let docController = UIDocumentInteractionController.init(url: fileUrl as URL)
            docController.delegate = self as UIDocumentInteractionControllerDelegate
            docController.uti = uti as String

            var wasOpened:Bool = false
            if(showPreview){
                wasOpened = docController.presentPreview(animated: false)
            }else{
                let rect:CGRect = CGRect.init(x: 0, y: 0, width: 150, height: 150)
                wasOpened = docController.presentOpenInMenu(from: rect, in: cont.view, animated: true)
            }
            if(wasOpened){
                pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: "")
            }else{
                let res:NSDictionary = ["status":"9", "message": "Could not handle UTI"]
                pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: res as NSDictionary? as? [AnyHashable: Any] ?? [:] )
            }
            self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
        }


    }
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self.cdvViewController!
    }
    // MARK: html2pdf
    func createPDF(_ command: CDVInvokedUrlCommand) -> Void {
        let html:String = command.argument(at: 0) as! String
        self.filePath = (command.argument(at: 1) as? NSString)?.expandingTildeInPath
        self.callbackId = command.callbackId
        let wwwFilePath:String = Bundle.main.path(forResource: "www", ofType: nil)!
        let baseURL:URL = URL.init(fileURLWithPath: wwwFilePath) as URL
        let webPage:UIWebView = UIWebView()
        webPage.delegate = self
        webPage.frame = CGRect.init(x: 0, y: 0, width: 1, height: 1)
        webPage.alpha = 0.0
        self.webView.superview?.addSubview(webPage)
        webPage.loadHTMLString(html, baseURL: baseURL)
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        let renderer:UIPrintPageRenderer = UIPrintPageRenderer()
        renderer.addPrintFormatter(webView.viewPrintFormatter(), startingAtPageAt: 0)
        let printableRect:CGRect = CGRect.init(x: pageMargins.left, y: pageMargins.top, width: kPaperSizeA4.width - pageMargins.left - pageMargins.right, height: kPaperSizeA4.height - pageMargins.top - pageMargins.bottom)
        let paperRect:CGRect = CGRect.init(x: 0, y: 0, width: kPaperSizeA4.width, height: kPaperSizeA4.height)
        renderer.setValue(paperRect, forKey: "paperRect")
        renderer.setValue(printableRect, forKey: "printableRect")
        if(filePath != nil){
            printToPDF(renderer: renderer).write(toFile: filePath!, atomically: true)
        }
        //remove webPage
        webView.stopLoading()
        webView.removeFromSuperview()
        //webView.delegate = nil
        let pluginResult:CDVPluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: "HTMLtoPDF did succeed " + filePath!)

        self.commandDelegate.send(pluginResult, callbackId: self.callbackId)
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        let pluginResult:CDVPluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: error.localizedDescription)

        self.commandDelegate.send(pluginResult, callbackId: self.callbackId)
    }
    func printToPDF(renderer: UIPrintPageRenderer) -> NSMutableData {
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, renderer.value(forKey: "paperRect") as! CGRect, nil)
        renderer.prepare(forDrawingPages: NSRange.init(location: 0, length: renderer.numberOfPages))
        let bounds:CGRect = UIGraphicsGetPDFContextBounds()
        for index in 1...renderer.numberOfPages {
            UIGraphicsBeginPDFPage()
            renderer.drawPage(at: index - 1, in: bounds)
        }
        UIGraphicsEndPDFContext()
        return pdfData
    }
}

extension URL {

    var uti: String {
        return (try? self.resourceValues(forKeys: [.typeIdentifierKey]))?.typeIdentifier ?? "public.data"
    }

}



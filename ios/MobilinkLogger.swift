//
//  Msc MobilinkLogger.swift
//  Mobilink Logger
//
//  Created by Ganesh Subramanian on 27/09/16.
//
//

import Foundation
import SystemConfiguration
import QuartzCore
import MobileCoreServices

@objc public enum logLevel:Int {
    case error, info, verbose
    func description() -> String {
        switch self {
        case .error:
            return "ERROR"
        case .info:
            return "INFO"
        case .verbose:
            return "VERBOSE"
        }
    }
}
fileprivate var loggerLogLevel:logLevel? = nil
fileprivate var loggerLogFolder:String = ""
fileprivate var loggerLogFileName:String = ""
fileprivate var loggerTransactionLogFileName:String = ""
fileprivate var fileHandle:FileHandle? = nil
fileprivate var fileTransactionHandle:FileHandle? = nil
fileprivate let serialQueue = DispatchQueue(label: "com.mscmobile.sam.fatclient")
fileprivate let dateFormatter = DateFormatter()
fileprivate var loggerTransactionLog:Bool = false

@objc public class MobilinkLogger:NSObject {
    
    public var deviceType: Device.Model {
        return Device.type
    }
    
    public var deviceOS: String {
        return Device.os
    }
    
    func setupLogger( logFolder: String, logFileName: String, logLevel:logLevel, transactionLogFileName: String) -> Void {
        loggerLogFileName = logFileName
        loggerLogFolder = logFolder
        loggerLogLevel = logLevel
        loggerTransactionLogFileName = transactionLogFileName
        let dir = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.allDomainsMask, true).first
        let fileURL = NSURL(fileURLWithPath: dir!).appendingPathComponent(loggerLogFolder)?.appendingPathComponent(loggerLogFileName)
        fileHandle = try? FileHandle(forWritingTo: fileURL!)
        let fileTransactionURL = NSURL(fileURLWithPath: dir!).appendingPathComponent(loggerLogFolder)?.appendingPathComponent(loggerTransactionLogFileName)
        fileTransactionHandle = try? FileHandle(forWritingTo: fileTransactionURL!)
        
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss "
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        
    }
    func checkAndCreateLogFolder(logFolder:String) -> Bool {
        //Check if log files exist - if not then create it
        var result:Bool = true
        var isDir: ObjCBool = false
        let dir = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.allDomainsMask, true).first
        let path:String = (dir?.appending("/" + logFolder))!
        print(path)
        do {
            if(!FileManager.default.fileExists(atPath: path, isDirectory: &isDir)){
                if(!isDir.boolValue){
                    try FileManager.default.createDirectory(atPath: path, withIntermediateDirectories: false, attributes: nil)
                }
            }
        } catch _ as NSError {
            result = false
        }
        return result
    }
    func isLogLevelOk(logLevel:logLevel) -> Bool {
        return logLevel.rawValue <= (loggerLogLevel?.rawValue)!
    }
    func timestampAndLogLevel(logLevel:logLevel) -> String {
        return dateFormatter.string(from: Date()) + logLevel.description().padding(toLength: 9, withPad: " ", startingAt: 0) + ":"
    }
    func log(logLevel:logLevel, message:String?=nil, messageStructure:[NSMutableDictionary]?=nil, isTransaction:Bool? = false) -> Void{
        serialQueue.async  {
            //Calling the logger asynchronously
            if(self.isLogLevelOk(logLevel: logLevel)){
                if(message != nil || messageStructure != nil){
                    //let logMessage = self.timestampAndLogLevel(logLevel: logLevel) + message!
                    self.logAsync(logLevel: logLevel, message:message , messageStructure: messageStructure)
                }
            }
            loggerTransactionLog = true
            if isTransaction! && loggerTransactionLog {
                if(message != nil || messageStructure != nil){
                    //let logMessage = self.timestampAndLogLevel(logLevel: logLevel) + message!
                    self.logAsync(logLevel: logLevel, message:message , messageStructure: messageStructure, isTransaction: true)
                }
            }
            serialQueue.async(execute: {
                // any callback if required
            })
        }
    }
    @objc func log(logLevel:logLevel, message:String, isTransaction:Bool) -> Void{
        serialQueue.async  {
            //Calling the logger asynchronously
            if(self.isLogLevelOk(logLevel: logLevel)){
                self.logAsync(logLevel: logLevel, message:message)
            }
            loggerTransactionLog = true
            if isTransaction && loggerTransactionLog {
                self.logAsync(logLevel: logLevel, message:message , isTransaction: true)
            }
        }
    }
    func logAsync(logLevel:logLevel, message:String?=nil, messageStructure:[NSMutableDictionary]?=nil, isTransaction:Bool? = false) -> Void{
        //Calling the logger in dispatch mode
        // log data to file
        do{
            let dir = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.allDomainsMask, true).first
            let handle = ( isTransaction! ? fileTransactionHandle : fileHandle)
            
            if (handle != nil) {
                handle?.seekToEndOfFile()
                let data = ("\n" + self.convertToString(logLevel: logLevel,message: message, messageStructure: messageStructure)).data(using: String.Encoding.utf8)!
                handle?.write(data)
            }
            else {
                if (!isTransaction!) {
                    let fileURL = NSURL(fileURLWithPath: dir!).appendingPathComponent(loggerLogFolder)?.appendingPathComponent(loggerLogFileName)
                    try self.convertToString(logLevel: logLevel,message: message, messageStructure: messageStructure).write(to: fileURL!, atomically: false, encoding: String.Encoding.utf8)
                    fileHandle = try? FileHandle(forWritingTo: fileURL!)
                }else{
                    let fileURL = NSURL(fileURLWithPath: dir!).appendingPathComponent(loggerLogFolder)?.appendingPathComponent(loggerTransactionLogFileName)
                    try self.convertToString(logLevel: logLevel,message: message, messageStructure: messageStructure).write(to: fileURL!, atomically: false, encoding: String.Encoding.utf8)
                    fileTransactionHandle = try? FileHandle(forWritingTo: fileURL!)
                }
                
            }
        }
        catch let writeError as NSError {
            //logging failed?! return result = false
            print(writeError)
        }
    }
    func convertToString(logLevel:logLevel, message:String?=nil, messageStructure:[NSMutableDictionary]?=nil) -> String {
        do{
            if(message != nil){
                return self.timestampAndLogLevel(logLevel: logLevel) + message!
            }
            if(messageStructure != nil){
                let data = try JSONSerialization.data(withJSONObject: messageStructure!, options: JSONSerialization.WritingOptions.init(rawValue: 0))
                return self.timestampAndLogLevel(logLevel: logLevel) + String.init(data:data, encoding: String.Encoding.utf8)!
            }
        }catch _ as NSError {
            //serialization failed?! return error statement
            return "JSON Serialization failed"
        }
        return ""
    }
    func setLogLevel(logLevel:logLevel, transactionLog: Bool) -> Void {
        serialQueue.async  {
            loggerLogLevel = logLevel
            loggerTransactionLog = transactionLog
        }
    }
    
    func getLogLevel() -> logLevel{
        return loggerLogLevel!
    }
    func resetLogFiles() -> Bool {
        do{
            let dir = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.allDomainsMask, true).first
            fileHandle?.closeFile()
            let fileURL = NSURL(fileURLWithPath: dir!).appendingPathComponent(loggerLogFolder)?.appendingPathComponent(loggerLogFileName)
            try FileManager.default.removeItem(at: fileURL!)
            fileHandle = nil
            fileTransactionHandle?.closeFile()
            let fileTransactionURL = NSURL(fileURLWithPath: dir!).appendingPathComponent(loggerLogFolder)?.appendingPathComponent(loggerTransactionLogFileName)
            try FileManager.default.removeItem(at: fileTransactionURL!)
            fileTransactionHandle = nil
            
            self.log(logLevel: logLevel.info, message: "########################################################################")
            self.log(logLevel: logLevel.info, message: "Logger reset, initialized in GMT Timezone")
        }
        catch _ as NSError {
            //serialization failed?! return error statement
            return false
        }
        return true
    }
    
    func removeLogFiles() -> Bool {
        guard let dir = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.allDomainsMask, true).first else {
            return false
        }
        
        guard let fileTransactionURL = NSURL(fileURLWithPath: dir).appendingPathComponent(loggerLogFolder) else {
            return false
        }
        
        do {
            try FileManager.default.removeItem(at: fileTransactionURL)
        } catch _ as NSError {
            //serialization failed?! return error statement
            return false
        }
        
        return true
    }
}


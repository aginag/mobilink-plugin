//
//  GeoTrackingService.swift
//  SAM
//
//  Created by Patrick Valenta on 27.01.20.
//

import Foundation
import CoreLocation

public class GeoTrackingService: NSObject {
    private static let TRACKING_PUBLICATION: String = "LIVE_TRACKING"
    /// In seconds
    private static let DEFAULT_SYNC_INTERVAL: Double = 3 * 60

    private var customInterval: Double?
    private var timer: Timer?
    private var user: User?
    private var commandDelegate: CDVCommandDelegate?
    private var command: CDVInvokedUrlCommand?

    private let locationManager = SAMLocationManager()

    /// Last received location.
    public var lastLocation: CLLocation?

    /// Return the current authorization state.
    public static var authorizationState: State {
        switch (CLLocationManager.locationServicesEnabled(), CLLocationManager.authorizationStatus()) {
        case (false,_):
            return .disabled
        case (true, .notDetermined):
            return .undetermined
        case (true, .denied):
            return .denied
        case (true, .restricted):
            return .restricted
        default:
            return .available
        }
    }

    internal init(withCommandDelegate delegate: CDVCommandDelegate, andCommand cdvCommand: CDVInvokedUrlCommand) {
        super.init()
        locationManager.delegate = self
        commandDelegate = delegate
        command = cdvCommand

        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.distanceFilter = CLLocationDistance.init(10)
    }

    public func start(withInterval interval: String, andUser newUser: User) throws {
        if let castInterval = Double(interval) {
            customInterval = castInterval * 60
        } else {
            customInterval = GeoTrackingService.DEFAULT_SYNC_INTERVAL
        }

        logger.log(logLevel: logLevel.info, message: "GeoTrackingService - start - Trying to start live tracking with interval of \(customInterval ?? 0) second(s)")

        user = newUser

        guard CLLocationManager.locationServicesEnabled() else {
            throw LocationError.serviceNotEnabled
        }

        guard GeoTrackingService.authorizationState != .disabled && GeoTrackingService.authorizationState != .denied && GeoTrackingService.authorizationState != .restricted else {
            throw LocationError.permissionNotGranted
        }

        if (GeoTrackingService.authorizationState == .undetermined) {
            locationManager.requestWhenInUseAuthorization()
            return;
        }

        if (!locationManager.running) {
            startWithPermission()
        }
    }

    private func startWithPermission() {
        logger.log(logLevel: logLevel.info, message: "GeoTrackingService - startWithPermission - Starting live tracking with permission")
        locationManager.startUpdatingLocation()

        if let interval = customInterval {
            timer = Timer.scheduledTimer(timeInterval: interval, target: self, selector: #selector(onTimerTick), userInfo: nil, repeats: true)
        }

        if let callbackId = command?.callbackId {
            commandDelegate?.send(GeoTrackingServiceResults.serviceStarted(), callbackId: callbackId)
        }
    }

    public func stop() {
        logger.log(logLevel: logLevel.info, message: "GeoTrackingService - stop - Stopping location tracking")
        locationManager.stopUpdatingLocation()
        UltraLiteDB.sharedInstance().cancelSync()
        timer?.invalidate()
        user = nil
    }

    @objc private func onTimerTick() {
        logger.log(logLevel: logLevel.info, message: "GeoTrackingService - onTimerTick - Starting new synchronization")
        handlePreDbSync()
        syncDbAsync()
    }

    private func insertNewLocation(_ mostRecentLocation: CLLocation) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMddHHmmss"

        let latitude: String = mostRecentLocation.coordinate.latitude.description
        let longitude: String = mostRecentLocation.coordinate.longitude.description
        let currentDateTime: String = dateFormatter.string(from: Date())


        let deleteOldLocationStmt: String = "DELETE FROM SAM_LT_COORD"
        let newLocationStmt: String = "INSERT INTO SAM_LT_COORD (LATITUDE, LONGITUDE, USER_NAME, CREATED_ON, ACTION_FLAG) VALUES('\(latitude)', '\(longitude)', '\(user?.name ?? "")', '\(currentDateTime)', 'N')"

        logger.log(logLevel: logLevel.info, message: "GeoTrackingService - insertLocation - Insert new location: \(newLocationStmt)")

        let resultDeleteOldStmt: String = UltraLiteDB.sharedInstance().executeUpdate(deleteOldLocationStmt)
        let resultNewLocationStmt: String = UltraLiteDB.sharedInstance().executeUpdate(newLocationStmt)

        if (resultDeleteOldStmt != "1") {
            logger.log(logLevel: logLevel.error, message: "GeoTrackingService - insertNewLocation - resultDeleteOldStmt - Query failed with error")
        }

        if (resultNewLocationStmt != "1") {
            logger.log(logLevel: logLevel.error, message: "GeoTrackingService - insertNewLocation - resultNewLocationStmt - Query failed with error")
        }
    }

    private func handlePreDbSync() {
        let result: NSArray = UltraLiteDB.sharedInstance().executeQueryInternal("SELECT * FROM SAM_LT_COORD WHERE ACTION_FLAG != 'O'")

        if (result == nil) {
            logger.log(logLevel: logLevel.error, message: "GeoTrackingService - handlePreDbSync - Query failed with error")
            return
        }

        if (result.count > 0 || lastLocation == nil) {
            return
        }

        if let lastLocation = lastLocation {
            insertNewLocation(lastLocation)
        }
    }

    private func syncDbAsync() {
        guard let delegate = commandDelegate else {
            return
        }

        guard let user = user else {
            return
        }

        delegate.run( inBackground:  {
            let result:Int32 = UltraLiteDB.sharedInstance().syncUser(user.name, withPassword: user.password, callbackId: nil, andSyncMode: "S", andPingMode: false, andPublication: GeoTrackingService.TRACKING_PUBLICATION)
            if (result == 1000) {
                logger.log(logLevel: logLevel.info, message: "GeoTrackingService - syncDbAsync - Sync successful")
            } else {
                logger.log(logLevel: logLevel.error, message: "GeoTrackingService - syncDbAsync - Sync failed with code -> \(result)")
            }

            self.checkForNewObjects()
        })
    }

    private func checkForNewObjects() {
        let result: NSArray = UltraLiteDB.sharedInstance().executeQueryInternal("SELECT * FROM SAM_USER_OBJECT_TRACKING")

        if (result == nil) {
            logger.log(logLevel: logLevel.error, message: "GeoTrackingService - checkForNewObjects - Query failed with error")
            return
        }

        if (result.count == 0) {
            return
        }

        if let callbackId = command?.callbackId {
            commandDelegate?.send(GeoTrackingServiceResults.newObjects(objects: result), callbackId: callbackId)
        }
    }
}

extension GeoTrackingService: CLLocationManagerDelegate {
    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status != .denied && status != .restricted && status != .notDetermined else {
            locationManager.stopUpdatingLocation()
            return
        }

        if (!locationManager.running) {
            startWithPermission()
        }
    }

    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let mostRecentLocation = locations.last else {
            return
        }

        logger.log(logLevel: logLevel.info, message: "GeoTrackingService - OnPositionChanged - New Location received")

        if (UltraLiteDB.sharedInstance()?.isSyncInProgress ?? false) {
            logger.log(logLevel: logLevel.info, message: "GeoTrackingService - OnPositionChanged - Another Sync currently in progress, not writing it to the database. Caching the location.")
            lastLocation = mostRecentLocation
            return;
        }

        insertNewLocation(mostRecentLocation)
        lastLocation = mostRecentLocation
    }

    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        logger.log(logLevel: logLevel.error, message: "GeoTrackingService - didFailWithError - Failed with following error: \(error)")
        stop()
    }
}

public extension GeoTrackingService {
    enum State: CustomStringConvertible {
        case available
        case undetermined
        case denied
        case restricted
        case disabled

        public var description: String {
            switch self {
            case .available:    return "available"
            case .undetermined: return "undetermined"
            case .denied:       return "denied"
            case .restricted:   return "restricted"
            case .disabled:     return "disabled"
            }
        }
    }

    struct User {
        var name: String
        var password: String
    }

    enum LocationError: Error {
        case serviceNotEnabled
        case permissionNotGranted
    }
}

fileprivate class GeoTrackingServiceResults {
    public static func serviceStarted() -> CDVPluginResult {
        let event = ["EVENT": 1, "STATUS": true] as [AnyHashable : Any]

        let pluginResult: CDVPluginResult = CDVPluginResult(status: .ok, messageAs: event)
        pluginResult.setKeepCallbackAs(true)
        return pluginResult
    }

    public static func newObjects(objects: NSArray) -> CDVPluginResult {
        let event = ["EVENT": 3, "STATUS": true, "OBJECTS": objects] as [AnyHashable : Any]

        let pluginResult: CDVPluginResult = CDVPluginResult(status: .ok, messageAs: event)
        pluginResult.setKeepCallbackAs(true)
        return pluginResult
    }
}

fileprivate class SAMLocationManager: CLLocationManager {
    var running: Bool

    override init() {
        running = false
        super.init()
    }

    override func startUpdatingLocation() {
        super.startUpdatingLocation()
        running = true
    }

    override func stopUpdatingLocation() {
        super.stopUpdatingLocation()
        running = false
    }

}

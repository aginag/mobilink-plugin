//
//  UltraLiteDB.m
//
//
//  Created by Alexander Ilg on 03/04/14.
//
//

#import "UltraLiteDB.h"
#import "ulcpp.h"
#import "sqlca.h"
#import "ulglobal.h"
#import "mlfiletransfer.h"
#import <Foundation/NSJSONSerialization.h>
#import "SAM-Swift.h"

@implementation UltraLiteDB
NSMutableString* dbFileFullName = [[NSMutableString alloc] initWithString: @"sam.udb"];
NSMutableString * connParms;
NSString* user_name;
NSString * _protocol;
NSString * host_name;
NSString * port_number;
NSString * _urlSuffix;
NSString * version;
NSString * publication;
NSString * subscription_name;
NSString* mainObjectTable;
NSString* mainObjectUUID;
NSString* newObjectUUID;
NSString* lastError;
MobilinkLogger* logger;
NSMutableArray* dbConnections;

typedef void (*fileTransferObserver)( ml_file_transfer_status * status);
typedef void (*syncObserver)( ul_sync_status * status);

@synthesize commandDelegate;
@synthesize fileTransfersInfo;
@synthesize syncInfo;
@synthesize isSyncInProgress;
static UltraLiteDB * sharedInstance = nil;

void * connection; // void * to avoid C++ vs Obj-C errors

+ (UltraLiteDB *)sharedInstance {

    // Create a new instance if none was created yet.
    if (sharedInstance == nil) {
        sharedInstance = [[self alloc] init];
    }

    // Otherwise, just return the existing instance,
    return sharedInstance;
}

- (id)init{
    self = [super init];
    fileTransfersInfo = [[NSMutableDictionary alloc] init];
    syncInfo = [[NSMutableDictionary alloc] init];
    dbConnections = [[NSMutableArray alloc] init];
    isSyncInProgress = false;
    return self;
}

- (void) setLogger:(MobilinkLogger*) Logger{
    logger = Logger;
}
+ (void)initialize {
    NSLog(@"Initializing the Database.");


    if( !ULDatabaseManager::Init() ) {
        NSLog(@"cannot initalize DB.");
        return;
    }

    // Use ULDatabaseManager.Fini() when terminating the app.

}
- (void) defineMLInfo:(NSString*)hostName withPort:(NSString*) portNumber andSubscription:(NSString*) subscriptionName andVersion:(NSString*) versionName andPublication:(NSString*) publicationName andDBName: (NSString*) dbName andProtocol:(NSString *)protocol andUrlSuffix: (NSString*) urlSuffix {
    if ([protocol caseInsensitiveCompare:@"http"] == NSOrderedSame) {
        _protocol = @"HTTP";
        ULDatabaseManager::EnableHttpSynchronization();
    } else if ([protocol caseInsensitiveCompare:@"https"] == NSOrderedSame) {
        _protocol = @"HTTPS";
        ULDatabaseManager::EnableHttpsSynchronization();
    } else if ([protocol caseInsensitiveCompare:@"tcpip"] == NSOrderedSame) {
        _protocol = @"TCPIP";
        ULDatabaseManager::EnableTcpipSynchronization();
    } else {
        _protocol = @"HTTP";
        ULDatabaseManager::EnableHttpSynchronization();
    }

    host_name = [NSString stringWithFormat:@"host=%@",hostName];
    port_number = [NSString stringWithString:portNumber];
    subscription_name = [NSString stringWithString:subscriptionName];
    publication = [NSString stringWithString:publicationName];
    version = [NSString stringWithString:versionName];

    if (urlSuffix != nil && urlSuffix.length > 0) {
        _urlSuffix = [NSString stringWithFormat:@";url_suffix=%@",urlSuffix];
    } else {
        _urlSuffix = [NSString stringWithFormat:@""];
    }

    [dbFileFullName setString:dbName];
}
- (void)setUserName:(NSString*)userName{
    user_name = [NSString stringWithString:userName];
    if (dbConnections.count > 0) {
        [self closeConnection];
    }
    //[self getConnections];
}
- (bool) getConnections: (NSString*)encryptionKey {
    //Here we get all possible connections and put it in an array
    NSString *writableDBPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:user_name];
    NSString *writableUserDBPath = [writableDBPath stringByAppendingPathComponent:dbFileFullName];
    if(encryptionKey){
        connParms = [NSMutableString stringWithString:[NSString stringWithFormat:@"DBF=%@;cache_size=1m;uid='';pwd='';DBKEY='%@'", writableUserDBPath, encryptionKey]];
    }else{
        connParms = [NSMutableString stringWithString:[NSString stringWithFormat:@"DBF=%@;cache_size=1m;uid='';pwd=''", writableUserDBPath]];
    }

    ULConnection* newConnection;
    ULError error;

    for (int i = 0; i< 14; i ++) {
        newConnection = (ULConnection*) ULDatabaseManager::OpenConnection([connParms UTF8String], &error);
        [dbConnections addObject:[NSValue valueWithPointer:newConnection]];
        if (!newConnection) {
            NSString* message = [NSString stringWithFormat:@"Couldn't connect to database, SQL CODE: %d", error.GetSQLCode()];
            [logger logWithLogLevel:logLevelError message:message isTransaction:false];
            return false;
        }
    }
    return true;
}
/*
 Message to acquire a DB connection. Please note that when you get a connection, always release it at the end of the DB activity
 */
- (void *) getNewConnection {
    ULConnection* newConnection;
    @synchronized (dbConnections) {
        newConnection = (ULConnection*)[[dbConnections objectAtIndex:0] pointerValue];
        [dbConnections removeObjectAtIndex:0];
    }
    return newConnection;
}
/*
 Message to release a DB connection. Please call this method to release the connection at the end of the DB activity
 */
- (void) releaseConnection: (void *)conn {
    @synchronized (dbConnections) {
        [dbConnections addObject:[NSValue valueWithPointer:(ULConnection*)conn]];
    }
}
- (void) startConnection:(NSString*)userName{
    NSString *writableDBPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:userName];
    NSString *writableUserDBPath = [writableDBPath stringByAppendingPathComponent:dbFileFullName];
    connParms = [NSMutableString stringWithString:[NSString stringWithFormat:@"DBF=%@;cache_size=1m;uid='';pwd=''", writableUserDBPath]];

    ULError error;
    connection = ULDatabaseManager::OpenConnection([connParms UTF8String], &error);
    if (!connection) {
        NSLog(@"Couldn't connect to database, SQL CODE: %ld", error.GetSQLCode());
    }
}
- (void) addNewFileTransferInfo: (NSMutableDictionary*)fileTransfer {
    @synchronized (fileTransfersInfo) {
        [fileTransfersInfo setObject:fileTransfer forKey:[fileTransfer objectForKey:@"fileId"]];
    }
}
- (NSMutableDictionary*) readFileTransferInfo: (NSString*)fileId {
    @synchronized (fileTransfersInfo) {
        return [fileTransfersInfo objectForKey:fileId];
    }
}
- (void) removeFileTransferInfo: (NSString*)fileId {
    @synchronized (fileTransfersInfo) {
        [fileTransfersInfo removeObjectForKey:fileId];
    }
}


+ (void)finalize {
    NSLog(@"Finalizing DB Manager.");
    ULDatabaseManager::Fini();
}
- (void) closeConnection {
    ULConnection* newConnection;
    int limit = dbConnections.count;
    @synchronized (dbConnections) {
        for (int i = 0; i < limit; i++) {
            newConnection = (ULConnection*)[[dbConnections objectAtIndex:i] pointerValue];
            newConnection->Close();

        }
        [dbConnections removeAllObjects];
    }
}


- (NSString *) executeQuery:(NSString *) sqlStatement {
    ULConnection * conn = (ULConnection*)[self getNewConnection];
    ULPreparedStatement *ps = conn->PrepareStatement([sqlStatement UTF8String]);
    if (ps) {
        ULResultSet *rs = ps->ExecuteQuery();
        NSString *json = [self convertResultSetToJSON:rs];

        rs->Close();
        ps->Close();
        [self releaseConnection:conn];
        return json;

    } else {
        char errorMsg[400];
        conn->GetLastError()->GetString(errorMsg, 400);
        [logger logWithLogLevel:logLevelError message:[NSString stringWithFormat:@"executeQuery - ERROR - Statement: %s", [sqlStatement UTF8String]] isTransaction:false];
        [logger logWithLogLevel:logLevelError message:[NSString stringWithFormat:@"executeQuery - ERROR: %@", [NSString stringWithUTF8String:errorMsg]] isTransaction:false];

        conn->Rollback();
        [self releaseConnection:conn];
        NSLog(@"Query failed");
    }
    //[self closeConnection];
    return nil;
}
- (NSMutableArray *) executeQueryInternal:(NSString *) sqlStatement {
    ULConnection * conn = (ULConnection*)[self getNewConnection];
    ULPreparedStatement *ps = conn->PrepareStatement([sqlStatement UTF8String]);
    if (ps) {
        ULResultSet *rs = ps->ExecuteQuery();
        NSMutableArray *records = [self convertResultSetToNSArray:rs];

        rs->Close();
        ps->Close();
        [self releaseConnection:conn];
        return records;

    } else {
        char errorMsg[400];
        conn->GetLastError()->GetString(errorMsg, 400);
        [logger logWithLogLevel:logLevelError message:[NSString stringWithFormat:@"executeQueryInternal - ERROR - Statement: %s", [sqlStatement UTF8String]] isTransaction:false];
        [logger logWithLogLevel:logLevelError message:[NSString stringWithFormat:@"executeQueryInternal - ERROR: %@", [NSString stringWithUTF8String:errorMsg]] isTransaction:false];

        lastError = [NSString stringWithUTF8String:errorMsg];

        conn->Rollback();
        [self releaseConnection:conn];
        NSLog(@"Query failed: %@", sqlStatement);
    }
    //[self closeConnection];
    return nil;
}

- (NSString *) executeUpdate:(NSString *) sqlStatement {
    ULConnection * conn = (ULConnection*)[self getNewConnection];
    ULPreparedStatement *ps = conn->PrepareStatement([sqlStatement UTF8String]);

    if (ps) {
        bool success;
        success = ps->ExecuteStatement();
        ps->Close();
        conn->Commit();
        [self releaseConnection:conn];
        return @"1";

    } else {
        char errorMsg[400];
        conn->GetLastError()->GetString(errorMsg, 400);
        [logger logWithLogLevel:logLevelError message:[NSString stringWithFormat:@"executeUpdate - ERROR - Statement: %s", [sqlStatement UTF8String]] isTransaction:false];
        [logger logWithLogLevel:logLevelError message:[NSString stringWithFormat:@"executeUpdate - ERROR: %@", [NSString stringWithUTF8String:errorMsg]] isTransaction:false];

        conn->Rollback();
        [self releaseConnection:conn];
        NSLog(@"Update failed");
        return @"0";
    }
}

- (bool) executeUpdatesSequentially:(NSArray *) sqlStatements {
    ULConnection * conn = (ULConnection*)[self getNewConnection];
    ULPreparedStatement *ps = nil;
    bool success = true;

    [logger logWithLogLevel:logLevelInfo message:[NSString stringWithFormat:@"Execute updates sequentially - START"] isTransaction:false];

    for (NSInteger i = 0; i < [sqlStatements count]; i ++) {
        [logger logWithLogLevel:logLevelInfo message:[NSString stringWithFormat:@"Execute updates sequentially: %s", [sqlStatements[i] UTF8String]] isTransaction:false];
        ps = conn->PrepareStatement([sqlStatements[i] UTF8String]);

        if (!ps) {
            success = false;
            char errorMsg[400];
            conn->GetLastError()->GetString(errorMsg, 400);
            NSLog(@"%@", [NSString stringWithUTF8String:errorMsg]);
            [logger logWithLogLevel:logLevelError message:[NSString stringWithFormat:@"Execute updates sequentially - ERROR: %@", [NSString stringWithUTF8String:errorMsg]] isTransaction:false];
            break;
        }

        success = ps->ExecuteStatement();

        if(!success || !ps){
            char errorMsg[400];
            conn->GetLastError()->GetString(errorMsg, 400);
            NSLog(@"%@", [NSString stringWithUTF8String:errorMsg]);
            [logger logWithLogLevel:logLevelError message:[NSString stringWithFormat:@"Execute updates sequentially - ERROR: %@", [NSString stringWithUTF8String:errorMsg]] isTransaction:false];
            break;
        }

        ps->Close();
        ps = nil;
    }

    if(success){
        [logger logWithLogLevel:logLevelInfo message:[NSString stringWithFormat:@"Execute updates sequentially - SUCCESSFUL"] isTransaction:false];
        conn->Commit();
        [self releaseConnection:conn];
    }

    else{
        [logger logWithLogLevel:logLevelInfo message:[NSString stringWithFormat:@"Execute updates sequentially - UNSUCCESSFUL - Rolling back"] isTransaction:false];
        conn->Rollback();
        [self releaseConnection:conn];
    }

    return success;
}

- (bool) executeStatementsAndCommit:(NSArray *) sqlStatements {
    ULConnection * conn = (ULConnection*)[self getNewConnection];
    bool success = true;
    newObjectUUID = [NSString stringWithFormat:@""];
    for (NSInteger i = 0; i < [sqlStatements count]; i ++) {
        NSLog(@"Update statement for table: %s", [[sqlStatements[i] objectForKey:@"tableName" ] UTF8String]);
        bool mainTable = [[sqlStatements[i] objectForKey:@"mainTable"] boolValue];

        success = [self executeUpdateInsert:[sqlStatements[i] objectForKey:@"type" ] mainTable:mainTable tableName:[sqlStatements[i] objectForKey:@"tableName" ] whereConditions:[sqlStatements[i] objectForKey:@"whereConditions" ] values:[sqlStatements[i] objectForKey:@"values" ] replacementValues:[sqlStatements[i] objectForKey:@"replacementValues" ] mainObjectValues:[sqlStatements[i] objectForKey:@"mainObjectValues" ] conn:conn];
        if(!success){
            break;
        }
    }
    if(success){
        conn->Commit();
        [self releaseConnection:conn];
    }
    else{
        char errorMsg[400];
        conn->GetLastError()->GetString(errorMsg, 400);
        [logger logWithLogLevel:logLevelError message:[NSString stringWithFormat:@"executeStatementsAndCommit - ERROR: %@", [NSString stringWithUTF8String:errorMsg]] isTransaction:false];

        conn->Rollback();
        [self releaseConnection:conn];
    }
    return success;
}
- (NSString*) getLastError{
    return lastError;
}
- (NSString*) getNewObjectUUID{
    return newObjectUUID;
}

- (BOOL) executeUpdateInsert:(NSString *)type mainTable:(bool) mainTable tableName:(NSString *) tableName whereConditions:(NSString *) whereConditions values:(NSArray *) values replacementValues:(NSArray *) replacementValues mainObjectValues:(NSArray *) mainObjectValues conn:(ULConnection*)conn{
    BOOL result = true;
    NSMutableString *sql = nil;
    //ULConnection * conn = ((ULConnection*)connection);
    ULPreparedStatement *ps = nil;
    ULError error;
    NSString* uuid;
    //update case
    if([type  isEqual: @"update"]){
        sql = [NSMutableString stringWithFormat:@"UPDATE %@ SET ", tableName];
        for(int i = 0; i< [values count]; i++){
            [sql appendString: values[i][0]];
            [sql appendString: @"=?, "];
        }
        //remove last ','
        [sql deleteCharactersInRange:NSMakeRange([sql length] -2, 2)];
        [sql appendString:@" WHERE "];
        [sql appendString: whereConditions];
    }
    //update case
    else if([type  isEqual: @"delete"]){
        sql = [NSMutableString stringWithFormat:@"DELETE FROM %@", tableName];
        [sql appendString:@" WHERE "];
        [sql appendString: whereConditions];
    }
    //insert case
    else{
        //Fetch new UUID
        NSMutableArray* uuid_result = [self executeQueryInternal:@"SELECT NEWID() AS new_uuid"];
        uuid = [[uuid_result objectAtIndex:0] objectForKey:@"new_uuid"];
        newObjectUUID = [NSString stringWithString:uuid];
        sql = [NSMutableString stringWithFormat:@"INSERT INTO %@ (MOBILE_ID, ", tableName];
        NSMutableString *valuesString = [NSMutableString stringWithFormat:@""];
        for(int i = 0; i< [replacementValues count]; i++){
            [sql appendString: replacementValues[i][0]];
            [sql appendString: @", "];
            [valuesString appendString:@"?, "];
        }
        for(int i = 0; i< [values count]; i++){
            [sql appendString: values[i][0]];
            [sql appendString: @", "];
            [valuesString appendString:@"?, "];
        }
        [sql deleteCharactersInRange:NSMakeRange([sql length] -2, 2)];
        [valuesString deleteCharactersInRange:NSMakeRange([valuesString length] -2, 2)];

        [sql appendString:[NSMutableString stringWithFormat:@") VALUES( '%@', ", uuid]];
        [sql appendString:valuesString];
        [sql appendString:@")"];
        if(mainTable){
            mainObjectUUID = [NSString stringWithString:uuid];
            mainObjectTable = [NSString stringWithString:tableName];
        }
    }
    ps = conn->PrepareStatement([sql UTF8String]);
    int i = 0, j = 0;
    for(; i<[replacementValues count]; i++){
        NSString *value = @"''";
        if([replacementValues[i][1]  isEqual: @"MOBILE_ID"] || [replacementValues[i][1]  isEqual: @"mobile_id"]){
            value = [NSMutableString stringWithFormat:@"%@", uuid];
            value = [value stringByReplacingOccurrencesOfString:@"-" withString:@""];
            value = [value substringToIndex:18];
        }
        //NSString *value = values[i][1];
        //NSInteger valueLength = [value length];
        //NSLog(@"value length: %lu", (unsigned long)[value length]);
        const char *valueAsChar = [value UTF8String];
        long long int charLength = strlen( valueAsChar );
        result = ps->SetParameterString(i+1, valueAsChar);
        if(!result){
            result = ps->AppendParameterStringChunk(i+1,  valueAsChar, charLength);
        }
        if(!result){
            break;
        }
    }
    for(; j< [values count]; j++){
        NSString *value = values[j][1];
        //NSInteger valueLength = [value length];
        //NSLog(@"value length: %lu", (unsigned long)[value length]);
        const char *valueAsChar = [value UTF8String];
        long long int charLength = strlen( valueAsChar );
        result = ps->SetParameterString(i+j+1, valueAsChar);
        if(!result){
            result = ps->AppendParameterStringChunk(i+j+1,  valueAsChar, charLength);
        }
        if(!result){
            result = ps->SetParameterNull(i+j+1);
        }
        if(!result){
            break;
        }
    }
    if(result){
        result = ps->ExecuteStatement();

        if(!result){
            if(conn->GetLastError()->GetSQLCode() == -193 && !mainTable){
                result = [self executeUpdateForFailedInsert:type mainTable:mainTable tableName:tableName values:values mainObjectValues:mainObjectValues conn:conn];
            }
            if(!result){
                char errorMsg[400];
                conn->GetLastError()->GetString(errorMsg, 400);
                [logger logWithLogLevel:logLevelError message:[NSString stringWithFormat:@"executeUpdateInsert - ERROR: %@", [NSString stringWithUTF8String:errorMsg]] isTransaction:false];

                NSLog(@"Update failed: %s", errorMsg);

                lastError = [NSString stringWithUTF8String:errorMsg];
            }

        }
        //        result = conn->Commit();
    }

    if(ps != nil){
        ps->Close();
    }

    return result;
}
- (BOOL) executeUpdateForFailedInsert:(NSString *)type mainTable:(bool) mainTable tableName:(NSString *) tableName values:(NSArray *) values mainObjectValues:(NSArray *) mainObjectValues conn:(ULConnection*) conn{
    BOOL result = true;
    NSMutableString *sql = nil;
    //ULConnection * conn = ((ULConnection*)connection);
    ULPreparedStatement *ps = nil;
    ULError error;
    NSString* uuid;
    NSMutableString* whereClause = [NSMutableString stringWithFormat:@""];
    //build where clause
    NSString* keyColumnsSQLString = [NSString stringWithFormat:@"SELECT SY.column_name AS column_name,ST.object_id AS TABLE_ID, SY.object_id AS COL_ID,SI.index_name AS index_name FROM syscolumn AS SY INNER JOIN systable AS ST ON SY.table_id = ST.object_id INNER JOIN sysixcol AS SX ON ST.object_id = SX.table_id AND SX.column_id = SY.object_id INNER JOIN sysindex AS SI ON SI.table_id = ST.object_id AND SX.index_id = SI.object_id WHERE SI.type='key' AND SI.index_name LIKE '%@' AND ST.table_name='%@'", @"%_sapkey", tableName];
    NSMutableArray* keyColumnsResult = [self executeQueryInternal:keyColumnsSQLString];
    sql = [NSMutableString stringWithFormat:@"UPDATE %@ SET ", tableName];
    for(int i = 0; i< [keyColumnsResult count]; i++){
        [whereClause appendString: [keyColumnsResult[i] objectForKey:@"column_name"]];
        for (int j = 0; j< [values count]; j++) {
            if([values[j][0] isEqualToString:[keyColumnsResult[i] objectForKey:@"column_name"]]){
                [whereClause appendString: [NSMutableString stringWithFormat:@"='%@' AND ", values[j][1]]];
                break;
            }
        }
    }
    [whereClause deleteCharactersInRange:NSMakeRange([whereClause length] -5, 5)];
    sql = [NSMutableString stringWithFormat:@"UPDATE %@ SET ", tableName];
    for(int i = 0; i< [values count]; i++){
        [sql appendString: values[i][0]];
        [sql appendString: @"=?, "];
    }
    //remove last ','
    [sql deleteCharactersInRange:NSMakeRange([sql length] -2, 2)];
    [sql appendString:@" WHERE "];
    [sql appendString: whereClause];


    ps = conn->PrepareStatement([sql UTF8String]);

    for(int j = 0; j< [values count]; j++){
        NSString *value = values[j][1];
        if([values[j][0]  isEqual: @"ACTION_FLAG"]){
            value = @"C";
        }

        const char *valueAsChar = [value UTF8String];
        long long int charLength = strlen( valueAsChar );
        result = ps->SetParameterString(j+1, valueAsChar);
        if(!result){
            result = ps->AppendParameterStringChunk(j+1,  valueAsChar, charLength);
        }
        if(!result){
            result = ps->SetParameterNull(j+1);
        }
        if(!result){
            break;
        }
    }
    if(result){
        result = ps->ExecuteStatement();
        if(!result){
            char errorMsg[400];
            conn->GetLastError()->GetString(errorMsg, 400);
            [logger logWithLogLevel:logLevelError message:[NSString stringWithFormat:@"executeUpdateForFailedInsert - ERROR: %@", [NSString stringWithUTF8String:errorMsg]] isTransaction:false];
            NSLog(@"Update failed: %s", errorMsg);

            lastError = [NSString stringWithUTF8String:errorMsg];
        }
    }

    if(ps != nil){
        ps->Close();
    }

    return result;
}
- (NSString *) convertResultSetToJSON:(ULResultSet *) rs {
    NSArray *records = [self convertResultSetToNSArray:rs];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:records options:0 error:&error];


    NSString *JSONString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
    return JSONString;
}
- (NSMutableArray *) convertResultSetToNSArray:(ULResultSet *) rs {
    const ULResultSetSchema & rss =  rs->GetResultSetSchema();
    int ccount = rss.GetColumnCount();
    //NSLog(@"Number of columns: %d", ccount);
    NSMutableArray *records = [NSMutableArray array];

    while(rs->Next() ) {
        int j = 1;
        NSMutableDictionary *columns = [NSMutableDictionary dictionaryWithCapacity:ccount];
        while (ccount >= j) {
            ul_column_storage_type columnType = rss.GetColumnType(j);
            const char * columnName = rss.GetColumnName(j,ul_name_type_sql_column_only);
            NSInteger dataLength = 0;


            if(columnType == UL_TYPE_BINARY || rss.GetColumnSQLType(j) == UL_SQLTYPE_LONGVARCHAR){
                dataLength = rs->GetBinaryLength(j);
                char *data = new char[dataLength + 1](); //+1: we need to null terminate the string
                long long int bytesCopied = 0;

                const NSInteger maxChunkSize = sizeof(ul_binary); //32767;
                long chunks = dataLength / maxChunkSize;
                if(dataLength % maxChunkSize != 0){
                    chunks++;
                }

                //copy data in chunks from DB
                for(int i = 0; i < chunks; i++) {

                    NSInteger  chunkSize = maxChunkSize;
                    if( i == chunks - 1){ //last chunk
                        chunkSize = dataLength - bytesCopied;
                    }
                    ul_binary *chunk = (ul_binary *)malloc(chunkSize);
                    rs->GetByteChunk(j, chunk->data, chunkSize, i * maxChunkSize);
                    chunk->len = chunkSize;

                    memcpy(data + i * maxChunkSize, chunk->data, chunkSize);
                    free(chunk);

                    bytesCopied = bytesCopied + chunkSize;
                }
                data[dataLength] = '\0'; //null terminate the string

                NSString *base64Img = [NSString stringWithCString:data encoding:NSUTF8StringEncoding];

                NSUInteger columnCount = columns.count;
                [columns setValue:base64Img forKey:[NSString stringWithUTF8String:columnName]];
                columnCount++;
                if(columnCount != columns.count){
                    NSLog(@"Inserting image in dictionary failed!!!");
                }

                NSLog(@"Binary data type");
            }else{
                dataLength = rs->GetStringLength(j) + 1;
                char buffer[ dataLength ];
                rs->GetString( j, buffer, dataLength );
                [columns setValue:[NSString stringWithUTF8String:buffer] forKey:[NSString stringWithUTF8String:columnName]];


            }

            j++;
        }
        [records addObject: columns];

    }

    return records;
}
void SyncObserverFn (ul_sync_status* status) {
    @synchronized (UltraLiteDB.sharedInstance.syncInfo) {
        if([[UltraLiteDB.sharedInstance.syncInfo objectForKey:@"cancelled"] isEqual: @"X"]){
            status->stop = true;
        }
    }
    NSMutableDictionary* transferProgress = [[NSMutableDictionary alloc] init];
    [transferProgress setValue:@"DbSync" forKey:@"progressType"];
    [transferProgress setValue:@"Started" forKey:@"status"];
    [transferProgress setValue:[NSString stringWithFormat:@"%u",status->total_download_row_count] forKey:@"TotalDownloadRowCount"];
    [transferProgress setValue:[NSString stringWithFormat:@"%u",status->current_download_row_count] forKey:@"CurrentDownloadRowCount"];
    [transferProgress setValue:[NSString stringWithFormat:@"%hu",status->flags] forKey:@"Flags"];
    [transferProgress setValue:[NSString stringWithFormat:@"%u",status->received.truncate_deletes] forKey:@"TruncateDeletes"];
    [transferProgress setValue:[NSString stringWithFormat:@"%u",status->received.ignored_deletes] forKey:@"IgnoredDeletes"];
    [transferProgress setValue:[NSString stringWithFormat:@"%u",status->received.deletes] forKey:@"RecvDeletes"];
    [transferProgress setValue:[NSString stringWithFormat:@"%u",status->received.ignored_updates] forKey:@"IgnoredUpdates"];
    [transferProgress setValue:[NSString stringWithFormat:@"%u",status->received.updates] forKey:@"RecvInserts"];
    [transferProgress setValue:[NSString stringWithFormat:@"%u",status->received.bytes] forKey:@"RecvBytes"];
    [transferProgress setValue:[NSString stringWithFormat:@"%u",status->sent.deletes] forKey:@"SentDeletes"];
    [transferProgress setValue:[NSString stringWithFormat:@"%u",status->sent.updates] forKey:@"SentUpdates"];
    [transferProgress setValue:[NSString stringWithFormat:@"%u",status->sent.inserts] forKey:@"SentInserts"];
    [transferProgress setValue:[NSString stringWithFormat:@"%u",status->sent.bytes] forKey:@"SentBytes"];
    [transferProgress setValue:[NSString stringWithFormat:@"%s",status->table_name] forKey:@"TableName"];
    [transferProgress setValue:[NSString stringWithFormat:@"%u",status->table_id] forKey:@"TableID"];
    [transferProgress setValue:[NSString stringWithFormat:@"%u",status->sync_table_index] forKey:@"TableIndex"];
    [transferProgress setValue:[NSString stringWithFormat:@"%u",status->sync_table_count] forKey:@"TableCount"];

    switch( status->state ) {
        case UL_SYNC_STATE_STARTING:
            [transferProgress setValue:@"STATE_STARTING" forKey:@"State"];
            break;
        case UL_SYNC_STATE_CONNECTING:
            [transferProgress setValue:@"STATE_CONNECTING" forKey:@"State"];
            break;
        case UL_SYNC_STATE_SENDING_HEADER:
            [transferProgress setValue:@"STATE_SENDING_HEADER" forKey:@"State"];
            break;
        case UL_SYNC_STATE_SENDING_TABLE:
            [transferProgress setValue:@"STATE_SENDING_TABLE" forKey:@"State"];
            break;
        case UL_SYNC_STATE_RECEIVING_TABLE:
            [transferProgress setValue:@"STATE_RECEIVING_TABLE" forKey:@"State"];
            break;
        case UL_SYNC_STATE_SENDING_DOWNLOAD_ACK:
            [transferProgress setValue:@"STATE_SENDING_DOWNLOAD_ACK" forKey:@"State"];
            break;
        case UL_SYNC_STATE_DISCONNECTING:
            [transferProgress setValue:@"STATE_DISCONNECTING" forKey:@"State"];
            break;
        case UL_SYNC_STATE_RESUMING_DOWNLOAD:
            [transferProgress setValue:@"STATE_RESUMING_DOWNLOAD" forKey:@"State"];
            break;
        case UL_SYNC_STATE_SENDING_CHECK_SYNC_REQUEST:
            [transferProgress setValue:@"STATE_CHECK_SYNC_REQUEST" forKey:@"State"];
            break;
        case UL_SYNC_STATE_WAITING_FOR_CHECK_SYNC_RESPONSE:
            [transferProgress setValue:@"STATE_WAITING_FOR_CHECK_SYNC_RESPONSE" forKey:@"State"];
            break;
        case UL_SYNC_STATE_PROCESSING_CHECK_SYNC_RESPONSE:
            [transferProgress setValue:@"STATE_PROCESSING_CHECK_SYNC_RESPONSE" forKey:@"State"];
            break;
        case UL_SYNC_STATE_SENDING_DATA:
            [transferProgress setValue:@"STATE_SENDING_DATA" forKey:@"State"];
            break;
        case UL_SYNC_STATE_ERROR:
            [transferProgress setValue:@"STATE_ERROR" forKey:@"State"];
            break;
        case UL_SYNC_STATE_ROLLING_BACK_DOWNLOAD:
            [transferProgress setValue:@"STATE_ROLLING_BACK_DOWNLOAD" forKey:@"State"];
            break;
        case UL_SYNC_STATE_COMMITTING_DOWNLOAD:
            [transferProgress setValue:@"STATE_COMMITTING_DOWNLOAD" forKey:@"State"];
            break;
        case UL_SYNC_STATE_RECEIVING_DATA:
            [transferProgress setValue:@"STATE_RECEIVING_DATA" forKey:@"State"];
            break;
        case UL_SYNC_STATE_WAITING_FOR_DOWNLOAD:
            [transferProgress setValue:@"STATE_WAITING_FOR_DOWNLOAD" forKey:@"State"];
            break;
        case UL_SYNC_STATE_FINISHING_UPLOAD:
            [transferProgress setValue:@"STATE_FINISHING_UPLOAD" forKey:@"State"];
            break;
        case UL_SYNC_STATE_PROCESSING_UPLOAD_ACK:
            [transferProgress setValue:@"STATE_PROCESSING_UPLOAD_ACK" forKey:@"State"];
            break;
        case UL_SYNC_STATE_WAITING_FOR_UPLOAD_ACK:
            [transferProgress setValue:@"STATE_WAITING_FOR_UPLOAD_ACK" forKey:@"State"];
            break;
        case UL_SYNC_STATE_DONE:
            [transferProgress setValue:@"STATE_DONE" forKey:@"State"];
            break;
    }

    //NSError *error;
    //NSData *jsonData = [NSJSONSerialization dataWithJSONObject:transferProgress options:0 error:&error];
    //NSString *JSONString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];

    NSString* callback = [UltraLiteDB.sharedInstance.syncInfo valueForKey:@"callbackId"];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus: CDVCommandStatus_OK messageAsDictionary:transferProgress];
    [pluginResult setKeepCallbackAsBool:true];

    [UltraLiteDB.sharedInstance.commandDelegate sendPluginResult:pluginResult callbackId:callback];

}
/**
 * sync error codes: http://dcx.sybase.com/1101/en/saerrors_en11/sa-errors-by-sybase-code.html
 *
 *
 */
- (int) syncUser:(NSString *) user withPassword:(NSString *) password callbackId: (NSString*)callbackId andSyncMode: (NSString*) syncMode andPingMode: (bool) pingMode andPublication:(NSString *)sPublication{
    int result = 0;

    @synchronized (self) {
        if (isSyncInProgress) {
          return 5002;
        }

        isSyncInProgress = true;
    }

    ULConnection * conn = (ULConnection*)[self getNewConnection];
    ul_sync_info info;
    ul_stream_error * se = &info.stream_error;
    conn->InitSyncInfo( &info );

    //Fetch syncVersion from syncProfile of publication
    NSMutableArray* syncVersionResult = [self executeQueryInternal:[NSString stringWithFormat:@"SELECT SYNC_PROFILE_OPTION_VALUE('%@', 'ScriptVersion')", sPublication]];
    NSString *syncVersion = [[syncVersionResult objectAtIndex:0] objectForKey:@""];

    info.user_name = [user cStringUsingEncoding:NSASCIIStringEncoding];
    info.password = [password cStringUsingEncoding:NSASCIIStringEncoding];
    info.version = [syncVersion cStringUsingEncoding:NSASCIIStringEncoding];
    info.publications = [sPublication cStringUsingEncoding:NSASCIIStringEncoding] ; //"Synchronization Model 1";
    info.stream = [_protocol cStringUsingEncoding:NSUTF8StringEncoding];
    const char * Params[1] = { UL_TEXT( [syncMode cStringUsingEncoding:NSASCIIStringEncoding] ) };
    // sync parameters
    info.num_auth_parms = 1;
    info.ping = pingMode;
    info.auth_parms = Params;
    syncObserver observer = &SyncObserverFn;
    if(!pingMode){
        info.observer = observer;
    }
    [syncInfo setValue:@"" forKey:@"cancelled"];
    [syncInfo setValue:callbackId forKey:@"callbackId"];
    NSString* stream_params = [NSString stringWithFormat:@"%@;port=%@%@", host_name, port_number, _urlSuffix];
    info.stream_parms = [stream_params cStringUsingEncoding:NSASCIIStringEncoding] ; //"host=192.168.1.147;port=2439";
    //conn->Synchronize( &info );

    if (conn->Synchronize(&info)) {

    } else {
        // Get the error message and log it.
        char errorMsg[500];
        conn->GetLastError()->GetString(errorMsg, 500);
        NSLog(@"Sync failed: %s", errorMsg);
        printf( " stream_error_code is '%hu'\n", se->stream_error_code );
        printf( " system_error_code is '%d'\n", se->system_error_code );
        NSString* message = [NSString stringWithFormat:@"Sync failed: %s", errorMsg];
        [logger logWithLogLevel:logLevelError message:message isTransaction:false];
        conn->Rollback();
    }

    if (info.auth_value >= 2000) {
        result = info.auth_value;
    } else {
        result = info.stream_error.stream_error_code != 0 ? info.stream_error.stream_error_code : info.auth_value;
    }

    @synchronized (self) {
        isSyncInProgress = false;
    }

    [self releaseConnection:conn];
    return result;
}
- (void) cancelSync{
    @synchronized (syncInfo) {
        if (isSyncInProgress) {
            if([syncInfo valueForKey:@"callbackId"] != nil){
                [syncInfo setValue:@"X" forKey:@"cancelled"];
            }
        }
    }
}
void FTObserverFn (ml_file_transfer_status* status) {
    NSMutableDictionary* fileTransferInfo;
    @synchronized (UltraLiteDB.sharedInstance.fileTransfersInfo) {
        fileTransferInfo = [UltraLiteDB.sharedInstance.fileTransfersInfo objectForKey:[NSString stringWithUTF8String:(char *)status->info->user_data]];
        if( [[fileTransferInfo objectForKey:@"cancelled"]  isEqual: @"X"]) {
            status->stop = true;
        }
    }
    NSMutableDictionary* transferProgress = [[NSMutableDictionary alloc] init];
    [transferProgress setValue:@"FileTransfer" forKey:@"progressType"];
    [transferProgress setValue:[fileTransferInfo valueForKey:@"fileId"] forKey:@"fileId"];
    [transferProgress setValue:@"Started" forKey:@"status"];
    [transferProgress setValue:[NSString stringWithFormat:@"%llu",status->file_size] forKey:@"fileSize"];
    [transferProgress setValue:[NSString stringWithFormat:@"%llu",status->bytes_transferred] forKey:@"bytesTransferred"];
    [transferProgress setValue:@"" forKey:@"error"];

    //    NSError *error;
    //    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:transferProgress options:0 error:&error];
    //
    //
    //    NSString *JSONString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];

    NSString* callback = [fileTransferInfo valueForKey:@"callbackId"];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus: CDVCommandStatus_OK messageAsDictionary:transferProgress ];
    [pluginResult setKeepCallbackAsBool:true];
    NSLog(@"printing observer output");
    [UltraLiteDB.sharedInstance.commandDelegate sendPluginResult:pluginResult callbackId:callback];

}
- (ml_file_transfer_info) initFileTransferInfo:(NSString *)user withPassword:(NSString *)password file:(NSString *)file destFolder:(NSString *)destFolder callbackId: (NSString*)callbackId{
    ml_file_transfer_info info;
    MLInitFileTransferInfo( &info );
    MLFTEnableZlibCompression( &info );

    info.filename = [file cStringUsingEncoding:NSUTF8StringEncoding];
    info.local_filename = [file fileSystemRepresentation];
    info.local_path = [destFolder cStringUsingEncoding:NSUTF8StringEncoding];
    //    info.local_path = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] cStringUsingEncoding:NSASCIIStringEncoding];
    info.username = [user cStringUsingEncoding:NSUTF8StringEncoding];
    info.password = [password cStringUsingEncoding:NSUTF8StringEncoding];
    info.version = [subscription_name cStringUsingEncoding:NSUTF8StringEncoding];
    info.stream = [_protocol cStringUsingEncoding:NSUTF8StringEncoding];
    NSString *stream_params = [NSString stringWithFormat:@"%@;port=%@;compression=zlib%@", host_name, port_number, _urlSuffix];
    info.stream_parms = [stream_params cStringUsingEncoding:NSUTF8StringEncoding];
    info.user_data = (void *)[file cStringUsingEncoding:NSUTF8StringEncoding];
    info.enable_resume = true;
    fileTransferObserver observer = &FTObserverFn;
    info.observer = observer;
    NSMutableDictionary*  fileTransferInfo = [[NSMutableDictionary alloc] init];

    [fileTransferInfo setValue:file forKey:@"fileName"];
    [fileTransferInfo setValue:callbackId forKey:@"callbackId"];
    [fileTransferInfo setValue:file forKey:@"fileId"];
    [fileTransferInfo setValue:@"" forKey:@"cancelled"];
    [self addNewFileTransferInfo:fileTransferInfo];


    return info;
}
- (int) downloadFile:(NSString *) user withPassword:(NSString *) password fileName:(NSString *) file directoryPath:(NSString *) directoryPath callbackId: (NSString*)callbackId{
    ml_file_transfer_info info = [self initFileTransferInfo:user withPassword:password file:file destFolder:directoryPath callbackId:callbackId];
    int result = 0;

    if( ! MLFileDownload( &info ) ) {
        NSLog(@"Download failed for: %@", file);
        MLFiniFileTransferInfo( &info );
    }
    else {
        NSLog(@"Download successful for: %@", file);
        MLFiniFileTransferInfo( &info );
    }

    if (info.auth_value >= 2000) {
        result = info.auth_value;
    } else if (info.file_auth_code >= 2000) {
        result = info.file_auth_code;
    } else {
        result = info.error.stream_error_code != 0 ? info.error.stream_error_code : info.auth_value;
    }

    return result;

}
- (int) uploadFile:(NSString *) user withPassword:(NSString *) password fileName:(NSString *) file directoryPath:(NSString *) directoryPath callbackId: (NSString*)callbackId{
    ml_file_transfer_info info = [self initFileTransferInfo:user withPassword:password file:file destFolder:directoryPath callbackId:callbackId];
    int result = 0;

    if( ! MLFileUpload( &info ) ) {
        NSLog(@"Upload failed for: %@", file);
        MLFiniFileTransferInfo( &info );
    }
    else {
        NSLog(@"Upload successful for: %@", file);
        MLFiniFileTransferInfo( &info );
    }

    if (info.auth_value >= 2000) {
        result = info.auth_value;
    } else if (info.file_auth_code >= 2000) {
        result = info.file_auth_code;
    } else {
        result = info.error.stream_error_code != 0 ? info.error.stream_error_code : info.auth_value;
    }

    return result;

}
- (void) cancelFileTransfer: (NSString*)fileId{
    @synchronized (fileTransfersInfo) {
        if([fileTransfersInfo objectForKey:fileId] != nil){
            [[fileTransfersInfo objectForKey:fileId] setValue:@"X" forKey:@"cancelled"];
        }
    }
}
@end

